<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class inventory_transformation extends Model
{
    protected $table = 'inventory_transformation';

    public function product_detail(){

        return $this->belongsTo(Product_details::class,'product_detail_id');
    }
    public function inventory_from(){

        return $this->belongsTo(Inventory::class,'from_inv_id');
    } public function inventory_to(){

        return $this->belongsTo(Inventory::class,'to_inv_id');
    }

    public function admin()
    {
        return $this->belongsTo(Shop::class,'user_id');
    }
}
