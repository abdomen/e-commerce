<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Tax;
use App\Models\products;

class Category extends Model
{    
    protected $table = 'categories';

    public function product()
    {
        return $this->hasMany(Products::class,'cat_id');
    }


}
