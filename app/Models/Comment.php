<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function user_comment()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
