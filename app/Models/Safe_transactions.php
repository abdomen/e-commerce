<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Safe_transactions extends Model
{
    protected $table = 'safe_transactions';
    
    public function admin()
    {
        return $this->belongsTo(Shop::class,'admin_id');
    }

    public function safe_from_data()
    {
        return $this->belongsTo(Safe::class,'safe_from');
    }

    public function safe_to_data()
    {
        return $this->belongsTo(Safe::class,'safe_to');
    }
}
