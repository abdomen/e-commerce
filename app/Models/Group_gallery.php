<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Groups;

class Group_gallery extends Model
{
    
    protected $table = 'group_gallery';
    
    public function group()
    {
        return $this->belongsTo(Groups::class);
    }
}
