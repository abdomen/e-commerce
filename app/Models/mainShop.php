<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MainShop extends Model
{
    protected $table = 'mainshop';
}
