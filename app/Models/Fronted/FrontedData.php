<?php

namespace App\Models\Fronted;


use App\Models\Category;

class FrontedData
{
    public static function get_categories()
    {
        $cats=Category::all();
        return $cats;
    }

}
