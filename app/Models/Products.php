<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Inventory;
use App\Models\Product_details;

class Products extends Model
{
    public function orders()
    {
        return $this->belongsToMany('App\Models\Order','carts','product_id','order_id');
    }

    public function cat()
    { 
        return $this->belongsTo(Category::class,'cat_id');
    }

    public function user_favorite()
    {
        return $this->belongsToMany('App\User','whishlists','product_id','user_id');
    }

    public function price_product()
    {
        return $this->belongsToMany('App\Models\Currency','product__currencies','product_id','currency_id')
            ->withPivot('price','price_after_offer');
    }

    /*
     * get currency & price depended on currency_id of custom user
     * get general currency & price
     */
    public function get_price($user,$lang)
    {
        $price_v1=$this->price_product->count() > 0 ? $this->price_product[0]->pivot->price : 0;
        $price_after_amount=$this->price_product->count() > 0 ? $this->price_product[0]->pivot->price_after_offer : 0;
        if($this->is_offer == 1) {
            $price=$price_after_amount;
        }else{
            $price=$price_v1;

        }
       // $price_name=$lang=='ar' ? $price->name_ar : $price->name_en;
        //return  [$price_v1,$price_after_amount];

        if($user) {
            $currency_id = $user->currency_id;
            if ($currency_id) {
                $has_price = $this->price_product->contains($currency_id);
                if ($has_price) {
                    $price = $this->price_product->where('id', $currency_id)->first();
                    $price_name=$lang=='ar' ? $price->name_ar : $price->name_en;
                    return [$price->pivot->price .  $price_name,$price->pivot->price_after_offer .  $price_name];
                }

        return  [$price_v1,$price_after_amount];
            }
        }
        $price= $this->price_product->where('defualt',1)->first();
        if(is_null($price)){
        return  [$price_v1,$price_after_amount];
        }else{
            $price_name=$lang=='ar' ? $price->name_ar : $price->name_en;
            return [$price->pivot->price .  $price_name,$price->pivot->price_after_offer . $price_name];
        }
    }


    public function user_rate()
    {
        return $this->belongsToMany('App\User','rates','product_id','user_id')
            ->withPivot('rate','created_at','comment');
    }


    public function group()
    {
        return $this->hasMany(Product_group::class,'product_id');
    }


    public function inventory()
    {
        return $this->belongsTo(inventory::class,'inventory_id');
    }

    public function product_details()
    {
        return $this->hasMany(Product_details::class,'product_id');
    }
}
