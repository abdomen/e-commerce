<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Models\Inventory;

class Rep_Inv extends Model
{
    protected $table = 'representative_inventory';

    public function Representative()
    {
        return $this->belongsTo(User::class , 'representative_id');
    }

    public function Inventory()
    {
        return $this->belongsTo(Inventory::class , 'inventory_id');
    }
}