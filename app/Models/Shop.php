<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Shop extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $guard = 'Shop';
    
    public function roles()
    {
        return $this->belongsToMany('App\Models\role', 'shop_role', 'shop_id', 'role_id')->orderBy('id','asc');
    }

    public function getRoleIds()
    {
        $array=[];
        foreach ($this->roles as $row)
        {
            $array[]=$row->id;
        }
        return $array;
    }
}
