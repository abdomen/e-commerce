<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminRoles extends Model
{
    protected $table = 'admin_roles';

    public function admin()
    {
        return $this->belongsToMany('App\Models\Admin', 'admin_role', 'role_id', 'admin_id');
    }
}
