<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inventory_stock extends Model
{
    protected $table = 'inventory_stock';
    
    public function user()
    {
        return $this->belongsTo(Shop::class,'user_id');
    }

    public function inventory()
    {
        return $this->belongsTo(Inventory::class,'inventory_id');
    }

    public function product_detail()
    {
        return $this->belongsTo(Product_details::class,'product_detail_id');
    }
}
