<?php

namespace App\Models; 

use Illuminate\Database\Eloquent\Model;
use App\User;

class P_d_orders extends Model
{
    protected $table = 'orders';

    

    public function productdetails(){
        return $this->belongsToMany('App\Models\Product_details','carts','p_d_order_id','product_detail_id')
            ->withPivot('size_id','color_id','quantity')->where('carts.type',1);
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function representative()
    {
        return $this->belongsTo(User::class,'representative_id');
    }

    public function address()
    {
        return $this->belongsTo('App\Models\order_locations','address_id');
    }

    public function Products()
    {
        return $this->hasMany(Order_product_details::class , 'p_d_order_id');
    }

    public function safe()
    {
        return $this->belongsTo(Safe::class,'safe_id');
    }
}
