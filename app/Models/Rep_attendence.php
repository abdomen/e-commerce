<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Rep_attendence extends Model
{
    protected $table = 'attendence';

    public function representative()
    {
        return $this->belongsTo(User::class , 'representative_id');
    }
}
