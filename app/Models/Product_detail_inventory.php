<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Inventory;
use App\Models\Product_details;

class Product_detail_inventory extends Model
{
    protected $table = 'product_detail_inventory';

    public function inventory()
    {
        return $this->belongsTo(Inventory::class , 'inventory_id');
    }

    public function Product_detail()
    {
        return $this->belongsTo(Product_details::class , 'product_detail_id');
    }
}
