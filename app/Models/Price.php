<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $table = 'price';

    public function Product_detail_price()
    {
        return $this->hasMany(Product_details_price::class , 'price_id');
    }
}
