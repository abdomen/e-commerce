<?php

namespace App\Http\Controllers\GeneralDash;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Manage\BaseController;
use Validator,Auth,Artisan,Hash,File,Crypt,DB;
use App\Models\MainShop;
use App\Models\Product_details;
use App\Models\Category;
use App\Models\Currency;
use App\Models\Inventory;
use App\Models\P_d_orders;
use App\Models\Products;
use App\Http\Resources\GeneralDash\MainShopResource;

class MainShopController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    /*
     * Add new user to Auth MainShop database
     */
    public function add_mainShop(Request $request)
    {
        $lang = $request->header('lang');
        
        $input = $request->all();
        $validationMessages = [
            'name.required' => $lang == 'ar' ?  'من فضلك ادخل رقم الاسم ' :" name is required" ,
            'name.unique' => $lang == 'ar' ?  'الاسم موجود لدينا بالفعل' :"name is already teken" ,
            'email.required' => $lang == 'ar' ? 'من فضلك ادخل البريد الالكتروني' :"email is required"  ,
            'email.unique' => $lang == 'ar' ? 'هذا البريد الالكتروني موجود لدينا بالفعل' :"email is already teken" ,
            'email.regex'=>$lang=='ar'? 'من فضلك ادخل بريد الكتروني صالح' : 'The email must be a valid email address',
            'phone.required' => $lang == 'ar' ? 'من فضك ادخل رقم الهاتف' :"phone is required"  ,
            'phone.unique' => $lang == 'ar' ? 'رقم الهاتف موجود لدينا بالفعل' :"phone is already teken" ,
            'phone.min' => $lang == 'ar' ?  'رقم الهاتف يجب ان لا يقل عن 7 ارقام' :"The phone must be at least 7 numbers" ,
            'phone.numeric' => $lang == 'ar' ?  ' الهاتف يجب ان يكون رقما' :"The phone must be a number" ,
        ];

        $validator = Validator::make($input, [
            'name' => 'required|unique:MainShop',
            'phone' => 'required|unique:users|numeric|min:7',
            'email' => 'required|unique:users|regex:/(.+)@(.+)\.(.+)/i',
        ], $validationMessages);

        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 200);
        }

        $MainShop = new MainShop();
        $MainShop->name = $request->name;
        $MainShop->email = $request->email;
        $MainShop->phone = $request->phone;
        $MainShop->active = $request->active;
        if($request->logo){
            $name=BaseController::saveImage('MainShop',$request->file('logo'));
            $MainShop->logo=$name;
        }
        $MainShop->address_ar = $request->address_ar;
        $MainShop->address_en = $request->address_en;
        $MainShop->about_ar = $request->about_ar;
        $MainShop->about_en = $request->about_en;
        $MainShop->save();

        $msg=$lang == 'ar' ? 'تم اضافة المتجر الرئيسي بنجاح' : 'Main Shop added successfully';
        return response()->json([ 'status'=>1,'message'=> $msg, 'data'=>new MainShopResource($MainShop)]);
    }

    /*
     * Edit MainShop information
    */
    public function edit_mainShop(Request $request,$MainShopId)
    {
        $lang = $request->header('lang');

        $MainShop = MainShop::find($MainShopId);
        $check=$this->not_found($MainShop,'المتجر الرئيسي','MainShop',$lang);
        if(isset($check))
        {
            return $check;
        }

        $input = $request->all();
        $validationMessages = [
            'name.required' => $lang == 'ar' ?  'من فضلك ادخل رقم الاسم ' :" name is required" ,
            'name.unique' => $lang == 'ar' ?  'الاسم موجود لدينا بالفعل' :"name is already teken" ,
            'email.required' => $lang == 'ar' ? 'من فضلك ادخل البريد الالكتروني' :"email is required"  ,
            'email.unique' => $lang == 'ar' ? 'هذا البريد الالكتروني موجود لدينا بالفعل' :"email is already teken" ,
            'email.regex'=>$lang=='ar'? 'من فضلك ادخل بريد الكتروني صالح' : 'The email must be a valid email address',
            'phone.required' => $lang == 'ar' ? 'من فضك ادخل رقم الهاتف' :"phone is required"  ,
            'phone.unique' => $lang == 'ar' ? 'رقم الهاتف موجود لدينا بالفعل' :"phone is already teken" ,
            'phone.min' => $lang == 'ar' ?  'رقم الهاتف يجب ان لا يقل عن 7 ارقام' :"The phone must be at least 7 numbers" ,
            'phone.numeric' => $lang == 'ar' ?  ' الهاتف يجب ان يكون رقما' :"The phone must be a number" ,
        ];

        $validator = Validator::make($input, [
            'name' => 'bail|required|unique:admin,name,'.$MainShopId,
            'phone' => 'bail|required|unique:admin,phone,'.$MainShopId.'|numeric|min:7',
            'email' => 'bail|required|unique:admin,email,'.$MainShopId.'|regex:/(.+)@(.+)\.(.+)/i',
        ], $validationMessages);

        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 200);
        }

        $MainShop->name = $request->name;
        $MainShop->email = $request->email;
        $MainShop->phone = $request->phone;
        $MainShop->active = $request->active;
        if($request->logo){
            BaseController::deleteFile('MainShop',$MainShop->logo);
            $name=BaseController::saveImage('MainShop',$request->file('logo'));
            $MainShop->logo=$name;
        }
        $MainShop->address_ar = $request->address_ar;
        $MainShop->address_en = $request->address_en;
        $MainShop->about_ar = $request->about_ar;
        $MainShop->about_en = $request->about_en;
        $MainShop->save();

        $msg=$lang=='ar' ?  'تم تعديل بيانات المتجر الرئيسي' :'Main Shop Edited successfully' ;
        return $this->apiResponseData(new MainShopResource($MainShop),  $msg);
    }


    /*
     * get All MainShops
     */
    public function all_mainShops(Request $request)
    {
        $MainShops=MainShop::orderBy('id','asc')->get();
        return $this->apiResponseData(MainShopResource::collection($MainShops),'success');
    }


    /*
     * Show single  MainShop
    */
    public function single_mainShop(Request $request,$MainShopId){
        $lang=$request->header('lang');

        $MainShop = MainShop::find($MainShopId);
        $check=$this->not_found($MainShop,'المتجر الرئيسي','MainShop',$lang);
        if(isset($check))
        {
            return $check;
        }

        $msg=$lang=='ar' ?'تمت العملية بنجاح' : 'success';
        return $this->apiResponseData($MainShop,$msg);

    }

    /*
     * Delete MainShop ..
     */

    public function delete_mainShop(Request $request,$MainShopId){
        $lang=$request->header('lang');

        $MainShop=MainShop::find($MainShopId);
        $check=$this->not_found($MainShop,'المتجر الرئيسي','MainShop',$lang);
        if(isset($check)){
            return $check;
        }

        if($MainShopId == 1)
        {
            $msg=$lang=='ar' ? 'لا يمكن حذف المتجر الرئيسي الاول'  : 'Cannot delete first main shop';
            return $this->apiResponseMessage(1,$msg,200);

        }else{
            BaseController::deleteFile('MainShop',$MainShop->logo);
            $MainShop->delete();
        }

        $msg=$lang=='ar' ? 'تم حذف المتجر الرئيسي بنجاح'  : 'Main Shop Deleted successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }


    /*
     * Show single  MainShop
    */
    public function change_mainShop_status(Request $request,$MainShopId)
    {
        $lang=$request->header('lang');

        $MainShop = MainShop::find($MainShopId);
        $check=$this->not_found($MainShop,'المتجر الرئيسي','MainShop',$lang);
        if(isset($check))
        {
            return $check;
        }

        $MainShop->active = $request->active;
        $MainShop->save();

        $msg=$lang=='ar' ?'تمت العملية بنجاح' : 'success';
        return $this->apiResponseData(new MainShopResource($MainShop),$msg);

    }

/*
     * Show single  MainShop
    */
    public function mainShop_details(Request $request,$MainShopId)
    {
        $lang=$request->header('lang');

        $MainShop = MainShop::find($MainShopId);
        $check=$this->not_found($MainShop,'المتجر الرئيسي','MainShop',$lang);
        if(isset($check))
        {
            return $check;
        }

        $products_count = Product_details::where('mainShop_id',$MainShopId)->count('id');
        $Category_count = Category::where('mainShop_id',$MainShopId)->count('id');
        $currencies_count = Currency::where('mainShop_id',$MainShopId)->count('id');
        $Inventories_count = Inventory::where('mainShop_id',$MainShopId)->count('id');
        $P_d_orders_count = P_d_orders::where('mainShop_id',$MainShopId)->count('id');
        $main_products_count = Products::where('mainShop_id',$MainShopId)->count('id');

        $data = [
            'categories_count' => $Category_count,
            'main_products_count' => $main_products_count,
            'products_count' => $products_count,
            'currencies_count' => $currencies_count,
            'Inventories_count' => $Inventories_count,
            'orders_count' => $P_d_orders_count,
        ];
        $msg=$lang=='ar' ?'تمت العملية بنجاح' : 'success';
        return $this->apiResponseData($data,$msg);

    }
}
