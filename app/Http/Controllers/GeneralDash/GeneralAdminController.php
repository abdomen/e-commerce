<?php

namespace App\Http\Controllers\GeneralDash;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Manage\BaseController;
use Validator,Auth,Artisan,Hash,File,Crypt,DB;
use App\Models\Admin;
use App\Models\AdminRoles;
use App\Http\Resources\GeneralDash\GeneralAdminResource;

class GeneralAdminController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;


    public function login(Request $request)
    {
        $lang = $request->header('lang');
        $credentials = [

            'name' => $request['nameORemail'],
            'password'=>$request['password'],
        ];
        $credentials_2 = [

            'email' => $request['nameORemail'],
            'password'=>$request['password'],
        ];
        if (Auth::guard('Admin')->attempt($credentials) || Auth::guard('Admin')->attempt($credentials_2)) {
            $admin=Auth::guard('Admin')->user();
            $token=$admin->createToken('Admin')->accessToken;
            $admin['token']=$token;
            $msg=$lang=='ar' ? 'تم تسجيل الدخول بنجاح' : 'login success';
            return $this->apiResponseData(new GeneralAdminResource($admin),$msg,200);
        }
        $msg=$lang=='ar' ? 'البيانات المدخلة غير صحيحة' : 'invalid admin name, admin email or password';
        return $this->apiResponseMessage(0,$msg,200);

    }



    /*
     * Add new user to Auth Admin database
     */
    public function add_generalAdmin(Request $request)
    {
        $lang = $request->header('lang');
        
        $input = $request->all();
        $validationMessages = [
            'name.required' => $lang == 'ar' ?  'من فضلك ادخل رقم الاسم ' :" name is required" ,
            'name.unique' => $lang == 'ar' ?  'الاسم موجود لدينا بالفعل' :"name is already teken" ,
            'password.required' => $lang == 'ar' ? 'من فضلك ادخل كلمة السر' :"password is required"  ,
            'password.confirmed' => $lang == 'ar' ? 'كلمتا السر غير متطابقتان' :"The password confirmation does not match"  ,
            'password.min' => $lang == 'ar' ?  'كلمة السر يجب ان لا تقل عن 7 احرف' :"The password must be at least 6 character" ,
            'email.required' => $lang == 'ar' ? 'من فضلك ادخل البريد الالكتروني' :"email is required"  ,
            'email.unique' => $lang == 'ar' ? 'هذا البريد الالكتروني موجود لدينا بالفعل' :"email is already teken" ,
            'email.regex'=>$lang=='ar'? 'من فضلك ادخل بريد الكتروني صالح' : 'The email must be a valid email address',
            'phone.required' => $lang == 'ar' ? 'من فضك ادخل رقم الهاتف' :"phone is required"  ,
            'phone.unique' => $lang == 'ar' ? 'رقم الهاتف موجود لدينا بالفعل' :"phone is already teken" ,
            'phone.min' => $lang == 'ar' ?  'رقم الهاتف يجب ان لا يقل عن 7 ارقام' :"The phone must be at least 7 numbers" ,
            'phone.numeric' => $lang == 'ar' ?  ' الهاتف يجب ان يكون رقما' :"The phone must be a number" ,
        ];

        $validator = Validator::make($input, [
            'name' => 'required|unique:admin',
            'phone' => 'required|unique:users|numeric|min:7',
            'email' => 'required|unique:users|regex:/(.+)@(.+)\.(.+)/i',
            'password' => 'required|confirmed|min:6',
        ], $validationMessages);

        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 200);
        }

        $admin = new Admin();
        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->phone = $request->phone;
        $admin->jop = $request->jop;
        if($request->image){
            $name=BaseController::saveImage('GeneralAdmin',$request->file('image'));
            $admin->image=$name;
        }
        $admin->password = Hash::make($request->password);
        $admin->save();
        $admin['token']=null;
        $msg=$lang == 'ar' ? 'تم اضافة المدير بنجاح' : 'Admin added successfully';
        return response()->json([ 'status'=>1,'message'=> $msg, 'data'=>new GeneralAdminResource($admin)]);
    }

    /*
     * Edit admin information
    */
    public function edit_generalAdmin(Request $request,$adminId)
    {
        $lang = $request->header('lang');

        $admin = Admin::find($adminId);
        $check=$this->not_found($admin,'المدير','Admin',$lang);
        if(isset($check))
        {
            return $check;
        }

        $input = $request->all();
        $validationMessages = [
            'name.required' => $lang == 'ar' ?  'من فضلك ادخل رقم الاسم ' :" name is required" ,
            'name.unique' => $lang == 'ar' ?  'الاسم موجود لدينا بالفعل' :"name is already teken" ,
            'password.required' => $lang == 'ar' ? 'من فضلك ادخل كلمة السر' :"password is required"  ,
            'password.confirmed' => $lang == 'ar' ? 'كلمتا السر غير متطابقتان' :"The password confirmation does not match"  ,
            'password.min' => $lang == 'ar' ?  'كلمة السر يجب ان لا تقل عن 7 احرف' :"The password must be at least 6 character" ,
            'email.required' => $lang == 'ar' ? 'من فضلك ادخل البريد الالكتروني' :"email is required"  ,
            'email.unique' => $lang == 'ar' ? 'هذا البريد الالكتروني موجود لدينا بالفعل' :"email is already teken" ,
            'email.regex'=>$lang=='ar'? 'من فضلك ادخل بريد الكتروني صالح' : 'The email must be a valid email address',
            'phone.required' => $lang == 'ar' ? 'من فضك ادخل رقم الهاتف' :"phone is required"  ,
            'phone.unique' => $lang == 'ar' ? 'رقم الهاتف موجود لدينا بالفعل' :"phone is already teken" ,
            'phone.min' => $lang == 'ar' ?  'رقم الهاتف يجب ان لا يقل عن 7 ارقام' :"The phone must be at least 7 numbers" ,
            'phone.numeric' => $lang == 'ar' ?  ' الهاتف يجب ان يكون رقما' :"The phone must be a number" ,
        ];

        $validator = Validator::make($input, [
            'name' => 'bail|required|unique:admin,name,'.$adminId,
            'phone' => 'required|unique:users|numeric|min:7',
            'email' => 'bail|required|unique:admin,email,'.$adminId.'|regex:/(.+)@(.+)\.(.+)/i',
            'phone' => 'bail|required|unique:admin,phone,'.$adminId.'|numeric|min:7',
        ], $validationMessages);

        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 200);
        }

        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->phone = $request->phone;
        $admin->jop = $request->jop;
        if($request->image){
            BaseController::deleteFile('GeneralAdmin',$admin->image);
            $name=BaseController::saveImage('GeneralAdmin',$request->file('image'));
            $admin->image=$name;
        }
        $admin->password = Hash::make($request->password);
        $admin->save();
        $admin['token']=null;

        $msg=$lang=='ar' ?  'تم تعديل بيانات المدير' :'Admin Edited successfully' ;
        return $this->apiResponseData(new GeneralAdminResource($admin),  $msg);
    }


    /*
     * get All admins
     */
    public function all_generalAdmins(Request $request)
    {
        $admins=Admin::orderBy('id','asc')->get();
        foreach($admins as $row){$row['token']=null;}
        return $this->apiResponseData(GeneralAdminResource::collection($admins),'success');
    }


    /*
     * Show single  admin
    */
    public function single_generalAdmin(Request $request,$adminId){
        $lang=$request->header('lang');

        $user = Admin::find($adminId);
        $check=$this->not_found($user,'المدير','Admin',$lang);
        if(isset($check))
        {
            return $check;
        }

        $user['token']=null;
        $msg=$lang=='ar' ?'تمت العملية بنجاح' : 'success';
        return $this->apiResponseData(new GeneralAdminResource($user),$msg);

    }

    /*
     * Delete admin ..
     */

    public function delete_generalAdmin(Request $request,$adminId){
        $lang=$request->header('lang');

        $admin=Admin::find($adminId);
        $check=$this->not_found($admin,'المدير','Admin',$lang);
        if(isset($check)){
            return $check;
        }

        if($adminId == 1)
        {
            $msg=$lang=='ar' ? 'لا يمكن حذف المدير الرئيسي'  : 'Cannot delete main admin';
            return $this->apiResponseMessage(1,$msg,200);

        }else{
            BaseController::deleteFile('GeneralAdmin',$admin->image);
            $admin->delete();
        }

        $msg=$lang=='ar' ? 'تم حذف المدير بنجاح'  : 'Admin Deleted successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }


    /*
     * add role
     */
    public function add_role(Request $request,$adminId)
    {
        $lang=$request->header('lang');

        $admin=Admin::find($adminId);
        $check=$this->not_found($admin,'المدير','Admin',$lang);
        if(isset($check)){
            return $check;
        }

        $isset_role=$admin->roles->contains($request->role_id);
        if($isset_role)
        {
            $msg=$lang=='ar' ? 'هذا المدير لديه هذه الصلاحية'  : 'Admin already has this role';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $role=AdminRoles::find($request->role_id);
        if(is_null($role))
        {
            $msg=$lang=='ar' ? 'الصلاحية غير موجودة'  : 'role not found';
            return $this->apiResponseMessage(0,$msg,200);
        }

        DB::table('admin_role')->insert(
            ['admin_id' => $adminId, 'role_id' => $request->role_id]
        );

        $admin['token']=null;
        $msg=$lang=='ar' ?'تم اضافة الصلاحية بنجاح' : 'role added successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }

    /*
     * add role
     */
    public function delete_role(Request $request,$adminId)
    {
        $lang=$request->header('lang');

        $roleId = $request->role_id;

        $admin=Admin::find($adminId);
        $check=$this->not_found($admin,'المدير','Admin',$lang);
        if(isset($check)){
            return $check;
        }

        DB::table('admin_role')->where('admin_id',$adminId)->where('role_id',$roleId)->delete();

        $admin['token']=null;

        $msg=$lang=='ar' ?'تمت العملية بنجاح' : 'success';
        return $this->apiResponseData(new GeneralAdminResource($admin),$msg);
    }
}