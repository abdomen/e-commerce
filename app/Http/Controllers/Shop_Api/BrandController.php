<?php

namespace App\Http\Controllers\Shop_Api;

use App\Http\Resources\BrandsResource;
use App\Models\Brands;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Controllers\Manage\BaseController;

class BrandController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    /*
        * Add new Brands
    */

    public function add_Brands(Request $request)
    {
        $lang=$request->header('lang');
        if(!$request->image)
        {
            $msg=$lang=='ar' ? 'من فضلك ادخل الصورة' : 'image is required';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $Brands = new Brands;
        $Brands->image=BaseController::saveImage('Brands',$request->image);
        $Brands->mainShop_id = $request->mainShop_id;
        $Brands->save();
        $msg=$lang=='ar' ? 'تم اضافة البراند' : 'brand added successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }

    /*
     * Delete Brands
     */

    public function delete_Brands(Request $request,$Brands_id)
    {
        $lang=$request->header('lang');
        $Brands=Brands::find($Brands_id);
        $check=$this->not_found($Brands,'البراند','Brand',$lang);
        if(isset($check))
        {
            return $check;
        }
        BaseController::deleteFile('Brands',$Brands->image);
        $Brands->delete();
        $msg=$lang=='ar' ? 'تم حذف البراند بنجاح' : 'Brand deleted successfully';
        return $this->apiResponseMessage(0,$msg,200);
    }

    /*
     * single Brands
     */

    public function single_Brands(Request $request,$Brands_id)
    {
        $lang=$request->header('lang');
        $Brands=Brands::find($Brands_id);
        $check=$this->not_found($Brands,'البراند','Brand',$lang);
        if(isset($check))
        {
            return $check;
        }

        return $this->apiResponseData(new BrandsResource($Brands),'success',200);
    }

    /*
     * All Brandss
     */

    public function all_Brands(Request $request)
    {
        $lang=$request->header('lang');
        $Brandss=Brands::where('mainShop_id' , $request->mainShop_id)->get();
        return $this->apiResponseData(BrandsResource::collection($Brandss),'success',200);
    }
}
