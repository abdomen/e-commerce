<?php

namespace App\Http\Controllers\Shop_Api;

use App\Models\role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Resources\UserResource;
use App\Http\Resources\Shop\ProductResource;
use App\Http\Controllers\Manage\BaseController;

class RoleController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    /*
        * Add new Role
    */

    public function add_Role(Request $request)
    {
        $lang=$request->header('lang');
        if(!$request->name_en)
        {
            $msg=$lang=='ar' ? 'من فضلك ادخل اسم الصلاحية بالعربية' : 'Role in arabic is required';
            return $this->apiResponseMessage(0,$msg,200);
        }
        if(!$request->name_ar)
        {
            $msg=$lang=='ar' ? 'من فضلك ادخل الصلاحية بالانجليزية' : 'Role in english is required';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $Role = new role;
        $Role->name_en=$request->name_en;
        $Role->name_ar=$request->name_ar;
        $Role->save();
        $msg=$lang=='ar' ? 'تم اضافة الصلاحية بنجاح' : 'Role added successfully';
        return $this->apiResponseMessage(0,$msg,200);
    }

    /*
     * Edit Role
    */
    public function edit_Role(Request $request,$Role_id)
    {
        $lang=$request->header('lang');
        $Role=Role::find($Role_id);
        $check=$this->not_found($Role,'الصلاحية','Role',$lang);
        if(isset($check))
        {
            return $check;
        }
        if(!$request->name_en)
        {
            $msg=$lang=='ar' ? 'من فضلك ادخل الصلاحية بالعربية' : 'Role in arabic is required';
            return $this->apiResponseMessage(0,$msg,200);
        }
        if(!$request->name_ar)
        {
            $msg=$lang=='ar' ? 'من فضلك ادخل الصلاحية بالانجليزية' : 'Role in english is required';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $Role->name_en=$request->name_en;
        $Role->name_ar=$request->name_ar;
        $Role->save();
        $msg=$lang=='ar' ? 'تم تعديل الصلاحية بنجاح' : 'Role edited successfully';
        return $this->apiResponseMessage(0,$msg,200);
    }

    /*
     * Delete Role
     */

    public function delete_Role(Request $request,$Role_id)
    {
        $lang=$request->header('lang');
        $Role=role::find($Role_id);
        $check=$this->not_found($Role,'الصلاحية','Role',$lang);
        if(isset($check))
        {
            return $check;
        }
        $Role->delete();
        $msg=$lang=='ar' ? 'تم حذف الصلاحية بنجاح' : 'Role deleted successfully';
        return $this->apiResponseMessage(0,$msg,200);
    }

    /*
     * single Role
     */

    public function single_Role(Request $request,$Role_id)
    {
        $lang=$request->header('lang');
        $Role=role::find($Role_id);
        $check=$this->not_found($Role,'الصلاحية','Role',$lang);
        if(isset($check))
        {
            return $check;
        }

        return $this->apiResponseData($Role,'success',200);
    }

    /*
     * All Roles
     */

    public function all_Role(Request $request)
    {
        $lang=$request->header('lang');
        $roles=role::all();
        return $this->apiResponseData($roles,'success',200);
    }
}