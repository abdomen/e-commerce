<?php

namespace App\Http\Controllers\Shop_Api;

use App\Http\Controllers\Api\NotificationMethods;
use App\Http\Resources\ColorResource;
use App\Models\Products;
use App\Models\Product_group;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Resources\Product_groupResource;
use App\Http\Resources\ProductGroupResource;
use App\User;
use App\Models\Shop;
use App\Http\Controllers\Manage\BaseController;

class Product_groupController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    /*
     * get all Product_groups
     */
    public function all_Product_group (Request $request){
        $user=Auth::user();
        $page=$request->page * 20;
        $Product_groups=Product_group::where('mainShop_id' , $user->id)->orderBy('id','desc')->get();
        return $this->apiResponseData(Product_groupResource::collection($Product_groups),'success');
    }


    /*
    * Delete Product_groups
    */
    public function delete_Product_group(Request $request,$Product_group_id)
    {
        $lang=$request->header('lang');
        $Product_group=Product_group::find($Product_group_id);
        $check=$this->not_found($Product_group,'مجموعة المنتج','Product_group',$lang);
        if(isset($check)){
            return $check;
        }
        $Product_group->delete();
        $msg=$lang=='ar' ? 'تم حذف مجموعة المنتج بنجاح'  : 'Product_group Deleted successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }



    /*
     * Single Product_group to show data
     */
    public function single_Product_group(Request $request,$Product_group_id)
    {
        $lang=$request->header('lang');
        $Product_group=Product_group::find($Product_group_id);
        $check=$this->not_found($Product_group,'مجموعة المنتج','Product_group',$lang);
        if(isset($check)){
            return $check;
        }
        return $this->apiResponseData(new ProductGroupResource($Product_group),'success');
    }

    /*
     * Add new Product_group
     * add general Product_group column , add products to this Product_group
     */

    public function add_Product_group(Request $request)
    {
        
        $lang=$request->header('lang');
        // $user=Auth::user();
        // $check=$this->not_found($user,'العضو','user',200);
        // if(isset($check))
        // {
        //     return $check;
        // }
        $validate_Product_group=$this->validate_Product_group($request);
        if(isset($validate_Product_group)){
            return $validate_Product_group;
        }

        $Product_group=new Product_group();
        $Product_group->group_id=$request->group_id;
        $Product_group->product_id=$request->product_id;
        $Product_group->discount_per_type=$request->discount_per_type;
        $Product_group->discount_per_value=$request->discount_per_value;
        $Product_group->mainShop_id=$request->mainShop_id;
        $Product_group->save();
        $msg=$lang=='ar' ?  'تم تسجيل مجموعة المنتج بنجاح' :'Product_group saved successfully' ;
        return $this->apiResponseData(new Product_groupResource($Product_group),$msg);
    }



    /*
     * @pram Product_group column ,
     * @return success or validate
     */

    public function edit_Product_group(Request $request,$Product_group_id)
    {
        $lang=$request->header('lang');
        $Product_group=Product_group::find($Product_group_id);
        $check=$this->not_found($Product_group,'مجموعة المنتج','Product_group',$lang);
        if(isset($check)){
            return $check;
        }
        $validate_Product_group=$this->validate_Product_group($request);
        if(isset($validate_Product_group)){
            return $validate_Product_group;
        }

        $Product_group->group_id=$request->group_id;
        $Product_group->product_id=$request->product_id;
        $Product_group->discount_per_type=$request->discount_per_type;
        $Product_group->discount_per_value=$request->discount_per_value;
        $Product_group->save();
        $msg=$lang=='ar' ? 'تم تعديل مجموعة المنتج بنجاح'  : 'Product_group edited successfully';
        return $this->apiResponseData(new Product_groupResource($Product_group),$msg);
    }


    /*
     * @pram $request
     * @return Error message or check if user is null
    */

    private function validate_Product_group($request){
        $lang = $request->header('lang');
        $input = $request->all();
        $validationMessages = [
            'group_id.required' => $lang == 'ar' ?  'من فضلك ادخل رقم المجموعة' :"group_id in arabic is required" ,
            'product_id.required' => $lang == 'ar' ? 'من فضلك ادخل رقم المنتج' :"product_id in english is required"  ,
        ];
        $validator = Validator::make($input, [
            'group_id' => 'required',
            'product_id' => 'required',
        ], $validationMessages);
        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 400);
        }
    }
  
}
