<?php

namespace App\Http\Controllers\Shop_Api;

use App\Http\Resources\ColorResource;
use App\Models\Category;
use App\Models\Currency;
use App\Models\Product_Currency;
use App\Models\Products;
use App\Models\Sizes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Resources\UserResource;
use App\Http\Resources\Shop\ProductResource;
use App\Http\Resources\CartResource;
use App\User;
use App\Models\Shop;
use App\Http\Controllers\Manage\BaseController;
use App\Models\color;

class ColorController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;


    /*
     * get All product for Auth shop
     */
    public function all_colors(Request $request)
    {
        $user=Auth::user();
        $page=$request->page * 20;
        $colors=color::skip($page)->take(20)->get();
        return $this->apiResponseData(ColorResource::collection($colors),'success');
    }

    /*
     * Add new color
     */

    public function add_color(Request $request)
    {
        $lang=$request->header('lang');
        if(!$request->color_code)
        {
            $msg=$lang=='ar' ? 'من فضلك ادخل اللون' : 'color is required';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $color = new color;
        $color->color_code=$request->color_code;
        $color->mainShop_id = $request->mainShop_id;
        $color->save();
        $msg=$lang=='ar' ? 'تم اضافة اللون بنجاح' : 'color added successfully';
        return $this->apiResponseMessage(0,$msg,200);
    }

    /*
     * Edit Color
     */
    public function edit_color(Request $request,$color_id)
    {
        $lang=$request->header('lang');
        $color=color::find($color_id);
        $check=$this->not_found($color,'اللون','color',$lang);
        if(isset($check))
        {
            return $check;
        }
        if(!$request->color_code)
        {
            $msg=$lang=='ar' ? 'من فضلك ادخل اللون' : 'color is required';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $color->color_code=$request->color_code;
        $color->save();
        $msg=$lang=='ar' ? 'تم تعديل اللون بنجاح' : 'color edited successfully';
        return $this->apiResponseMessage(0,$msg,200);
    }

    /*
     * Delete Color
     */

    public function delete_color(Request $request,$color_id)
    {
        $lang=$request->header('lang');
        $color=color::find($color_id);
        $check=$this->not_found($color,'اللون','color',$lang);
        if(isset($check))
        {
            return $check;
        }
        $color->delete();
        $msg=$lang=='ar' ? 'تم حذف اللون بنجاح' : 'color deleted successfully';
        return $this->apiResponseMessage(0,$msg,200);
    }

    /*
     * single color
     */

    public function single_color(Request $request,$color_id)
    {
        $lang=$request->header('lang');
        $color=color::find($color_id);
        $check=$this->not_found($color,'اللون','color',$lang);
        if(isset($check))
        {
            return $check;
        }

        return $this->apiResponseData(new ColorResource($color),'success',200);
    }

}