<?php

namespace App\Http\Controllers\Shop_Api;

use App\Http\Resources\Shop\SizeResource;
use App\Models\Sizes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Resources\UserResource;
use App\Http\Resources\Shop\ProductResource;
use App\Http\Controllers\Manage\BaseController;

class SizeController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;


    /*
     * get All product for Auth shop
     */
    public function all_Sizes(Request $request)
    {
        $user=Auth::user();
        $page=$request->page * 20;
        $Sizess=Sizes::skip($page)->take(20)->get();
        return $this->apiResponseData(SizeResource::collection($Sizess),'success');
    }

    /*
     * Add new Sizes
     */

    public function add_Size(Request $request)
    {
        $lang=$request->header('lang');
        if(!$request->name_en)
        {
            $msg=$lang=='ar' ? 'من فضلك ادخل المقاس بالعربية' : 'Size in arabic is required';
            return $this->apiResponseMessage(0,$msg,200);
        }
        if(!$request->name_ar)
        {
            $msg=$lang=='ar' ? 'من فضلك ادخل المقاس بالانجليزية' : 'Size in english is required';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $Sizes = new Sizes;
        $Sizes->name_en=$request->name_en;
        $Sizes->name_ar=$request->name_ar;
        $Sizes->mainShop_id = $request->mainShop_id;
        $Sizes->save();
        $msg=$lang=='ar' ? 'تم اضافة المقاس بنجاح' : 'Size added successfully';
        return $this->apiResponseMessage(0,$msg,200);
    }

    /*
     * Edit Sizes
     */
    public function edit_size(Request $request,$Sizes_id)
    {
        $lang=$request->header('lang');
        $Sizes=Sizes::find($Sizes_id);
        $check=$this->not_found($Sizes,'المقاس','Size',$lang);
        if(isset($check))
        {
            return $check;
        }
        if(!$request->name_en)
        {
            $msg=$lang=='ar' ? 'من فضلك ادخل المقاس بالعربية' : 'Size in arabic is required';
            return $this->apiResponseMessage(0,$msg,200);
        }
        if(!$request->name_ar)
        {
            $msg=$lang=='ar' ? 'من فضلك ادخل المقاس بالانجليزية' : 'Size in english is required';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $Sizes->name_en=$request->name_en;
        $Sizes->name_ar=$request->name_ar;
        $Sizes->save();
        $msg=$lang=='ar' ? 'تم تعديل المقاس بنجاح' : 'Size edited successfully';
        return $this->apiResponseMessage(0,$msg,200);
    }

    /*
     * Delete Sizes
     */

    public function delete_size(Request $request,$Sizes_id)
    {
        $lang=$request->header('lang');
        $Sizes=Sizes::find($Sizes_id);
        $check=$this->not_found($Sizes,'المقاس','Size',$lang);
        if(isset($check))
        {
            return $check;
        }
        $Sizes->delete();
        $msg=$lang=='ar' ? 'تم حذف المقاس بنجاح' : 'Size deleted successfully';
        return $this->apiResponseMessage(0,$msg,200);
    }

    /*
     * single Sizes
     */

    public function single_Size(Request $request,$Sizes_id)
    {
        $lang=$request->header('lang');
        $Sizes=Sizes::find($Sizes_id);
        $check=$this->not_found($Sizes,'المقاس','Size',$lang);
        if(isset($check))
        {
            return $check;
        }

        return $this->apiResponseData(new SizeResource($Sizes),'success',200);
    }

}