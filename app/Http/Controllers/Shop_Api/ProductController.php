<?php

namespace App\Http\Controllers\Shop_Api;

use App\Http\Resources\ColorResource;
use App\Models\Category;
use App\Models\Currency;
use App\Models\Product_Currency;
use App\Models\Products;
use App\Models\Sizes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Resources\UserResource;
use App\Http\Resources\Shop\ProductResource;
use App\Http\Resources\CartResource;
use App\User;
use App\Models\Shop;
use App\Http\Controllers\Manage\BaseController;
use App\Models\color;
use App\Models\Product_details;
use App\Models\Product_details_price;
use App\Models\Price;
use App\Models\Product_detail_inventory;
use App\Models\Product_color;
use App\Models\Produc_view_user;
use App\Models\Product_Size;
use App\Models\Product_image;

class ProductController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    /*
     * Add new product to database
     */
    public function add_product(Request $request)
    {
        $user=Auth::user();
        $validate_product=$this->validate_product($request);
        if(isset($validate_product)){
            return $validate_product;
        }
        $product=new Products();
        $product->name_ar=$request->name_ar;
        $product->name_en=$request->name_en;
        $product->cat_id=$request->cat_id;
        // $product->shop_id=$user->id;
        $product->lat=$request->lat;
        $product->lng=$request->lng;
        $product->quantity=$request->quantity;
        $product->status=$request->status;
        $product->is_offer=$request->is_offer;
        $product->offer_amount=$request->offer_amount;
        $product->desc_ar=$request->desc_ar;
        $product->desc_en=$request->desc_en;
        $product->barcode=$request->barcode;
        if($request->image){
            $name=BaseController::saveImage('Products',$request->file('image'));
            $product->image=$name;
        }
        $product->save();
        $lang = $request->header('lang');
        // $this->addDefualPrice($product->id,$request->price);
        // BaseController::set_offer($product->id);
        $msg=$lang=='ar' ? 'تم اضافة المنتج بنجاح'  : 'product add successfully';
        return $this->apiResponseData(new ProductResource($product),$msg);
    }

    /*
     * GEt Products by cat id
     */
    public function product_by_cat_id(Request $request,$cat_id)
    {
        $Products=Products::where('cat_id',$request->cat_id)->orderBy('created_at','desc')->get();
        $lang = $request->header('lang');
        $msg=$lang=='ar' ? 'تمت العملية بنجاح'  : 'success';
        return $this->apiResponseData(ProductResource::collection($Products),$msg);
    }

    /*
     * Add Products Defualt Price price
     */
    public function addDefualPrice($product_id,$price){
        $currency=Currency::where('defualt',1)->first();
        $currency=is_null($currency) ? Currency::first() : $currency;
        $Product_Currency=new Product_Currency;
        $Product_Currency->product_id=$product_id;
        $Product_Currency->currency_id=$currency->id;
        $Product_Currency->price=$price;
        $Product_Currency->save();
    }
    /*
     * Edit product
    */
    public function edit_product(Request $request,$product_id)
    {
        $user=Auth::user();
        $lang=$request->header('lang');
        $validate_product=$this->validate_product($request);
        if(isset($validate_product)){
            return $validate_product;
        }
        $product=Products::find($product_id);
        $check=$this->not_found($product,'المنتج','Product',$lang);
        if(isset($check)){
            return $check;
        }
        $product->name_ar=$request->name_ar;
        $product->name_en=$request->name_en;
        $product->cat_id=$request->cat_id;
        $product->lat=$request->lat;
        $product->lng=$request->lng;
        $product->quantity=$request->quantity;
        $product->status=$request->status;
        $product->is_offer=$request->is_offer;
        $product->offer_amount=$request->offer_amount;
        $product->desc_ar=$request->desc_ar;
        $product->desc_en=$request->desc_en;
        $product->barcode=$request->barcode;
        if($request->image){
            BaseController::deleteFile('Products',$product->image);
            $name=BaseController::saveImage('Products',$request->file('image'));
            $product->image=$name;
        }
        $product->save();
        // BaseController::set_offer($product->id);
        $lang = $request->header('lang');
        $msg=$lang=='ar' ? 'تم تعديل المنتج بنجاح'  : 'product Edited successfully';
        return $this->apiResponseData(new ProductResource($product),$msg);

    }

    /*
     * Show single  product
     */
    public function single_product(Request $request,$product_id){
        $lang=$request->header('lang');
        $product=Products::find($product_id);
        $check=$this->not_found($product,'المنتج','Product',$lang);
        if(isset($check)){
            return $check;
        }
        $msg=$lang=='ar' ?'تمت العملية بنجاح' : 'success';
        return $this->apiResponseData(new ProductResource($product),$msg);
    }

    /*
     * Delete Product ..
     */

    public function delete_product(Request $request,$product_id){
        $lang=$request->header('lang');
        $product = Products::find($product_id);
        $check=$this->not_found($product,'المنتج','Product',$lang);
        if(isset($check)){
            return $check;
        }
        BaseController::deleteFile('Products',$product->iamge);
        // $product->delete();

        if (Product_details::where('product_id',$product_id)->exists()){
            $pro_det_ids = Product_details::where('product_id',$product_id)->pluck('id');
            $this->delete_product_details($pro_det_ids);
        }

        $product->delete();
        $msg=$lang=='ar' ? 'تم حذف المنتج بنجاح'  : 'product Deleted successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }

    public function delete_product_details ($pro_det_ids){
        foreach ($pro_det_ids as $Product_details_id){
            $Product_details=Product_details::where('id',$Product_details_id)->first();
            if(Product_details_price::where('product_detail_id', '=' ,$Product_details_id)->exists()){
                $Product_details_price = Product_details_price::where('product_detail_id', '=' ,$Product_details_id)->first();

                if(Product_details_price::where('product_detail_id', '=' ,$Product_details_id)->value('price_id') != null){
                    
                    $Price_id = Product_details_price::where('product_detail_id', '=' ,$Product_details_id)->value('price_id');
                    if(Price::where('id',$Price_id)->exists()){Price::where('id',$Price_id)->delete();}

                }
                
                $Product_details_price->delete();
            }
            
            if(Product_detail_inventory::where('product_detail_id' , '=' , $Product_details_id)->exists()){
                Product_detail_inventory::where('product_detail_id' , '=' , $Product_details_id)->delete();
            }

            if(Product_Currency::where('product_detail_id' , '=' , $Product_details_id)->exists()){
                Product_Currency::where('product_detail_id' , '=' , $Product_details_id)->delete();
            }

            if(Product_color::where('product_detail_id' , '=' , $Product_details_id)->exists()){
                Product_color::where('product_detail_id' , '=' , $Product_details_id)->delete();
            }

            if(Produc_view_user::where('product_detail_id' , '=' , $Product_details_id)->exists()){
                Produc_view_user::where('product_detail_id' , '=' , $Product_details_id)->delete();
            }

            if(Product_Size::where('product_detail_id' , '=' , $Product_details_id)->exists()){
                Product_Size::where('product_detail_id' , '=' , $Product_details_id)->delete();
            }

            if(Product_image::where('product_detail_id' , '=' , $Product_details_id)->exists()){
                Product_image::where('product_detail_id' , '=' , $Product_details_id)->delete();
            }

            BaseController::deleteFile('Product_details',$Product_details->iamge);
            foreach ($Product_details->images as $row)
            {
                BaseController::deleteFile('Product_image',$row->iamge);
            }
            
            $Product_details->delete();
        }
    }

    /*
     * get All product for Auth shop
     */
    public function all_products(Request $request)
    {
        $user=Auth::user();
        // $page=$request->page * 20;
        $products=Products::orderBy('id','desc')->get();
        return $this->apiResponseData(ProductResource::collection($products),'success');
    }

    /*
     * @pram $request
     * @return Error message or check if cateogry is null
     */

    private function validate_product($request){
        $lang = $request->header('lang');
        $input = $request->all();
        $validationMessages = [
            'name_ar.required' => $lang == 'ar' ?  'من فضلك ادخل رقم اسم المنتج بالعربية' :"name in arabic is required" ,
            'name_en.required' => $lang == 'ar' ? 'من فضلك ادخل رقم اسم المنتج بالانجليزية' :"name in english is required"  ,
            'image.required' => $lang == 'ar' ? 'من فضلك ادخل صورة المنتج ' :"image is required"  ,
            'cat_id.required' => $lang == 'ar' ?  'من فضلك ادخل القسم' :"category name  is required" ,
            'quantity.required' => $lang == 'ar' ?  'من فضلك ادخل الكمية' :"quantity is required" ,
            'image.image'=>$lang == 'ar' ?  'من فضلك ادخل صورة صحيحة' :"The image must be a valid image",
            'quantity.numeric' => $lang == 'ar' ?  'يجب ان تكون الكمية رقم صحيح' :"The quantity must be a number" ,
            'price.required' => $lang == 'ar' ? 'من فضلك ادخل سعر المنتج ' :"price is required"  ,
            'offer_amount.between'=>$lang == 'ar' ? 'نسبة الخصم يجب ان تكون من0 الي 100' : 'The offer amount must be between 0 and 100.',
            // 'inventory_id.required' => $lang == 'ar' ? 'من فضلك ادخل المخزن ' :"Inventory is required"  ,
            'barcode.required' => $lang == 'ar' ? 'من فضلك ادخل الباركود ' :"Barcode is required"  ,
        ];

        $validator = Validator::make($input, [
            'name_ar' => 'required',
            'name_en' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg|max:2048',
            'cat_id' => 'required',
            'quantity' => 'required|numeric',
            'price'=>'required',
            'offer_amount'=>'nullable|integer|between:0,100',
            // 'inventory_id' => 'required',
            'barcode' => 'required',
        ], $validationMessages);

        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 400);
        }
        $cat=Category::find($request->cat_id);
        $check=$this->not_found($cat,'القسم','Category',$lang);
        if(isset($check)){
            return $check;
        }
    }
}
