<?php

namespace App\Http\Controllers\Shop_Api;

use App\Http\Controllers\Manage\BaseController;
use App\Http\Resources\ColorResource;
use App\Models\Category;
use App\Models\Currency;
use App\Models\Product_Currency;
use App\Models\Product_image;
use App\Models\Product_Size;
use App\Models\Products;
use App\Models\Product_details;
use App\Models\Sizes;
use App\Models\Product_color;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Resources\UserResource;
use App\Http\Resources\Shop\ProductResource;
use App\Models\color;

class ProductDetialsController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;
    /*
     * Add color to product
     */
    public function add_color(Request $request,$product_id)
    {
        $lang=$request->header('lang');
        $product=Product_details::find($product_id);
        $color=color::find($request->color_id);
        $check=$this->not_found($product,'المنتج','product',$lang);
        if(isset($check))
        {
            return $check;
        }
        $check=$this->not_found($color,'اللون','color',$lang);
        if(isset($check))
        {
            return $check;
        }
        $Product_color=Product_color::where('product_detail_id',$product_id)->where('color_id',$request->color_id)->first();
        if(is_null($Product_color)){
            $Product_color=new Product_color;
            $Product_color->product_detail_id=$product_id;
            $Product_color->color_id=$request->color_id;
            $Product_color->save();
            $msg=$lang == 'ar' ? 'تم اضافة اللون بنجاح' : 'color added successfully';
        }else {
            $msg = $lang == 'ar' ? 'اللون موجود بالفعل لهذا المنتج' : 'color already exist';
        }
        return $this->apiResponseMessage(1,$msg,200);

    }

    /*
     * Remove Color From Products
     *
     */
    public function remove_color(Request $request,$product_id)
    {
        $lang=$request->header('lang');

        $Product_color=Product_color::where('product_detail_id',$product_id)->where('color_id',$request->color_id)->first();
        if(!is_null($Product_color)){
            $Product_color->delete();
            $msg=$lang == 'ar' ? 'تم حذف اللون بنجاح' : 'color deleted successfully';
        }else {
            $msg = $lang == 'ar' ? 'اللون غير موجود ' : 'color not exist';
        }
        return $this->apiResponseMessage(1,$msg,200);
    }

    /*
     * Add size to product
     */
    public function add_size(Request $request,$product_id)
    {
        $lang=$request->header('lang');
        $product=Product_details::find($product_id);
        $size=Sizes::find($request->size_id);
        $check=$this->not_found($product,'المنتج','product',$lang);
        if(isset($check))
        {
            return $check;
        }
        $check=$this->not_found($size,'الحجم','size',$lang);
        if(isset($check))
        {
            return $check;
        }
        $Product_Size=Product_Size::where('product_detail_id',$product_id)->where('size_id',$request->size_id)->first();
        if(is_null($Product_Size)){
            $Product_Size=new Product_Size;
            $Product_Size->product_detail_id=$product_id;
            $Product_Size->size_id=$request->size_id;
            $Product_Size->save();
            $msg=$lang == 'ar' ? 'تم اضافة الحجم بنجاح' : 'size added successfully';
        }else {
            $msg = $lang == 'ar' ? 'الحجم موجود بالفعل لهذا المنتج' : 'size already exist';
        }
        return $this->apiResponseMessage(1,$msg,200);

    }

    /*
     * Remove Size From Products
     *
     */
    public function remove_size(Request $request,$product_id)
    {
        $lang=$request->header('lang');

        $Product_Size=Product_Size::where('product_detail_id',$product_id)->where('size_id',$request->size_id)->first();
        if(!is_null($Product_Size)){
            $Product_Size->delete();
            $msg=$lang == 'ar' ? 'تم حذف الحجم بنجاح' : 'size deleted successfully';
        }else {
            $msg = $lang == 'ar' ? 'الحجم غير موجود ' : 'size not exist';
        }
        return $this->apiResponseMessage(1,$msg,200);
    }

    /*
     * Add image to product gallery
     */

    public function add_image(Request $request,$product_id)
    {
        $lang=$request->header('lang');
        $input = $request->all();
        $validationMessages = [
            'image.required' => $lang == 'ar' ?  'من فضلك ادخل الصورة' :"image is required" ,
            'image.image'=>$lang == 'ar' ?  'من فضلك ادخل صورة صحيحة' :"The image must be a valid image",
        ];

        $validator = Validator::make($input, [
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ], $validationMessages);

        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 200);
        }

        $product=Product_details::find($product_id);
        $check=$this->not_found($product,'المنتج','product',$lang);
        if(isset($check))
        {
            return $check;
        }
        $Product_image=new Product_image();
        $name=BaseController::saveImage('Product_image',$request->image);
        $Product_image->image=$name;
        $Product_image->product_detail_id=$product_id;
        $Product_image->save();
        $msg=$lang == 'ar' ? 'تم اضافة الصورة بنجاح' : 'image added successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }

    /*
     * remove image from product
     */

    public function remove_image(Request $request,$image_id)
    {
        $lang=$request->header('lang');
        $Product_image=Product_image::find($image_id);
        $check=$this->not_found($Product_image,'الصورة','image',$lang);
        if(isset($check))
        {
            return $check;
        }
        BaseController::deleteFile('Product_image',$Product_image->image);
        $Product_image->delete();
        $msg=$lang == 'ar' ? 'تم حذف الصورة بنجاح' : 'image deleted successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }
 
    /*
     * Add Price To Products
     */

    public function add_currency(Request $request,$product_id)
    {
        $lang=$request->header('lang');
        $product=Product_details::find($product_id);
        $Currency=Currency::find($request->currency_id);
        $check=$this->not_found($product,'المنتج','product',$lang);
        if(isset($check))
        {
            return $check;
        }
        $check=$this->not_found($Currency,'العملة','Currency',$lang);
        if(isset($check))
        {
            return $check;
        }
        if(!$request->price){
            $msg = $lang == 'ar' ? 'من فضلك ادخل السعر' : 'price is required';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $Product_Currency=Product_Currency::where('product_id',$product_id)->where('currency_id',$request->currency_id)->first();
        if(is_null($Product_Currency)){
            $Product_Currency=new Product_Currency;
            $Product_Currency->product_detail_id=$product_id;
            $Product_Currency->currency_id=$request->currency_id;
            $Product_Currency->price=$request->price;
            $Product_Currency->save();
            $msg=$lang == 'ar' ? 'تم اضافة العملة بنجاح' : 'Currency added successfully';
            return $this->apiResponseMessage(1,$msg,200);
        }else {
            $msg = $lang == 'ar' ? 'العملة موجودة بالفعل لهذا المنتج' : 'Currency already exist';
            return $this->apiResponseMessage(0,$msg,200);
        }

    }

    /*
     * return $this->apiResp
     * Remove currency From Products
    */
    public function remove_currency(Request $request,$product_id)
    {
        $lang=$request->header('lang');

        $Product_Currency=Product_Currency::where('product_detail_id',$product_id)->where('currency_id',$request->currency_id)->first();
        if(!is_null($Product_Currency)){
            $Product_Currency->delete();
            $msg=$lang == 'ar' ? 'تم حذف العملة بنجاح' : 'Currency deleted successfully';
        }else {
            $msg = $lang == 'ar' ? 'العملة غير موجود ' : 'Currency not exist';
        }
        return $this->apiResponseMessage(1,$msg,200);
    }
}