<?php

namespace App\Http\Controllers\Shop_Api;

use App\Models\Product_details_price;
use App\Models\Product_details;
use App\Models\Price;
use App\Models\Products;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Controllers\Manage\BaseController;
use App\Http\Resources\Product_details_priceResource;

class Product_details_priceController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    /*
     * Add new Product_details_price to database
     */
    public function add_Product_details_price(Request $request)
    {
        $lang = $request->header('lang');
        $user=Auth::user();
        $validate_Product_details=$this->validate_Product_details_price_add($request);
        if(isset($validate_Product_details)){
            return $validate_Product_details;
        }

        $product_detail_id = $request->product_detail_id;
        $product_detail_ex = Product_details::find($product_detail_id);
        $check=$this->not_found($product_detail_ex,'تفاصيل المنتج','Product detail',$lang);
        if(isset($check)){
            return $check;
        }
        
        $price_id = $request->price_id;
        $price_ex = Price::find($price_id);
        $check=$this->not_found($price_ex,'السعر','Price',$lang);
        if(isset($check)){
            return $check;
        }
        
        // $product_id = $request->product_id;
        // $proex = Products::find($product_id);
        // $check=$this->not_found($proex,'المنتج','Product',$lang);
        // if(isset($check)){
        //     return $check;
        // }

        // $product_details=new Product_details();
        // $product_details->product_id=$product_id;
        // $product_details->difference_en=$request->difference_en;
        // $product_details->difference_ar=$request->difference_ar;
        // $product_details->save();

        // $product_details_id = $product_details->id;

        // $Price=new Price();
        // $Price->purchasing_price=$request->purchasing_price;
        // $Price->wholesale_wholesale_price=$request->wholesale_wholesale_price;
        // $Price->wholesale_price=$request->wholesale_price;
        // $Price->selling_price=$request->selling_price;
        // $Price->save();

        // $Price_id = $Price->id;

        $Product_details_price=new Product_details_price();
        $Product_details_price->product_detail_id=$product_detail_id;
        $Product_details_price->price_id=$price_id;
        $Product_details_price->save();

        $lang = $request->header('lang');
        $msg = $lang=='ar' ? 'تم اضافة تفاصيل المنتج بنجاح'  : 'Product_details added successfully';
        return $this->apiResponseData(new Product_details_priceResource($Product_details_price),$msg);

    }


    /*
     * Edit product
    */
    public function edit_Product_details_price(Request $request,$Product_details_price_id)
    {
        $lang = $request->header('lang');
        $Product_details_price = Product_details_price::find($Product_details_price_id);
        $check=$this->not_found($Product_details_price,'السعر','Price',$lang);
        if(isset($check)){
            return $check;
        }

        $product_detail_id = $request->product_detail_id;
        $product_detail_ex = Product_details::find($product_detail_id);
        $check=$this->not_found($product_detail_ex,'تفاصيل المنتج','Product detail',$lang);
        if(isset($check)){
            return $check;
        }
        
        $price_id = $request->price_id;
        $price_ex = Price::find($price_id);
        $check=$this->not_found($price_ex,'السعر','Price',$lang);
        if(isset($check)){
            return $check;
        }
        
        $validate_Product_details_price=$this->validate_Product_details_price_add($request);
        if(isset($validate_Product_details_price)){
            return $validate_Product_details_price;
        }
        $Product_details_price->product_detail_id=$request->product_detail_id;
        $Product_details_price->price_id=$request->price_id;
        $Product_details_price->save();
        $lang = $request->header('lang');
        $msg=$lang=='ar' ? 'تم تعديل سعر تفاصيل المنتج بنجاح'  : 'Product_details_price edited successfully';
        return $this->apiResponseData(new Product_details_priceResource($Product_details_price),$msg);
    }


    /*
     * get All product for Auth shop
     */
    public function all_Product_details_price(Request $request)
    {
        $user=Auth::user();
        $page=$request->page * 20;
        $Product_details_price=Product_details_price::/*where('mainShop_id' , $request->mainShop_id)->*/orderBy('id','DESC')->get();
        return $this->apiResponseData(Product_details_priceResource::collection($Product_details_price),'success');
    }


    /*
     * Show single Product_details_price
     */
    public function single_Product_details_price(Request $request,$Product_details_price_id){
        $lang=$request->header('lang');
        $Product_details_price=Product_details_price::find($Product_details_price_id);
        $check=$this->not_found($Product_details_price,'تفاصيل المنتج','Product_details_price',$lang);
        if(isset($check)){
            return $check;
        }
        $msg=$lang=='ar' ?'تمت العملية بنجاح' : 'success';
        return $this->apiResponseData(new Product_details_priceResource($Product_details_price),$msg);
    }

    /*
     * Delete Product_details_price ..
     */

    public function delete_Product_details_price(Request $request,$Product_details_price_id){
        $lang=$request->header('lang');
        $Product_details_price=Product_details_price::where('id',$Product_details_price_id)->where('id','!=',1)->first();
        $check=$this->not_found($Product_details_price,'تفاصيل المنتج','Product_details_price',$lang);
        if(isset($check)){
            return $check;
        }
        
        $Product_details_price->delete();
        $msg=$lang=='ar' ? 'تم حذف تفاصيل المنتج بنجاح'  : 'Product_details_price Deleted successfully';
        
        return $this->apiResponseMessage(1,$msg,200);
    }


    /*
     * @pram $request
     * @return Error message or check if cateogry is null
     */

    private function validate_Product_details_price_add($request){
        $lang = $request->header('lang');
        $input = $request->all();
        $validationMessages = [
            'product_detail_id.required' => $lang == 'ar' ?  'من فضلك ادخل كود تفاصيل المنتج' :"product_detail_id is required" ,
            'price_id.required' => $lang == 'ar' ? 'من فضلك ادخل كود السعر' :"price_id is required"  ,
        ];
        $validator = Validator::make($input, [
            'product_detail_id' => 'required',
            'price_id' => 'required',
        ], $validationMessages);
        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 400);
        }
        
    }

    private function validate_Product_details_add($request){
        $lang = $request->header('lang');
        $input = $request->all();
        $validationMessages = [
            'product_id.required' => $lang == 'ar' ?  'من فضلك ادخل كود المنتج' :"product_id is required" ,
            'difference_ar.required' => $lang == 'ar' ? 'من فضلك ادخل الاختلاف تفاصيل المنتج بالعربية' :"Difference in english is required"  ,
            'difference_en.required' => $lang == 'ar' ? 'من فضلك ادخل الاختلاف تفاصيل المنتج بالانجليزية' :"Difference in english is required"  ,
        ];
        $validator = Validator::make($input, [
            'product_id' => 'required',
            'difference_ar' => 'required',
            'difference_en' => 'required',
        ], $validationMessages);
        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 400);
        }
        
    }


}
