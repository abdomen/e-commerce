<?php

namespace App\Http\Controllers\Shop_Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt,DB;
use App\Http\Controllers\Manage\BaseController;
use App\Models\Category;
use App\Models\Discount;
use App\Models\Order;
use App\Models\Products;
use App\Models\role;
use App\Models\Inventory;
use App\Models\Product_details;
use App\Models\P_d_orders;
use App\Models\Rep_Inv;
use App\Models\Order_product_details;
use App\Models\Representative_debt_log;
use App\Models\Admin_debt_log;
use App\Models\User_debt_log;
use App\Http\Resources\User_debt_logResource;
use App\Models\Price;
use App\Models\Product_details_price;
use App\Models\Safe_details;
use App\Models\Safe_transactions;
use App\User;
use App\Http\Resources\UserResource;
use App\Http\Resources\Shop\ProductResource;
use App\Http\Resources\Admin_debt_logResource;
use App\Http\Resources\Representative_debt_logResource;
use App\Http\Resources\Rep_selled_qtyResource;
use App\Http\Resources\Safe_detailsResource;
use App\Http\Resources\Safe_transactionsResource;
use Carbon\Carbon; 


class ReportsController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    public function get_counts(Request $request)
    {
        $user=DB::table('users')->select(DB::raw('count(id) as `data`'), DB::raw("DATE_FORMAT(created_at, '%m-%Y') new_date"),  DB::raw('YEAR(created_at) year, MONTH(created_at) month'))->groupby('year','month')->pluck('data')->toArray();
        $user_rate = array_map('intval', $user);
        //Order_rate
        $order=DB::table('orders_p_d')->select(DB::raw('count(id) as `data`'), DB::raw("DATE_FORMAT(created_at, '%m-%Y') new_date"),  DB::raw('YEAR(created_at) year, MONTH(created_at) month'))->groupby('year','month')->pluck('data')->toArray();
        $order_rate = array_map('intval', $order);

        //Products_rate
        $products=DB::table('products')->select(DB::raw('count(id) as `data`'), DB::raw("DATE_FORMAT(created_at, '%m-%Y') new_date"),  DB::raw('YEAR(created_at) year, MONTH(created_at) month'))->groupby('year','month')->pluck('data')->toArray();
        $Products_rate = array_map('intval', $products);

        //cats_rate
        $cats=DB::table('categories')->select(DB::raw('count(id) as `data`'), DB::raw("DATE_FORMAT(created_at, '%m-%Y') new_date"),  DB::raw('YEAR(created_at) year, MONTH(created_at) month'))->groupby('year','month')->pluck('data')->toArray();
        $cats_rate = array_map('intval', $cats);
        
        //codes_rate
        $codes=DB::table('discounts')->select(DB::raw('count(id) as `data`'), DB::raw("DATE_FORMAT(created_at, '%m-%Y') new_date"),  DB::raw('YEAR(created_at) year, MONTH(created_at) month'))->groupby('year','month')->pluck('data')->toArray();
        $codes_rate = array_map('intval', $codes);

        //inventories_count
        $invs=DB::table('inventory')->select(DB::raw('count(id) as `data`'), DB::raw("DATE_FORMAT(created_at, '%m-%Y') new_date"),  DB::raw('YEAR(created_at) year, MONTH(created_at) month'))->groupby('year','month')->pluck('data')->toArray();
        $invs_rate = array_map('intval', $invs);

        $user_count=User::where('status' , '!=' , '2')->count();
        $cat_count=Category::where('id','!=',1)->count();
        $inv_count=Inventory::count();
        $orders_count=P_d_orders::count();
        $products_count=Product_details::count();
        $discount_count=Discount::count();
        $rep_count=User::where('status','2')->count();
        $active_orders_count=P_d_orders::where('status' , '=' , '1')->count();
        $finished_orders_count=P_d_orders::where('status' , '=' , '2')->count();
        $day_orders_count=P_d_orders::whereDate('created_at', Carbon::now())->count();
        $data=['users_count'=>$user_count,'representatives_count'=>$rep_count,'categories_count'=>$cat_count,'inventories_count'=>$inv_count,
            'orders_count'=>$orders_count,'active_orders_count'=>$active_orders_count,'finished_orders_count'=>$finished_orders_count,'day_orders_count'=>$day_orders_count, 'products_count'=>$products_count,
            'discounts_count'=>$discount_count,'users_rates'=>$user_rate,'orders_rate'=>$order_rate,'Products_rate'=>$Products_rate,'cats_rate'=>$cats_rate,'inventories_rate'=>$invs_rate,'codes_rate'=>$codes_rate];
        return $this->apiResponseData($data,'success',200);
    }


///////////////////////////////////////////// DEBT REPORTS ///////////////////////////////////////////


    //Admin-Representative


    public function admin_rep_debt_log (Request $request){
        $log = Admin_debt_log::orderBy('id','DESC')->get();       
        return $this->apiResponseData(Admin_debt_logResource::collection($log),'success');
    }


    public function adminRepDebtLog_byDate(Request $request)
    {
        $from = date($request->from);
        $to = date($request->to);
        $log = Admin_debt_log::whereBetween('created_at', [$from." 00:00:00",$to." 23:59:59"])->orderBy('id','DESC')->get();
        return $this->apiResponseData(Admin_debt_logResource::collection($log),'success');
    }

    public function rep_debt_log (Request $request , $representative_id){
        $log = Admin_debt_log::where('representative_id' , $representative_id)->orderBy('id','DESC')->get();       
        return $this->apiResponseData(Admin_debt_logResource::collection($log),'success');
    }

    public function admin_debt_log (Request $request , $admin_id){
        $log = Admin_debt_log::where('admin_id' , $admin_id)->orderBy('id','DESC')->get();       
        return $this->apiResponseData(Admin_debt_logResource::collection($log),'success');
    }

    //Admin-User

    public function admin_user_debt_log (Request $request){
        $log = User_debt_log::orderBy('id','DESC')->get();       
        return $this->apiResponseData(User_debt_logResource::collection($log),'success');
    }


    public function adminuserDebtLog_byDate(Request $request)
    {
        $from = date($request->from);
        $to = date($request->to);
        $log = User_debt_log::whereBetween('created_at', [$from." 00:00:00",$to." 23:59:59"])->orderBy('id','DESC')->get();
        return $this->apiResponseData(User_debt_logResource::collection($log),'success');
    }

    public function admin_user_debt_log_by_user (Request $request , $user_id){
        $log = User_debt_log::where('user_id' , $user_id)->orderBy('id','DESC')->get();       
        return $this->apiResponseData(User_debt_logResource::collection($log),'success');
    }

    public function admin_user_debt_log_by_admin (Request $request , $admin_id){
        $log = User_debt_log::where('admin_id' , $admin_id)->orderBy('id','DESC')->get();       
        return $this->apiResponseData(User_debt_logResource::collection($log),'success');
    }


    //Representative-User

    public function rep_user_debt_log (Request $request){
        $log = Representative_debt_log::orderBy('id','DESC')->get();       
        return $this->apiResponseData(Representative_debt_logResource::collection($log),'success');
    }


    public function repUserDebtLog_byDate(Request $request)
    {
        $from = date($request->from);
        $to = date($request->to);
        $log = Representative_debt_log::whereBetween('created_at', [$from." 00:00:00",$to." 23:59:59"])->orderBy('id','DESC')->get();
        return $this->apiResponseData(Representative_debt_logResource::collection($log),'success');
    }

    public function user_debt_log (Request $request , $user_id){
        $log = Representative_debt_log::where('user_id' , $user_id)->orderBy('id','DESC')->get();       
        return $this->apiResponseData(Representative_debt_logResource::collection($log),'success');
    }

    public function repUser_debt_log (Request $request , $representative_id){
        $log = Representative_debt_log::where('representative_id' , $representative_id)->orderBy('id','DESC')->get();       
        return $this->apiResponseData(Representative_debt_logResource::collection($log),'success');
    }
    

    public function rep_selled_quantity (Request $request , $representative_id){
        $inv_id = Rep_Inv::where('representative_id' , $representative_id)->value('inventory_id');
        $data = Order_product_details::select('product_detail_id','quantity')->where('inventory_id' , $inv_id)->orderBy('id','DESC')->get();
        // return $data;
        return $this->apiResponseData(Rep_selled_qtyResource::collection($data),'success');
    }
    


///////////////////////////////////////////// PROFIT REPORTS ///////////////////////////////////////////


    public function profit_report (Request $request){

        $lang = $request->header('lang');

        $ids = Product_details::pluck('id');

        foreach($ids as $id){
            $sumSell = 0;
            $sumProfit = 0;
            $rows = Order_product_details::where('product_detail_id',$id)->get();
            foreach($rows as $row){
                $pur_price = Price::where('id',Product_details_price::where('product_detail_id' , $id)->value('price_id'))->value('purchasing_price');
                $sell = $row->quantity * $row->price;
                $profit = $sell - ($pur_price*$row->quantity);
                $sumSell += $sell;
                $sumProfit += $profit;
            }
            $m_p_id = Product_details::where('id',$id)->value('product_id');
            $name = $lang == 'ar' ? (string)Products::where('id',$m_p_id)->value('name_ar') : (string)Products::where('id',$m_p_id)->value('name_en');
            $diff = $lang == 'ar' ? (string)Product_details::where('id',$id)->value('difference_ar') : (string)Product_details::where('id',$id)->value('difference_en');
            $purch_price = Price::where('id',Product_details_price::where('product_detail_id' , $id)->value('price_id'))->value('purchasing_price');
            $qty = DB::table('order_product_details')->where('order_product_details.product_detail_id', '=', $id)->sum('order_product_details.quantity');
            $cost = $purch_price*$qty;

            $data[] = ['product_id'=>(int)$id,'name'=>(string)$name,'difference'=>(string)$diff,'purchasing_price'=>(double)$cost,'sell'=>(double)$sumSell,'profit'=>(double)$sumProfit,'selled_qty'=>(double)$qty];
        }

        $profitarr = array();
        foreach ($data as $key => $row)
        {
            $profitarr[$key] = $row['profit'];
        }
        array_multisort($profitarr, SORT_DESC, $data);
        
        return $this->apiResponseData($data,'success');
    }


    public function profit_report_byDate (Request $request){

        $lang = $request->header('lang');

        $from = date($request->from);
        $to = date($request->to);

        $ids = Product_details::pluck('id');

        foreach($ids as $id){
            $sumSell = 0;
            $sumProfit = 0;
            $rows = Order_product_details::where('product_detail_id',$id)->whereBetween('created_at', [$from." 00:00:00",$to." 23:59:59"])->get();
            foreach($rows as $row){
                $pur_price = Price::where('id',Product_details_price::where('product_detail_id' , $id)->value('price_id'))->value('purchasing_price');
                $sell = $row->quantity * $row->price;
                $profit = $sell - ($pur_price*$row->quantity);
                $sumSell += $sell;
                $sumProfit += $profit;
            }
            $m_p_id = Product_details::where('id',$id)->value('product_id');
            $name = $lang == 'ar' ? (string)Products::where('id',$m_p_id)->value('name_ar') : (string)Products::where('id',$m_p_id)->value('name_en');
            $diff = $lang == 'ar' ? (string)Product_details::where('id',$id)->value('difference_ar') : (string)Product_details::where('id',$id)->value('difference_en');
            $purch_price = Price::where('id',Product_details_price::where('product_detail_id' , $id)->value('price_id'))->value('purchasing_price');
            $qty = DB::table('order_product_details')->where('order_product_details.product_detail_id', '=', $id)->whereBetween('order_product_details.created_at', [$from." 00:00:00",$to." 23:59:59"])->sum('order_product_details.quantity');
            $cost = $purch_price*$qty;

            $data[] = ['product_id'=>(int)$id,'name'=>(string)$name,'difference'=>(string)$diff,'purchasing_price'=>(double)$cost,'sell'=>(double)$sumSell,'profit'=>(double)$sumProfit,'selled_qty'=>(double)$qty];
        }

        $profitarr = array();
        foreach ($data as $key => $row)
        {
            $profitarr[$key] = $row['profit'];
        }
        array_multisort($profitarr, SORT_DESC, $data);
        
        return $this->apiResponseData($data,'success');
    }


    public function total_profit (Request $request){

        $lang = $request->header('lang');

        $ids = Product_details::pluck('id');

        $total_profit = 0;
        $total_cost = 0;
        $total_sell = 0;
        $day_total_profit = 0;
        $day_total_cost = 0;
        $day_total_sell = 0;

        foreach($ids as $id){
            $day_sumSell = 0;
            $day_sumProfit = 0;
            $day_rows = Order_product_details::where('product_detail_id',$id)->whereDate('created_at', Carbon::now())->get();
            foreach($day_rows as $day_row){
                $day_pur_price = Price::where('id',Product_details_price::where('product_detail_id' , $id)->value('price_id'))->value('purchasing_price');
                $day_sell = $day_row->quantity * $day_row->price;
                $day_profit = $day_sell - ($day_pur_price*$day_row->quantity);
                $day_sumSell += $day_sell;
                $day_sumProfit += $day_profit;
            }
            $day_purch_price = Price::where('id',Product_details_price::where('product_detail_id' , $id)->value('price_id'))->value('purchasing_price');
            $day_qty = DB::table('order_product_details')->where('order_product_details.product_detail_id', '=', $id)->whereDate('created_at', Carbon::now())->sum('order_product_details.quantity');
            $day_cost = $day_purch_price*$day_qty;
            $day_total_cost += $day_cost;
            $day_total_sell += $day_sumSell;
            $day_total_profit += $day_sumProfit;
        }

        foreach($ids as $id){
            $sumSell = 0;
            $sumProfit = 0;
            $rows = Order_product_details::where('product_detail_id',$id)->get();
            foreach($rows as $row){
                $pur_price = Price::where('id',Product_details_price::where('product_detail_id' , $id)->value('price_id'))->value('purchasing_price');
                $sell = $row->quantity * $row->price;
                $profit = $sell - ($pur_price*$row->quantity);
                $sumSell += $sell;
                $sumProfit += $profit;
            }
            $purch_price = Price::where('id',Product_details_price::where('product_detail_id' , $id)->value('price_id'))->value('purchasing_price');
            $qty = DB::table('order_product_details')->where('order_product_details.product_detail_id', '=', $id)->sum('order_product_details.quantity');
            $cost = $purch_price*$qty;
            $total_cost += $cost;
            $total_sell += $sumSell;
            $total_profit += $sumProfit;
        }

        $data = ['today_total_cost'=>(double)$day_total_cost,'today_total_sell'=>(double)$day_total_sell,'today_total_profit'=>(double)$day_total_profit,
                 'total_cost'=>(double)$total_cost,'total_sell'=>(double)$total_sell,'total_profit'=>(double)$total_profit];
        
        return $this->apiResponseData($data,'success');
    }


    public function total_profit_byDate (Request $request){

        $lang = $request->header('lang');

        $from = date($request->from);
        $to = date($request->to);

        $ids = Product_details::pluck('id');
        $total_profit = 0;
        $total_cost = 0;
        $total_sell = 0;

        foreach($ids as $id){
            $sumSell = 0;
            $sumProfit = 0;
            $rows = Order_product_details::where('product_detail_id',$id)->whereBetween('created_at', [$from." 00:00:00",$to." 23:59:59"])->get();
            foreach($rows as $row){
                $pur_price = Price::where('id',Product_details_price::where('product_detail_id' , $id)->value('price_id'))->value('purchasing_price');
                $sell = $row->quantity * $row->price;
                $profit = $sell - ($pur_price*$row->quantity);
                $sumSell += $sell;
                $sumProfit += $profit;
            }
            $purch_price = Price::where('id',Product_details_price::where('product_detail_id' , $id)->value('price_id'))->value('purchasing_price');
            $qty = DB::table('order_product_details')->where('order_product_details.product_detail_id', '=', $id)->whereBetween('order_product_details.created_at', [$from." 00:00:00",$to." 23:59:59"])->sum('order_product_details.quantity');
            $cost = $purch_price*$qty;
            $total_cost += $cost;
            $total_sell += $sumSell;
            $total_profit += $sumProfit;
        }

        $data = ['total_cost'=>(double)$total_cost,'total_sell'=>(double)$total_sell,'total_profit'=>(double)$total_profit];
        
        return $this->apiResponseData($data,'success');
    }


//////////////////////////////////////////////// SAFE REPORTS ///////////////////////////////////////

                        ////////////// SAFES DETAILS //////////////

    /*
     * safe processes report
     */
    public function safes_log (Request $request){
        $report = Safe_details::orderBy('id','DESC')->get();
        $total_money = Safe_details::sum('value');
        return response()->json(['status'=>1 , 'data' => Safe_detailsResource::collection($report),'total money' => $total_money,'message' => 'success']);
    }



    /*
     * filter safe processes report by date
     */
    public function safes_log_byDate(Request $request)
    {
        $from = date($request->from);
        $to = date($request->to);
        $report = Safe_details::whereBetween('created_at', [$from." 00:00:00",$to." 23:59:59"])->orderBy('id','DESC')->get();
        $total_money = Safe_details::whereBetween('created_at', [$from." 00:00:00",$to." 23:59:59"])->sum('value');
        return response()->json(['status'=>1 , 'data' => Safe_detailsResource::collection($report),'total money' => $total_money,'message' => 'success']);
    }


    /*
     * one safe processes report
     */
    public function safes_log_bySafeID (Request $request,$safe_id){
        $report = Safe_details::where('safe_id',$safe_id)->orderBy('id','DESC')->get();       
        $total_money = Safe_details::where('safe_id',$safe_id)->sum('value');
        return response()->json(['status'=>1 , 'data' => Safe_detailsResource::collection($report),'total money' => $total_money,'message' => 'success']);    }


    /*
     * filter one safe processes report by date
     */
    public function safes_log_bySafeID_byDate(Request $request,$safe_id)
    {
        $from = date($request->from);
        $to = date($request->to);
        $report = Safe_details::where('safe_id',$safe_id)->whereBetween('created_at', [$from." 00:00:00",$to." 23:59:59"])->orderBy('id','DESC')->get();
        $total_money = Safe_details::where('safe_id',$safe_id)->whereBetween('created_at', [$from." 00:00:00",$to." 23:59:59"])->sum('value');
        return response()->json(['status'=>1 , 'data' => Safe_detailsResource::collection($report),'total money' => $total_money,'message' => 'success']);
    }


                        ////////////// SAFES TRANSACTIONS //////////////

    /*
     * safe processes report
     */
    public function safes_transactions_log (Request $request){
        $report = Safe_transactions::orderBy('id','DESC')->get();
        $total_money = Safe_transactions::sum('value');
        return response()->json(['status'=>1 , 'data' => Safe_transactionsResource::collection($report),'total money' => $total_money,'message' => 'success']);
    }



    /*
     * filter safe processes report by date
     */
    public function safes_transactions_byDate(Request $request)
    {
        $from = date($request->from);
        $to = date($request->to);
        $report = Safe_transactions::whereBetween('created_at', [$from." 00:00:00",$to." 23:59:59"])->orderBy('id','DESC')->get();
        $total_money = Safe_transactions::whereBetween('created_at', [$from." 00:00:00",$to." 23:59:59"])->sum('value');
        return response()->json(['status'=>1 , 'data' => Safe_transactionsResource::collection($report),'total money' => $total_money,'message' => 'success']);
    }


    /*
     * one safe processes report
     */
    public function safes_transactions_bySafeID (Request $request,$safe_id){
        $report = Safe_transactions::where('safe_from',$safe_id)->orWhere('safe_to',$safe_id)->orderBy('id','DESC')->get();       
        $total_money = Safe_transactions::where('safe_from',$safe_id)->orWhere('safe_to',$safe_id)->sum('value');
        return response()->json(['status'=>1 , 'data' => Safe_transactionsResource::collection($report),'total money' => $total_money,'message' => 'success']);    }


    /*
     * filter one safe processes report by date
     */
    public function safes_transactions_bySafeID_byDate(Request $request,$safe_id)
    {
        $from = date($request->from);
        $to = date($request->to);
        $report = Safe_transactions::where('safe_from',$safe_id)->orWhere('safe_to',$safe_id)->whereBetween('created_at', [$from." 00:00:00",$to." 23:59:59"])->orderBy('id','DESC')->get();
        $total_money = Safe_transactions::where('safe_from',$safe_id)->orWhere('safe_to',$safe_id)->whereBetween('created_at', [$from." 00:00:00",$to." 23:59:59"])->sum('value');
        return response()->json(['status'=>1 , 'data' => Safe_transactionsResource::collection($report),'total money' => $total_money,'message' => 'success']);
    }
}