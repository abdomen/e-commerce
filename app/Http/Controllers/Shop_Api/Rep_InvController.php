<?php

namespace App\Http\Controllers\Shop_Api;

use App\Models\Rep_Inv;
use App\User;
use App\Models\Inventory;
use App\Models\Safe;
use App\Models\Safe_details;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Controllers\Manage\BaseController;
use App\Http\Resources\Rep_InvResource;
use App\Http\Resources\RepShowResource;
use App\Http\Resources\UserResource;
use App\Http\Resources\singleRepResource;

class Rep_InvController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    /*
     * Add new Rep_Inv to database
     */
    public function add_representative(Request $request)
    {
        $lang = $request->header('lang');
        $user=Auth::user();
        
        $validate_Representative=$this->validate_Representative($request);
        if(isset($validate_Representative))
        {
            return $validate_Representative;
        }
        $validate_Inventory=$this->validate_Inventory($request);
        if(isset($validate_Inventory))
        {
            return $validate_Inventory;
        }

        $N_user = new User();
        $N_user->phone = $request->phone;
        $N_user->email = $request->email;
        $N_user->first_name = $request->first_name;
        $N_user->last_name = $request->last_name;
        $N_user->lat  = $request->lat;
        $N_user->lng  = $request->lng;
        $N_user->status  = 2;
        $N_user->shop_id = $user->id;
        $N_user->social  = 0;
        $N_user->lang=$lang;
        $N_user->password = Hash::make($request->password);
        if($request->image)
        {
            $name=BaseController::saveImage('users',$request->file('image'));
            $N_user->image=$name;
        }
        // Add Rep Safe //
        $safe = new Safe;
        $safe->name_ar = "خزنة $request->first_name $request->last_name";
        $safe->name_en = "$request->first_name $request->last_name Safe";
        $safe->status = 1;
        $safe->total_money = 0;
        $safe->save();
        //////////////////
        $N_user->safe_id = $safe->id;
        $N_user->save();
        $N_user['token']=null;
        
        $Inventory=new Inventory();
        $Inventory->name_ar=$request->inv_name_ar;
        $Inventory->name_en=$request->inv_name_en;
        $Inventory->address_ar=$request->inv_address_ar;
        $Inventory->address_en=$request->inv_address_en;
        $Inventory->phone=$request->inv_phone;
        $Inventory->lng=$request->inv_lng;
        $Inventory->lat=$request->inv_lat;
        $Inventory->save();

        $Rep_Inv=new Rep_Inv();
        $Rep_Inv->representative_id=$N_user->id;
        $Rep_Inv->inventory_id=$Inventory->id;
        $Rep_Inv->car_num=$request->car_num;
        $Rep_Inv->save();
        $lang = $request->header('lang');
        $msg = $lang=='ar' ? 'تم اضافةالمندوب بنجاح'  : 'Representative added successfully';
        return $this->apiResponseData(new Rep_InvResource($Rep_Inv),$msg);
    }


    /*
     * Edit
    */
    public function edit_representative(Request $request,$representative_id)
    {
        $lang = $request->header('lang');
        $N_user = User::find($representative_id);
        $check=$this->not_found($N_user,'المندوب','Representative',$lang);
        if(isset($check))
        {
            return $check;
        }

        $Rep_Inv = Rep_Inv::where('representative_id' , '=' , $representative_id)->first();
        if ($Rep_Inv){
            // $check=$this->not_found($Rep_Inv,'مخزن المندوب','Representative inventory',$lang);
            // if(isset($check))
            // {
            //     return $check;
            // }

            $Inv_id = Rep_Inv::where('representative_id' , '=' , $representative_id)->value('inventory_id');

            $Inventory = Inventory::where('id' , '=' , $Inv_id)->first();
            
            $N_user->phone = $request->phone;
            $N_user->email = $request->email;
            $N_user->first_name = $request->first_name;
            $N_user->last_name = $request->last_name;
            $N_user->lat  = $request->lat;
            $N_user->lng  = $request->lng;
            $N_user->status  = 2;
            $N_user->social  = 0;
            $N_user->lang=$lang;
            if ($request->filled('password')){
                $N_user->password = Hash::make($request->password);
            }else{
                $N_user->password = $N_user->password;
            }
            if($request->image)
            {
                $name=BaseController::saveImage('users',$request->file('image'));
                $N_user->image=$name;
            }
            $N_user->save();

            $Inventory->name_ar=$request->inv_name_ar;
            $Inventory->name_en=$request->inv_name_en;
            $Inventory->address_ar=$request->inv_address_ar;
            $Inventory->address_en=$request->inv_address_en;
            $Inventory->phone=$request->inv_phone;
            $Inventory->lng=$request->inv_lng;
            $Inventory->lat=$request->inv_lat;
            $Inventory->save();

            $Rep_Inv->car_num=$request->car_num;
            $Rep_Inv->save();

            $lang = $request->header('lang');
            $msg=$lang=='ar' ? 'تم تعديل المندوب بنجاح'  : 'Representative edited successfully';
            return $this->apiResponseData(new Rep_InvResource($Rep_Inv),$msg);
        }else{
            $N_user->phone = $request->phone;
            $N_user->email = $request->email;
            $N_user->first_name = $request->first_name;
            $N_user->last_name = $request->last_name;
            $N_user->lat  = $request->lat;
            $N_user->lng  = $request->lng;
            $N_user->status  = 2;
            $N_user->social  = 0;
            $N_user->lang=$lang;
            $N_user->password = Hash::make($request->password);
            if($request->image)
            {
                $name=BaseController::saveImage('users',$request->file('image'));
                $N_user->image=$name;
            }
            $N_user->save();
    
            $Inventory=new Inventory();
            $Inventory->name_ar=$request->inv_name_ar;
            $Inventory->name_en=$request->inv_name_en;
            $Inventory->address_ar=$request->inv_address_ar;
            $Inventory->address_en=$request->inv_address_en;
            $Inventory->phone=$request->inv_phone;
            $Inventory->lng=$request->inv_lng;
            $Inventory->lat=$request->inv_lat;
            $Inventory->save();

            $Rep_Inv=new Rep_Inv();
            $Rep_Inv->representative_id=$N_user->id;
            $Rep_Inv->inventory_id=$Inventory->id;
            $Rep_Inv->car_num=$request->car_num;
            $Rep_Inv->save();
    
            $lang = $request->header('lang');
            $msg=$lang=='ar' ? 'تم تعديل المندوب بنجاح'  : 'Representative edited successfully';
            return $this->apiResponseData(new Rep_InvResource($Rep_Inv),$msg);
        }
    }


    


    /*
     * get All product for Auth shop
     */
    public function all_representative(Request $request)
    {
        $user=Auth::user();
        $Rep_Inv=User::where('status' , '=', '2')->orderBy('id','DESC')->get();
        // return $Rep_Inv;
        return $this->apiResponseData(RepShowResource::collection($Rep_Inv),'success');
    }


    /*
     * Show single Rep_Inv
     */
    public function single_representative(Request $request,$representative_id)
    {
        $lang=$request->header('lang');
        $Rep_Inv = User::where('id' , '=' , $representative_id)->first();
        $check=$this->not_found($Rep_Inv,'المندوب','Representative',$lang);
        if(isset($check))
        {
            return $check;
        }
        $msg=$lang=='ar' ?'تمت العملية بنجاح' : 'success';
        return $this->apiResponseData(new RepShowResource($Rep_Inv),$msg);
    }

    
    public function single_inv_representative(Request $request,$representative_id)
    {
        $lang=$request->header('lang');
        $Rep_Inv = User::where('id' , '=' , $representative_id)->first();
        $check=$this->not_found($Rep_Inv,'المندوب','Representative',$lang);
        if(isset($check))
        {
            return $check;
        }
        $msg=$lang=='ar' ?'تمت العملية بنجاح' : 'success';
        return $this->apiResponseData(new singleRepResource($Rep_Inv),$msg);
    }
    /*
     * Delete Rep_Inv ..
     */

    public function delete_Representative(Request $request,$representative_id)
    {
        $lang=$request->header('lang');

        if(User::where('id',$representative_id)->exists()){
            if(Rep_Inv::where('representative_id' , '=' , $representative_id)->value('inventory_id') != null){
                $Inv_id = Rep_Inv::where('representative_id' , '=' , $representative_id)->value('inventory_id');
                if(Inventory::where('id' , '=' , $Inv_id)->exists()){
                    Inventory::where('id' , '=' , $Inv_id)->delete();
                }
                Rep_Inv::where('representative_id',$representative_id)->delete();
            }
        }
        User::find($representative_id)->delete();
        
        $msg=$lang=='ar' ? 'تم حذف المندوب'  : 'Representative Deleted successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }


    /*
     * @pram $request
     * @return Error message or check if cateogry is null
     */

    private function validate_Rep_Inv($request)
    {
        $lang = $request->header('lang');
        $input = $request->all();
        $validationMessages = [
            'representative_id.required' => $lang == 'ar' ?  'من فضلك ادخل كود المندوب' :"Representative_id is required" ,
            'inventory_id.required' => $lang == 'ar' ? 'من فضلك ادخل كود المخزن' :"Inventory_id is required"  ,
        ];
        $validator = Validator::make($input, [
            'representative_id' => 'required',
            'inventory_id' => 'required',
        ], $validationMessages);
        if ($validator->fails())
        {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 400);
        }
        
    }

    private function validate_Inventory($request)
    {
        $lang = $request->header('lang');
        $input = $request->all();
        $validationMessages = [
            'inv_name_ar.required' => $lang == 'ar' ?  'من فضلك ادخل اسم المخزن بالعربية' :"name in arabic is required" ,
            'inv_name_en.required' => $lang == 'ar' ? 'من فضلك ادخل اسم المخزن بالانجليزية' :"name in english is required"  ,
            'lng.required' => $lang == 'ar' ? 'من فضلك ادخل الموقع' :"Location is required" ,
            'lat.required' => $lang == 'ar' ? 'من فضلك ادخل الموقع' :"Location is required" ,       
        ];
        $validator = Validator::make($input, [
            'inv_name_ar' => 'required',
            'inv_name_en' => 'required',
            'lng' => 'required',
            'lat' => 'required',
        ], $validationMessages);
        if ($validator->fails())
        {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 400);
        }
        
    }


    
    private function validate_Representative($request)
    {
        $lang = $request->header('lang');
        $input = $request->all();
        $validationMessages = [
            'first_name.required' => $lang == 'ar' ?  'من فضلك ادخل رقم الاسم الاول' :"frist name is required" ,
            'password.required' => $lang == 'ar' ? 'من فضلك ادخل كلمة السر' :"password is required"  ,
            'email.required' => $lang == 'ar' ? 'من فضلك ادخل البريد الالكتروني' :"email is required"  ,
            'email.unique' => $lang == 'ar' ? 'هذا البريد الالكتروني موجود لدينا بالفعل' :"email is already teken" ,
            'email.regex'=>$lang=='ar'? 'من فضلك ادخل بريد الكتروني صالح' : 'The email must be a valid email address',
            'phone.required' => $lang == 'ar' ? 'من فضلك ادخل البريد رقم الهاتف' :"phone is required"  ,
            'phone.unique' => $lang == 'ar' ? 'رقم الهاتف موجود لدينا بالفعل' :"phone is already teken" ,
            'last_name.required' => $lang == 'ar' ?  'من فضلك ادخل رقم اسم العائلة' :"last name name is required" ,
            'phone.min' => $lang == 'ar' ?  'رقم الهاتف يجب ان لا يقل عن 7 ارقام' :"The phone must be at least 7 numbers" ,
            'phone.numeric' => $lang == 'ar' ?  'رقم الهاتف يجب ان يكون رقما' :"The phone must be a number" ,
            'car_num.numeric' => $lang == 'ar' ?  'ادخل رقم السيارة' :"add the car number" ,
        ];

        $validator = Validator::make($input, [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required|unique:users|numeric|min:7',
            'email' => 'required|unique:users|regex:/(.+)@(.+)\.(.+)/i',
            'password' => 'required',
            'car_num' => 'required',
        ], $validationMessages);

        if ($validator->fails())
        {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 400);
        }
        
    }
}