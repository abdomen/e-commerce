<?php

namespace App\Http\Controllers\Shop_Api;

use App\Http\Controllers\Api\NotificationMethods;
use App\Models\Group_discount;
use App\Models\Shop;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Resources\Group_discountResource;
use App\Http\Controllers\Manage\BaseController;


class Group_discountController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    /*
     * get all Group_discounts
     */
    public function all_Group_discount (Request $request){
        $user=Auth::user();
        $page=$request->page * 20;
        $Group_discounts=Group_discount::where('mainShop_id' , $user->id)->orderBy('id','desc')->get();
        return $this->apiResponseData(Group_discountResource::collection($Group_discounts),'success');
    }


    /*
    * Delete Group_discounts
    */
    public function delete_Group_discount(Request $request,$Group_discount_id)
    {
        $lang=$request->header('lang');
        $Group_discount=Group_discount::find($Group_discount_id);
        $check=$this->not_found($Group_discount,'الخصم','Group_discount',$lang);
        if(isset($check)){
            return $check;
        }
        $Group_discount->delete();
        $msg=$lang=='ar' ? 'تم حذف الخصم بنجاح'  : 'Group_discount Deleted successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }



    /*
     * Single Group_discount to show data
     */
    public function single_Group_discount(Request $request,$Group_discount_id)
    {
        $lang=$request->header('lang');
        $Group_discount=Group_discount::find($Group_discount_id);
        $check=$this->not_found($Group_discount,'الخصم','Group_discount',$lang);
        if(isset($check)){
            return $check;
        }
        return $this->apiResponseData(new Group_discountResource($Group_discount),'success');
    }

    /*
     * Add new Group_discount
     * add general Group_discount column , add products to this Group_discount
     */

    public function add_Group_discount(Request $request)
    {
        
        $lang=$request->header('lang');
        $user=Auth::user();
        $check=$this->not_found($user,'العضو','user',200);
        if(isset($check))
        {
            return $check;
        }
        $validate_Group_discount=$this->validate_Group_discount($request);
        if(isset($validate_Group_discount)){
            return $validate_Group_discount;
        }

        $Group_discount=new Group_discount();
        $Group_discount->group_id=$request->group_id;
        $Group_discount->name_en=$request->name_en;
        $Group_discount->name_ar=$request->name_ar;
        $Group_discount->desc_ar=$request->desc_ar;
        $Group_discount->desc_en=$request->desc_en;
        $Group_discount->discount_type=$request->discount_type;
        $Group_discount->discount_value=$request->discount_value;
        $Group_discount->end_date=$request->end_date;
        $Group_discount->mainShop_id=$user->id;
        $Group_discount->save();
        $msg=$lang=='ar' ?  'تم تسجيل الخصم بنجاح' :'Group_discount saved successfully' ;
        return $this->apiResponseData(new Group_discountResource($Group_discount),$msg);
    }



    /*
     * @pram Group_discount column ,
     * @return success or validate
     */

    public function edit_Group_discount(Request $request,$Group_discount_id)
    {
        $lang=$request->header('lang');
        $Group_discount=Group_discount::find($Group_discount_id);
        $check=$this->not_found($Group_discount,'الخصم','Group_discount',$lang);
        if(isset($check)){
            return $check;
        }
        $validate_Group_discount=$this->validate_Group_discount($request);
        if(isset($validate_Group_discount)){
            return $validate_Group_discount;
        }

        $Group_discount->group_id=$request->group_id;
        $Group_discount->name_en=$request->name_en;
        $Group_discount->name_ar=$request->name_ar;
        $Group_discount->desc_ar=$request->desc_ar;
        $Group_discount->desc_en=$request->desc_en;
        $Group_discount->discount_type=$request->discount_type;
        $Group_discount->discount_value=$request->discount_value;
        $Group_discount->end_date=$request->end_date;
        $Group_discount->save();
        $msg=$lang=='ar' ? 'تم تعديل الخصم بنجاح'  : 'Group_discount edited successfully';
        return $this->apiResponseData(new Group_discountResource($Group_discount),$msg);
    }


    /*
     * @pram $request
     * @return Error message or check if user is null
    */

    private function validate_Group_discount($request){
        $lang = $request->header('lang');
        $input = $request->all();
        $validationMessages = [
            'group_id.required' => $lang == 'ar' ?  'من فضلك ادخل رقم المجموعة' :"group_id in arabic is required" ,
            'name_en.required' => $lang == 'ar' ? 'من فضلك ادخل الاسم بالانجليزية' :"name in english is required"  ,
            'name_ar.required' => $lang == 'ar' ? 'من فضلك ادخل الاسم باللغة العربية' :"name in arabic is required"  ,
            'discount_type.required' => $lang == 'ar' ? 'من فضلك ادخل نوع الخصم' :"discount type is required"  ,
            'discount_value.required' => $lang == 'ar' ? 'من فضلك ادخل قيمة الخصم' :"discount value is required"  ,
        ];
        $validator = Validator::make($input, [
            'group_id' => 'required',
            'name_en' => 'required',
            'name_ar' => 'required',
            'discount_type' => 'required',
            'discount_value' => 'required',
        ], $validationMessages);
        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 400);
        }
    }
}
