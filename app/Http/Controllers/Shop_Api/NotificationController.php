<?php

namespace App\Http\Controllers\Shop_Api;

use App\Http\Controllers\Api\NotificationMethods;
use App\Http\Resources\ColorResource;
use App\Http\Resources\Shop\NotficationResource;
use App\Models\Discount;
use App\Models\Notfication;
use App\Models\user_Discount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Resources\Shop\CodeResource;
use App\User;
use App\Models\Shop;
use App\Http\Controllers\Manage\BaseController;
class NotificationController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    /*
     * Add new code to database
     * add this code to one or multi users
     */
    public function get_notification(Request $request)
    {
        $lang = $request->header('lang');
        $notification=Notfication::orderBy('id','desc')->with('user')->get();
        return $this->apiResponseData(NotficationResource::collection($notification),'success',200);
    }

    /*
     * Get Single notification
     */

    public function single_notification(Request $request,$id)
    {
        $lang = $request->header('lang');
        $notification=Notfication::find($id);
        $check=$this->not_found($notification,'الاشعار','Notfication',$lang);
        if(isset($check))
        {
            return $check;
        }
        return $this->apiResponseData(new NotficationResource($notification),'success',200);
    }

    /*
 * Get Single notification
 */

    public function delete_notification(Request $request,$id)
    {
        $lang = $request->header('lang');
        $notification=Notfication::find($id);
        $check=$this->not_found($notification,'الاشعار','Notfication',$lang);
        if(isset($check))
        {
            return $check;
        }
        $notification->delete();
        $msg=$lang=='ar' ? 'تم الحذف بنجاح' : 'Deleted Successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }

}