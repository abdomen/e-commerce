<?php

namespace App\Http\Controllers\Shop_Api;

use App\Http\Resources\AdsResource;
use App\Models\Ads;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Controllers\Manage\BaseController;

class AdsController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    /*
        * Add new Ads
    */

    public function add_Ads(Request $request)
    {
        $lang=$request->header('lang');
        if(!$request->image)
        {
            $msg=$lang=='ar' ? 'من فضلك ادخل الاعلان' : 'image is required';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $Ads = new Ads;
        $Ads->title_ar=$request->title_ar;
        $Ads->title_en=$request->title_en;
        $Ads->sub_title_ar=$request->sub_title_ar;
        $Ads->sub_title_en=$request->sub_title_en;
        $Ads->btn_text_ar=$request->btn_text_ar;
        $Ads->btn_text_en=$request->btn_text_en;
        $Ads->btn_link=$request->btn_link;
        $Ads->place_type=$request->place_type;
        $Ads->mainShop_id = $request->mainShop_id;
        $Ads->image=BaseController::saveImage('Ads',$request->image);
        $Ads->save();
        $msg=$lang=='ar' ? 'تم اضافة الاعلان بنجاح' : 'Adv added successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function edit_Ads(Request $request,$Ads_id)
    {
        $lang=$request->header('lang');

        $Ads =Ads::find($Ads_id);
        if(is_null($Ads)){
            $msg=$lang=='ar' ? 'الداتا غير موجودة' : 'data not found';
        }
        $Ads->title_ar=$request->title_ar;
        $Ads->title_en=$request->title_en;
        $Ads->sub_title_ar=$request->sub_title_ar;
        $Ads->sub_title_en=$request->sub_title_en;
        $Ads->btn_text_ar=$request->btn_text_ar;
        $Ads->btn_text_en=$request->btn_text_en;
        $Ads->btn_link=$request->btn_link;
        $Ads->place_type=$request->place_type;
        if($request->image) {
            BaseController::deleteFile('Ads',$Ads->image);
            $Ads->image = BaseController::saveImage('Ads', $request->image);
        }
        $Ads->save();
        $msg=$lang=='ar' ? 'تم التعديل ' : 'edited successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }

    /*
     * Delete Ads
     */

    public function delete_Ads(Request $request,$Ads_id)
    {
        $lang=$request->header('lang');
        $Ads=Ads::find($Ads_id);
        $check=$this->not_found($Ads,'الاعلان','image',$lang);
        if(isset($check))
        {
            return $check;
        }
        BaseController::deleteFile('Ads',$Ads->image);
        $Ads->delete();
        $msg=$lang=='ar' ? 'تم حذف الاعلان بنجاح' : 'Adv deleted successfully';
        return $this->apiResponseMessage(0,$msg,200);
    }

    /*
     * single Ads
     */

    public function single_Ads(Request $request,$Ads_id)
    {
        $lang=$request->header('lang');
        $Ads=Ads::find($Ads_id);
        $check=$this->not_found($Ads,'الاعلان','image',$lang);
        if(isset($check))
        {
            return $check;
        }

        return $this->apiResponseData(new AdsResource($Ads),'success',200);
    }

    /*
     * All Adss
     */

    public function all_Ads(Request $request)
    {
        $lang=$request->header('lang');
        $Adss=Ads::where('mainShop_id' , $request->mainShop_id)->get();
        return $this->apiResponseData(AdsResource::collection($Adss),'success',200);
    }
}
