<?php

namespace App\Http\Controllers\Shop_Api;

use App\Http\Resources\SliderResource;
use App\Models\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Resources\UserResource;
use App\Http\Resources\Shop\ProductResource;
use App\Http\Controllers\Manage\BaseController;

class SliderController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    /*
        * Add new Slider
    */

    public function add_Slider(Request $request)
    {
        $lang=$request->header('lang');
        if(!$request->image)
        {
            $msg=$lang=='ar' ? 'من فضلك ادخل الصورة' : 'image is required';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $Slider = new Slider;
        $Slider->title_ar=$request->title_ar;
        $Slider->title_en=$request->title_en;
        $Slider->sub_title_ar=$request->sub_title_ar;
        $Slider->sub_title_en=$request->sub_title_en;
        // $Slider->btn_text_ar=$request->btn_text_ar;
        // $Slider->btn_text_en=$request->btn_text_en;
        // $Slider->btn_link=$request->btn_link;
        // $Slider->mainShop_id = $request->mainShop_id;
        $Slider->image=BaseController::saveImage('Slider',$request->image);       
        $Slider->save();
        $msg=$lang=='ar' ? 'تم اضافة السليدر بنجاح' : 'Slider added successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function edit_Slider(Request $request,$slider_id)
    {
        $lang=$request->header('lang');

        $Slider =Slider::find($slider_id);
        if(is_null($Slider)){
            $msg=$lang=='ar' ? 'الداتا غير موجودة' : 'data not found';
        }
        $Slider->title_ar=$request->title_ar;
        $Slider->title_en=$request->title_en;
        $Slider->sub_title_ar=$request->sub_title_ar;
        $Slider->sub_title_en=$request->sub_title_en;
        // $Slider->btn_text_ar=$request->btn_text_ar;
        // $Slider->btn_text_en=$request->btn_text_en;
        // $Slider->btn_link=$request->btn_link;
        if($request->image) {
            BaseController::deleteFile('Slider',$Slider->image);
            $Slider->image = BaseController::saveImage('Slider', $request->image);
        }
        $Slider->save();
        $msg=$lang=='ar' ? 'تم التعديل ' : 'edited successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }

    /*
     * Delete Slider
     */

    public function delete_Slider(Request $request,$Slider_id)
    {
        $lang=$request->header('lang');
        $Slider=Slider::find($Slider_id);
        $check=$this->not_found($Slider,'الصورة','image',$lang);
        if(isset($check))
        {
            return $check;
        }
        BaseController::deleteFile('Slider',$Slider->image);
        $Slider->delete();
        $msg=$lang=='ar' ? 'تم حذف السليدر بنجاح' : 'Slider deleted successfully';
        return $this->apiResponseMessage(0,$msg,200);
    }

    /*
     * single Slider
     */

    public function single_Slider(Request $request,$Slider_id)
    {
        $lang=$request->header('lang');
        $Slider=Slider::find($Slider_id);
        $check=$this->not_found($Slider,'الصورة','image',$lang);
        if(isset($check))
        {
            return $check;
        }

        return $this->apiResponseData(new SliderResource($Slider),'success',200);
    }

    /*
     * All Sliders
     */

    public function all_Slider(Request $request)
    {
        $lang=$request->header('lang');
        $Sliders=Slider::orderBy('created_at','DESC')->get();
        return $this->apiResponseData(SliderResource::collection($Sliders),'success',200);
    }
}
