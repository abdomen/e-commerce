<?php

namespace App\Http\Controllers\Shop_Api;

use App\Models\Category;
use App\Models\Tax;
use App\Models\Price;
use App\Models\Products;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Controllers\Manage\BaseController;
use App\Http\Resources\TaxResource;

class TaxController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;
    /*
     * Add new Product_details_price to database
     */
    public function add_Tax(Request $request)
    {
        $lang = $request->header('lang');
        $user=Auth::user();
        $validate_Tax=$this->validate_Tax_add($request);
        if(isset($validate_Tax)){
            return $validate_Tax;
        }
        $type_id  = $request->type_id ;

        if($request=='1'){

            $type_id_ex = Category::find($type_id);
            $check=$this->not_found($type_id_ex,'قسم المنتج','category ',$lang);
            if(isset($check)){
                return $check;
            }
        }
        else if($request=='2' ){
            $type_id_ex = Groups::find($type_id);
            $check=$this->not_found($type_id_ex,'جروب المنتج','Groups ',$lang);
            if(isset($check)){
                return $check;
            }

            }
        else if ($request=='3')
        {
                $type_id_ex = Product_details::find($type_id);
                $check=$this->not_found($type_id_ex,' المنتج','Product_details ',$lang);
                if(isset($check)){
                    return $check;
                }

        }

        $Tax=new Tax();
        $Tax->name_en=$request->name_en;
        $Tax->name_ar=$request->name_ar;
        $Tax->type=$request->type;

        $Tax->type_id=$type_id;
        $Tax->value_type=$request->value_type;
        $Tax->value=$request->value;

        $Tax->save();
        $lang = $request->header('lang');
        $msg = $lang=='ar' ? 'تم اضافة ضريبة المنتج بنجاح'  : 'Tax added successfully';
        return $this->apiResponseData(new TaxResource($Tax),$msg);

    }

    public function edit_Tax(Request $request,$id)
    {
        $lang = $request->header('lang');
        $Tax = Tax::find($id);
        $check=$this->not_found($Tax,'ضريبه المنتج','Tax',$lang);
        if(isset($check)){
            return $check;
        }

        $Tax->name_en=$request->name_en;
        $Tax->name_ar=$request->name_ar;
        $Tax->value_type=$request->value_type;
        $Tax->value=$request->value;
        $Tax->save();
        $lang = $request->header('lang');
        $msg=$lang=='ar' ? 'تم تعديل ضريبة المنتج بنجاح'  : 'Tax edited successfully';
        return $this->apiResponseData(new TaxResource($Tax),$msg);
    }

    /*
     * get All product for Auth shop
     */
    public function all_Tax(Request $request)
    {
        $user=Auth::user();
        $page=$request->page * 20;
        $Tax=Tax::orderBy('id','DESC')->get();
        return $this->apiResponseData(TaxResource::collection($Tax),'success');
    }

    /*
     * Show single Product_details_price
     */
    public function single_Tax(Request $request,$id){
        $lang=$request->header('lang');
        $Tax=Tax::find($id);
        $check=$this->not_found($Tax,'ضريبة المنتج','Tax',$lang);
        if(isset($check)){
            return $check;
        }
        $msg=$lang=='ar' ?'تمت العملية بنجاح' : 'success';
        return $this->apiResponseData(new TaxResource($Tax),$msg);
    }

    /*
     * Delete Product_details_price ..
     */

    public function delete_Tax(Request $request,$id){
        $lang=$request->header('lang');
        $Tax=Tax::where('id',$id)->where('id','!=',1)->first();
        $check=$this->not_found($Tax,'ضريبة المنتج','Tax',$lang);
        if(isset($check)){
            return $check;
        }



        $Tax->delete();
        $msg=$lang=='ar' ? 'تم حذف ضريبة المنتج بنجاح'  : 'Tax Deleted successfully';

        return $this->apiResponseMessage(1,$msg,200);
    }


    /*
     * @pram $request
     * @return Error message or check if cateogry is null
     */

    private function validate_Tax_add($request){
        $lang = $request->header('lang');
        $input = $request->all();
        $validationMessages = [
            'type_id.required' => $lang == 'ar' ?  'من فضلك ادخل قسم  المنتج' :"type_id is required" ,
        ];
        $validator = Validator::make($input, [
            'type_id' => 'required',
        ], $validationMessages);
        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 400);
        }

    }


}
