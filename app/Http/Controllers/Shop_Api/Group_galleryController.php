<?php

namespace App\Http\Controllers\Shop_Api;

use App\Http\Resources\Group_galleryResource;
use App\Models\Group_gallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Controllers\Manage\BaseController;

class Group_galleryController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    /*
        * Add new Group_gallery
    */

    public function add_Group_gallery(Request $request)
    {
        $lang=$request->header('lang');
        if(!$request->image)
        {
            $msg=$lang=='ar' ? 'من فضلك ادخل الصورة' : 'image is required';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $Group_gallery = new Group_gallery;
        $Group_gallery->image=BaseController::saveImage('Group_gallery',$request->image);
        $Group_gallery->group_id = $request->group_id;
        $Group_gallery->save();
        $msg=$lang=='ar' ? 'تم اضافة الصورة بنجاح' : 'image added successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }

    /*
     * Delete Group_gallery
     */

    public function delete_Group_gallery_image(Request $request,$Group_gallery_id)
    {
        $lang=$request->header('lang');
        $Group_gallery=Group_gallery::find($Group_gallery_id);
        $check=$this->not_found($Group_gallery,'البراند','image',$lang);
        if(isset($check))
        {
            return $check;
        }
        BaseController::deleteFile('Group_gallery',$Group_gallery->image);
        $Group_gallery->delete();
        $msg=$lang=='ar' ? 'تم حذف الصورة بنجاح' : 'image deleted successfully';
        return $this->apiResponseMessage(0,$msg,200);
    }

    /*
     * single Group_gallery
     */

    public function single_Group_gallery_image(Request $request,$Group_gallery_id)
    {
        $lang=$request->header('lang');
        $Group_gallery=Group_gallery::find($Group_gallery_id);
        $check=$this->not_found($Group_gallery,'الصورة','image',$lang);
        if(isset($check))
        {
            return $check;
        }

        return $this->apiResponseData(new Group_galleryResource($Group_gallery),'success',200);
    }

    /*
     * All Group_gallerys
     */

    public function all_Group_Gallary_images(Request $request)
    {
        $lang=$request->header('lang');
        $Group_gallerys=Group_gallery::where('group_id' , $request->group_id )->get();
        return $this->apiResponseData(Group_galleryResource::collection($Group_gallerys),'success',200);
    }
}
