<?php

namespace App\Http\Controllers\Shop_Api;

use App\Models\Currency;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Resources\UserResource;
use App\Http\Resources\Shop\ProductResource;
use App\Http\Controllers\Manage\BaseController;

class CurrencyController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    /*
     * Add new Currency
    */

    public function add_currency(Request $request)
    {
        $lang=$request->header('lang');
        if(!$request->name_en)
        {
            $msg=$lang=='ar' ? 'من فضلك ادخل اسم العملة بالعربية' : 'Currency in arabic is required';
            return $this->apiResponseMessage(0,$msg,200);
        }
        if(!$request->name_ar)
        {
            $msg=$lang=='ar' ? 'من فضلك ادخل العملة بالانجليزية' : 'Currency in english is required';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $Currency = new Currency;
        $Currency->name_en=$request->name_en;
        $Currency->name_ar=$request->name_ar;
        $Currency->defualt = $request->defualt;
        $Currency->mainShop_id = $request->mainShop_id;
        $Currency->save();
        $msg=$lang=='ar' ? 'تم اضافة العملة بنجاح' : 'Currency added successfully';
        return $this->apiResponseMessage(0,$msg,200);
    }

    /*
     * Edit Currency
    */
    public function edit_Currency(Request $request,$Currency_id)
    {
        $lang=$request->header('lang');
        $Currency=Currency::find($Currency_id);
        $check=$this->not_found($Currency,'العملة','Currency',$lang);
        if(isset($check))
        {
            return $check;
        }
        if(!$request->name_en)
        {
            $msg=$lang=='ar' ? 'من فضلك ادخل العملة بالعربية' : 'Currency in arabic is required';
            return $this->apiResponseMessage(0,$msg,200);
        }
        if(!$request->name_ar)
        {
            $msg=$lang=='ar' ? 'من فضلك ادخل العملة بالانجليزية' : 'Currency in english is required';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $Currency->name_en=$request->name_en;
        $Currency->name_ar=$request->name_ar;
        $Currency->defualt = $request->defualt;
        $Currency->save();
        $msg=$lang=='ar' ? 'تم تعديل المقاس بنجاح' : 'Currency edited successfully';
        return $this->apiResponseMessage(0,$msg,200);
    }

    /*
     * Delete Currency
     */

    public function delete_Currency(Request $request,$Currency_id)
    {
        $lang=$request->header('lang');
        $Currency=Currency::find($Currency_id);
        $check=$this->not_found($Currency,'المقاس','Currency',$lang);
        if(isset($check))
        {
            return $check;
        }
        $Currency->delete();
        $msg=$lang=='ar' ? 'تم حذف المقاس بنجاح' : 'Currency deleted successfully';
        return $this->apiResponseMessage(0,$msg,200);
    }

    /*
     * single Currency
     */

    public function single_Currency(Request $request,$Currency_id)
    {
        $lang=$request->header('lang');
        $Currency=Currency::find($Currency_id);
        $check=$this->not_found($Currency,'العملة','Currency',$lang);
        if(isset($check))
        {
            return $check;
        }

        return $this->apiResponseData($Currency,'success',200);
    }

}