<?php

namespace App\Http\Controllers\Fronted;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Manage\EmailsController;
use App\Models\Products;
use App\Models\Subscribe;
use Auth,File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;

class PagesController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    public function About_us()
    {
        return view('Fronted.Pages.about');
    }

    public function Contact_Us(){
        return view('Fronted.Pages.Contact_Us');

    }

    public function subscribe(Request $request)
    {
        $lang=$request->header('lang');
        $sus=new Subscribe;
        $sus->email=$request->email;
        $sus->save();
        EmailsController::subscribe_mail($request->email);
        $msg=$lang=='ar' ? 'تم الاشتراك بنجاح' : 'Subscribed successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }

    public function search(Request $request)
    {
        $products=Products::where('status',1)->where('name_ar','LIKE','%' . $request->text.'%')->orWhere('name_en','LIKE','%' . $request->text . '%');
        if($request->cat_id){
            $products=$products->where('cat_id',$request->cat_id);
        }
        $paginate=$request->paginate ? $request->paginate : 12;
        $products=$products->paginate($paginate);
        $text=$request->text;
        $cat_id=$request->cat_id;
        return view('Fronted.Pages.search',compact('products','paginate','cat_id','text'));
    }

    
}
