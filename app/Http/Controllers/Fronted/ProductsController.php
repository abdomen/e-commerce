<?php

namespace App\Http\Controllers\Fronted;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Products;
use Auth,File;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
  public function product_details($product_id)
  {
      $product=Products::find($product_id);
      if(is_null($product)) {
          return view('not_found');
      }
      $products=Products::where('cat_id',$product->cat_id)->where('id','!=',$product_id)->take(6)->get();
      return view('Fronted.Products.product_details',compact('product','products'));
  }

    /**
     * @param Request $request
     * @param $cat_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
  public function product_by_category (Request $request)
  {

      $products=Products::where('status',1);

      $products=$products->paginate(12);
      return view('Fronted.Products.product_by_category',compact('products'));
  }

    /**
     * @param $product_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
  public function get_model($product_id)
  {
      $row=Products::find($product_id);
      if(is_null($row)){
          return view('not_found');
      }
      return view('Fronted.Products.get_model',compact('row'));
  }
}