<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\AboutResource;
use App\Http\Resources\Shop\ShopResource;
use App\Http\Resources\SliderResource;
use App\Http\Resources\Sliders_MResource;
use App\Models\About;
use App\Models\Category;
use App\Models\Shop;
use App\Models\Slider;
use App\Models\Inventory;
use App\Models\Product_detail_inventory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\UserResource;
use App\User;
use Storage;
use App\Models\Products;
use App\Models\Product_details;
use App\Models\Price;
use App\Models\Product_details_price;
use App\Http\Resources\Product_detailsResource;
use App\Http\Resources\Products_MResource;
use App\Http\Resources\ProductResource;
use App\Http\Resources\CategoryResource;
use App\Http\Controllers\Shop_Api\P_d_orderController;

class homeController extends Controller
{
    use ApiResponseTrait;

    /*
     * @pram request array
     * @return  response()->json
     */

    public function home(Request $request)
    {
        $lang=$request->header('lang');

        $invs = Inventory::where('status',1)->pluck('id');
        $invPro = Product_detail_inventory::where('inventory_id',1)->where('quantity','!=',0)->pluck('product_detail_id');

        $high_rated = Product_details::/*where('rate','>=','0')->*/where('status',1)->whereIn('id',$invPro)->orderBy('rate','desc')->take(20)->get();

        $Offers = Product_details::where('is_offer',1)->where('status',1)->whereIn('id',$invPro)->orderBy('id','desc')->take(20)->get();

        $best_seller = Product_details::whereHas('orders')->whereIn('id',$invPro)->withCount('orders')->orderBy('orders_count','desc')->where('status',1)->take(20)->get();
        
        $sliders=Sliders_MResource::collection(Slider::get());
        
        $best_seller=Products_MResource::collection($best_seller);
        $high_rated=Products_MResource::collection($high_rated);
        $Offers=Products_MResource::collection($Offers);
        $data=['high_rated'=>$high_rated,'Offers'=>$Offers,'best_seller'=>$best_seller,'sliders'=>$sliders];
        $msg=$lang=='ar' ? 'تمت العملية بنجاح' : 'success';
        return response()->json(['data'=>$data,'status'=>1,'message'=>$msg]);
    }

    /*
     * home api from website becouse design is different
     * slider , new item , About us , Category , social icon and skip hot offer
     */

    public function home_website(Request $request)
    {
        $lang=$request->header('lang');
        $invs = Inventory::where('status',1)->pluck('id');
        $invPro = Product_detail_inventory::where('inventory_id',1)->where('quantity','!=',0)->pluck('product_detail_id');

        $high_rated=Product_details::orderBy('rate','desc')->whereIn('id',$invPro)->where('status',1)/*where('rate','>=','0')->*/->take(6)->get();
        $new_item=Product_details::orderBy('id','desc')->orderBy('created_at','desc')->whereIn('id',$invPro)->take(6)->where('status',1)->take(20)->get();
        $Offers=Product_details::orderBy('id','desc')->where('is_offer',1)->whereIn('id',$invPro)->take(6)->where('status',1)->take(20)->get();
        $Offers=Products_MResource::collection($Offers);
        $best_seller=Product_details::where('status',1)->whereIn('id',$invPro)->whereHas('orders')->withCount('orders')->take(6)->orderBy('orders_count','desc')->take(10)->get();
        $best_seller=Products_MResource::collection($best_seller);
        $high_rated=Products_MResource::collection($high_rated);
        $sliders=Sliders_MResource::collection(Slider::get());
        $cat=CategoryResource::collection(Category::get());
        $About['token']=null;
        $shop=Shop::first();
        $shop['token']=null;
        $about=new AboutResource(About::first());
        $social=new ShopResource($shop);
        $data=['high_rated'=>$high_rated ,'best_seller'=>$best_seller,'Offers'=>$Offers,'sliders'=>$sliders,'cats'=>$cat,'about'=>$about,'social'=>$social];
        $msg=$lang=='ar' ? 'تمت العملية بنجاح' : 'success';
        return response()->json(['data'=>$data,'status'=>1,'message'=>$msg]);
    }

    /*
     * new_item
     */
    public function new_item(Request $request)
    {
        $lang=$request->header('lang');
        $invs = Inventory::where('status',1)->pluck('id');
        $invPro = Product_detail_inventory::where('inventory_id',1)->where('quantity','!=',0)->pluck('product_detail_id');
        $new_item=Product_details::orderBy('id','desc')->whereIn('id',$invPro)->where('status',1)->get();
        $msg=$lang=='ar' ? 'تمت العملية بنجاح' : 'success';
        return response()->json(['data'=>Products_MResource::collection($new_item),'status'=>1,'message'=>$msg]);

    }
    /*
     * Filter home by type
     */
    /*
     * Filter home by type
     */
    public function filter(Request $request)
    {
        $lang=$request->header('lang');
        $sliders=Sliders_MResource::collection(Slider::get());
        $invs = Inventory::where('status',1)->pluck('id');
        $invPro = Product_detail_inventory::where('inventory_id',1)->where('quantity','!=',0)->pluck('product_detail_id');
        $products=Product_details::where('status',1)->whereIn('id',$invPro);
        if ($request->type == 1) {
            $products = $products->orderBy('rate', 'desc');
        } elseif ($request->type == 2) {
            if ($request->lat AND $request->lng) {
                $products = $products->select(DB::raw('*, ( 6367 * acos( cos( radians('.$request->lat.') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('.$request->lng.') ) + sin( radians('.$request->lat.') )* sin( radians( lat ) ) ) ) AS distance'))
                    ->having('distance', '<', 50)
                    ->orderBy('distance');
            }else{
                $msg=$lang=='ar' ?  'من فضلك ارسل الموقع ':'please send location' ;
                return $this->apiResponseMessage( 0,$msg, 404);

            }
        }elseif($request->type == 3){
            $products = $products->orderBy('price', 'desc');
        }

        // if($request->cat_id){
        //     $products=$products->where('cat_id',$request->cat_id);
        //     $pro = Products::where('cat_id', $request->cat_id)->pluck('id');
        //     $products = Product_details::whereIn('product_id' , $pro)->where('status',1)
        // }

        $Offers= $products->where('is_offer',1)->take(15)->get();
        
        $high_rated=$products->/*where('rate','>=','0')->*/orderBy('rate','DESC')->take(15)->get();
        
        $most_sell = DB::table('order_product_details')->select(DB::raw('COUNT(product_detail_id) as count'),'product_detail_id')
        ->groupBy('product_detail_id')
        ->orderBy('count','asc')
        ->get();
        $mosts_ids = $most_sell->pluck('product_detail_id');
        $products = Product_details::whereIn('id', $mosts_ids)->whereIn('id',$invPro)->take(5)->get();        

        $best_seller=Products_MResource::collection($products);       
        $high_rated=Products_MResource::collection($high_rated);
        $Offers=Products_MResource::collection($Offers);
        $data=['high_rated'=>$high_rated,'Offers'=>$Offers,'best_seller'=>$best_seller,'sliders'=>$sliders];
        $msg=$lang=='ar' ? 'نجاح' : 'success';
        return response()->json(['data'=>$data,'status'=>1,'message'=>$msg]);
    }

    /*
        * Filter home by type
   */
    public function filter_website(Request $request)
    {
        $lang=$request->header('lang');

        $pro = Products::where('cat_id', $request->cat_id)->pluck('id');
        $invs = Inventory::where('status',1)->pluck('id');
        $invPro = Product_detail_inventory::where('inventory_id',1)->where('quantity','!=',0)->pluck('product_detail_id');
        $product_details = Product_details::whereIn('product_id' , $pro)->whereIn('id',$invPro)->where('status',1)->take(5)->get();
        $Offers=Product_details::where('is_offer',1)->whereIn('id',$invPro)->where('status',1)->take(4)->get();
        $Offers=Products_MResource::collection($Offers);
        $data=['Offers'=>$Offers,'products'=>Products_MResource::collection($product_details)];
        $msg=$lang=='ar' ? 'نجاح' : 'success';
        return response()->json(['data'=>$data,'status'=>1,'message'=>$msg]);
    }

    /*
     * @pram name to search
     * @return json array of products
     */
    public function search_by_name(Request $request)
    {
        $lang=$request->header('lang');

        $invs = Inventory::where('status',1)->pluck('id');
        $invPro = Product_detail_inventory::where('inventory_id',1)->where('quantity','!=',0)->pluck('product_detail_id');

        if($lang == 'ar') {
            $products = Product_details::where('difference_ar','like','%' .$request->name. '%')->whereIn('id',$invPro)->where('status',1)->get();
        }else{
            $products = Product_details::where('difference_en','like','%' .$request->name. '%')->whereIn('id',$invPro)->where('status',1)->get();
        }
        
        $msg=$lang=='ar' ? 'تمت العملية بنجاح' : 'success';
        return response()->json(['data'=>Products_MResource::collection($products),'status'=>1,'message'=>$msg]);
    }

    /*
     * @pram lang
     * get main category level one
     */

    public function get_main_cat(Request $request)
    {
        $lang=$request->header('lang');
        $cats=Category::where('status',1);
        if($request->home ==1){
        $cats=$cats->where('id','!=',1);
        }
        $cats=$cats->get();
        return response()->json(['data'=>CategoryResource::collection($cats),'status'=>1,'message'=>'success']);
    }


    public function mob_home_filter(Request $request)
    {
        $lang = $request->header('lang');
        $user=Auth::user();
        if($user->status == "1"){
            $price_type = 'selling_price';
        }elseif($user->status == "3"){
            $price_type = 'wholesale_price';
        }elseif($user->status == "4"){
            $price_type = 'wholesale_wholesale_price';
        }

        $req = $request->all();
        $cat_ids = $req['cat_id'];
        $price_from = $req['price_from'];
        $price_to = $req['price_to'];

        $mainPro_IDs = Products::whereIn('cat_id',$cat_ids)->pluck('id');
        $price_Pro_IDs = Product_details_price::whereIn('price_id' , Price::whereBetween($price_type, [$price_from,$price_to])->pluck('id'))->pluck('product_detail_id');
        $invs = Inventory::where('status',1)->pluck('id');
        $invPro = Product_detail_inventory::where('inventory_id',1)->where('quantity','!=',0)->pluck('product_detail_id');
        $products = Product_details::whereIn('product_id' , $mainPro_IDs)->whereIn('id' , $price_Pro_IDs)->whereIn('id',$invPro)->get();

        $msg=$lang=='ar' ? 'نجاح' : 'success';
        return response()->json(['data'=>Products_MResource::collection($products),'status'=>1,'message'=>$msg]);
    }


    public function mob_home_sort(Request $request)
    {
        $lang = $request->header('lang');

        $type = $request->type;
        $invs = Inventory::where('status',1)->pluck('id');
        $invPro = Product_detail_inventory::where('inventory_id',1)->where('quantity','!=',0)->pluck('product_detail_id');
        $products = Products_MResource::collection(Product_details::where('status' , 1)->whereIn('id',$invPro)->get());

        if($type == '1'){
            $sorted = $products->sortByDesc('price')->toArray();
        }elseif($type == '2'){
            $sorted = $products->sortBy('price');
            $sorted->values()->all();
        }elseif($type == '3'){
            $sorted = $products->sortByDesc('rate')->toArray();
        }

        $msg=$lang=='ar' ? 'نجاح' : 'success';
        return response()->json(['data'=>Products_MResource::collection($sorted),'status'=>1,'message'=>$msg]);
    }



    public function mob_home_sorted_filter(Request $request)
    {
        $lang = $request->header('lang');
        $user=Auth::user();
        if($user->status == "1"){
            $price_type = 'selling_price';
        }elseif($user->status == "3"){
            $price_type = 'wholesale_price';
        }elseif($user->status == "4"){
            $price_type = 'wholesale_wholesale_price';
        }

        $req = $request->all();
        
        $cat_ids = $req['cat_id'];
        $price_from = $req['price_from'];
        $price_to = $req['price_to'];
        $name = $req['name'];
        $type = $req['type'];

        $mainProName_IDs = Products::Where('name_en','like','%' .$name. '%')->orWhere('name_ar','like','%' .$name. '%')->pluck('id');
        $mainProNameProDetIds = Product_details::where('product_id' , $mainProName_IDs)->pluck('id');
        $proDetNameIds = Product_details::Where('difference_ar','like','%' .$name. '%')->orWhere('difference_en','like','%' .$name. '%')->pluck('id');
        $nameIds = Product_details::whereIn('id',$proDetNameIds)->orWhereIn('id',$mainProNameProDetIds)->pluck('id');
        
        if(count($cat_ids) == 0){
            $catProIds = Product_details::pluck('id');
        }else{
            $mainPro_IDs = Products::whereIn('cat_id',$cat_ids)->pluck('id');
            $catProIds = Product_details::whereIn('product_id' , $mainPro_IDs)->pluck('id');
        }
        
        $price_Pro_IDs = Product_details_price::whereIn('price_id' , Price::whereBetween($price_type, [$price_from,$price_to])->pluck('id'))->pluck('product_detail_id');

        $invs = Inventory::where('status',1)->pluck('id');
        $invPro = Product_detail_inventory::where('inventory_id',1)->where('quantity','!=',0)->pluck('product_detail_id');

        $products = Products_MResource::collection(Product_details::WhereIn('id' , $nameIds)
        ->WhereIn('id' , $catProIds)
        ->WhereIn('id' , $price_Pro_IDs)
        ->WhereIn('id' , $invPro)
        ->get());

        if($type == '1'){
            $sorted = $products->sortByDesc('price')->toArray();
        }elseif($type == '2'){
            $sorted = $products->sortBy('price');
            $sorted->values()->all();
        }elseif($type == '3'){
            $sorted = $products->sortByDesc('rate')->toArray();
        }

        $msg=$lang=='ar' ? 'نجاح' : 'success';
        return response()->json(['data'=>Products_MResource::collection($sorted),'status'=>1,'message'=>$msg]);
    }
}