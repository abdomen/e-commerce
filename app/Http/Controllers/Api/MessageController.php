<?php

namespace App\Http\Controllers\Api;

use App\Events\MessagesSubmit;
use App\Http\Resources\MessageResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File;
use App\Models\Message;
use DB;
class MessageController extends Controller
{
    use ApiResponseTrait;

    /*
     * @pram array
     * product_view  ==>to save new view form user to product (Data analysis).
     * @return  response()->json
     */
    public function send_message(Request $request)
    {
        $lang=$request->header('lang');
        if(!$request->message)
        {
            $msg=$lang=='ar' ? 'من فضلك اكتب الرسالة' : 'message is required';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $user=Auth::user();
        $Messa=new Message;
        $Messa->from=$user->id;
        $Messa->to=1;
        $Messa->type=1;
        $Messa->is_read=1;
        $Messa->message=$request->message;
        $Messa->user_id=$user->id;
        $Messa->save();
        event(new MessagesSubmit($request->message,$user->id,1));
        $msg=$lang=='ar' ? 'تم الارسال بنجاح' : 'message sent successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }

    /*
     * get messages for custom user
     */

    public function get_message(Request $request)
    {
        $user=Auth::user();
        $messages=Message::where('user_id',$user->id)->get();
        return $this->apiResponseData(MessageResource::collection($messages),'success',200);
    }

    public function store(Request $request)
    {
        return $request->all();
    }

}