<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\NotificationMethods;
use App\Http\Resources\locationResource;
use App\Http\Resources\NotficationResource;
use App\Models\Currency;
use App\Models\Message;
use App\Models\Notfication;
use App\Models\order_locations;
use App\Models\Settings;
use App\Models\Product_details_price;
use App\Models\Price;
use App\Models\Representative_User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\UserResource;
use App\Http\Resources\ProductResource;
use App\Http\Resources\Product_detailsResource;
use App\Http\Resources\Products_MResource;
use App\Models\Product_details;
use App\Http\Resources\CartResource;
use App\Http\Resources\RepShowResource;
use App\Http\Resources\UserAddressResource;
use App\Http\Resources\Rep_attendenceResource;
use App\Models\Rep_attendence;
use App\User;
use App\Http\Controllers\Manage\BaseController;
use App\Http\Controllers\Manage\EmailsController;
use Illuminate\Support\Facades\Route;

class UserController extends Controller
{
    use ApiResponseTrait;

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */


    
    public function send_sms(Request $request)
    {
        $mob_num = $request->mob_num;
        $message = URLEncode($request->message);

        $url = "https://www.hisms.ws/api.php?send_sms&username=966530700488&password=966530700488&numbers=$mob_num&sender=Active-code&message=$message";
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);
        return response()->json($curl_scraped_page);

    }

    
    
    private function send__sms($mob_num,$message)
    {

        $url = "https://www.hisms.ws/api.php?send_sms&username=966530700488&password=966530700488&numbers=$mob_num&sender=Active-code&message=$message";
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);

    }
    
    
    public function register(Request $request)
    {
        $lang = $request->header('lang');
        $input = $request->all();
        $validationMessages = [
            'first_name.required' => $lang == 'ar' ?  'من فضلك ادخل  اسمك الاول' :"first name is required" ,
            'password.required' => $lang == 'ar' ? 'من فضلك ادخل كلمة السر' :"password is required"  ,
            'password.confirmed' => $lang == 'ar' ? 'كلمتا السر غير متطابقتان' :"The password confirmation does not match"  ,
            'password.min' => $lang == 'ar' ?  'كلمة السر يجب ان لا تقل عن 7 احرف' :"The password must be at least 6 character" ,
            'email.required' => $lang == 'ar' ? 'من فضلك ادخل البريد الالكتروني' :"email is required"  ,
            'email.unique' => $lang == 'ar' ? 'هذا البريد الالكتروني موجود لدينا بالفعل' :"email is already teken" ,
            'email.regex'=>$lang=='ar'? 'من فضلك ادخل بريد الكتروني صالح' : 'The email must be a valid email address',
            'phone.required' => $lang == 'ar' ? 'من فضك ادخل رقم الهاتف' :"phone is required"  ,
            'phone.unique' => $lang == 'ar' ? 'رقم الهاتف موجود لدينا بالفعل' :"phone is already teken" ,
            'last_name.required' => $lang == 'ar' ?  'من فضلك ادخل اسم العائلة' :"last name name is required" ,
            'phone.min' => $lang == 'ar' ?  'رقم الهاتف يجب ان لا يقل عن 7 ارقام' :"The phone must be at least 7 numbers" ,
            'phone.numeric' => $lang == 'ar' ?  ' الهاتف يجب ان يكون رقما' :"The phone must be a number" ,
        ];

        $validator = Validator::make($input, [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required|unique:users|numeric|min:7',
            'email' => 'required|unique:users|regex:/(.+)@(.+)\.(.+)/i',
            'password' => 'required|confirmed|min:6',
        ], $validationMessages);

        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 200);
        }

        $user = new User();
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->lat  = $request->lat;
        $user->lng  = $request->lng;
        $user->fire_base_token  = $request->fire_base_token;
        $user->status  = '1';
        $user->social  = 0;
        $user->is_active  = 1;
        $user->lang=$lang;
        $user->password = Hash::make($request->password);
        if($request->image){
            $name=BaseController::saveImage('users',$request->file('image'));
            $user->image=$name;
        }
        $code=mt_rand(999,9999);
        $user->code=$code;
        $user->save();
        
        $phone = $request->phone;
        $mob_num = "966$phone";
        $message = "كود التفعيل لهذا الرقم هو $code";
        $this->send__sms($mob_num,$message);

        // return $mob_num;
        // return $message;
        $url = "https://www.hisms.ws/api.php?send_sms&username=966530700488&password=966530700488&numbers=$mob_num&sender=Active-code&message=$message";     
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);
        // return response()->json($curl_scraped_page);
        
        $token = $user->createToken('TutsForWeb')->accessToken;
        $user['token']=$token;
       // EmailsController::verify_email($user->id,$lang);
        $this->first_notfication($user->fire_base_token,$user->lang,$user->id);
        Auth::loginUsingId($user->id);
        $msg=$lang == 'ar' ? ' تم التسجيل بنجاح ... تاكد من تفعيل الحساب بادخال الكود المرسل ' : 'register success .... activate your account by the sent code';
        return response()->json([ 'status'=>1,'message'=> $msg, 'data'=>$user]);
    }


    public static function add_representative(Request $request)
    {
        $lang = $request->header('lang');
        // $shop=Auth::user();
        $input = $request->all();
        $validationMessages = [
            'first_name.required' => $lang == 'ar' ?  'من فضلك ادخل رقم الاسم الاول' :"frist name is required" ,
            'password.required' => $lang == 'ar' ? 'من فضلك ادخل كلمة السر' :"password is required"  ,
            'email.required' => $lang == 'ar' ? 'من فضلك ادخل البريد الالكتروني' :"email is required"  ,
            'email.unique' => $lang == 'ar' ? 'هذا البريد الالكتروني موجود لدينا بالفعل' :"email is already teken" ,
            'email.regex'=>$lang=='ar'? 'من فضلك ادخل بريد الكتروني صالح' : 'The email must be a valid email address',
            'phone.required' => $lang == 'ar' ? 'من فضلك ادخل البريد رقم الهاتف' :"phone is required"  ,
            'phone.unique' => $lang == 'ar' ? 'رقم الهاتف موجود لدينا بالفعل' :"phone is already teken" ,
            'last_name.required' => $lang == 'ar' ?  'من فضلك ادخل رقم اسم العائلة' :"last name name is required" ,
            'phone.min' => $lang == 'ar' ?  'رقم الهاتف يجب ان لا يقل عن 7 ارقام' :"The phone must be at least 7 numbers" ,
            'phone.numeric' => $lang == 'ar' ?  'رقم الهاتف يجب ان يكون رقما' :"The phone must be a number" ,
        ];

        $validator = Validator::make($input, [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required|unique:users|numeric|min:7',
            'email' => 'required|unique:users|regex:/(.+)@(.+)\.(.+)/i',
            'password' => 'required',
        ], $validationMessages);

        if ($validator->fails()) {
            $msg=$lang == 'ar' ? 'رقم الهاتف او الايميل موجود من قبل' : 'Phone number or E-mail address is existed';
            return $this->apiResponseMessage(0,$msg, 200);
        }

        $user = new User();
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->lat  = $request->lat;
        $user->lng  = $request->lng;
        $user->status  = '2';
        $user->shop_id = $request->shop_id;
        $user->social  = 0;
        $user->is_active  = 1;
        $user->lang=$lang;
        $user->password = Hash::make($request->password);
        if($request->image){
            $name=BaseController::saveImage('users',$request->file('image'));
            $user->image=$name;
        }
        $user->save();
        $user['token']=null;

        $rep = User::find($user->id);
        $msg=$lang == 'ar' ? 'تم اضافة المندوب بنجاح' : 'Representative added successfully';
        return response()->json([ 'status'=>1,'message'=> $msg, 'data'=>new RepShowResource($rep)]);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function resend_verification_code(Request $request)
    {
        $lang=$request->header('lang');
        $user=Auth::user();
        $code=mt_rand(999,9999);
        $user->code=$code;
        $user->save();
        EmailsController::verify_email($user->id,$lang);
        $msg = $lang=='ar' ? 'تم اعادة ارسال كود التفعيل' : 'verification code resend successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }

    /*
     * check verification code
     */

    public function activate_account(Request $request)
    {
        $lang=$request->header('lang');
        $user=Auth::user();

        $code = $request->code;

        $user->code = null;
        $user->status = 1;
        $user->save();
        $msg = $lang=='ar' ? 'تم التفعيل بنجاح' : 'Activation completed successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function register_social(Request $request)
    {
        $lang = $request->header('lang' );
        $input = $request->all();
        $validationMessages = [
            'first_name.required' => $lang == 'ar' ?  'من فضلك ادخل اسمك الاول' :"frist name is required" ,
            'password.required' => $lang == 'ar' ? 'من فضلك ادخل كلمة السر' :"password is required"  ,
            'password.confirmed' => $lang == 'ar' ? 'كلمتا السر غير متطابقتان' :"The password confirmation does not match"  ,
            'email.required' => $lang == 'ar' ? 'من فضلك ادخل البريد الالكتروني' :"email is required"  ,
            'email.unique' => $lang == 'ar' ? 'هذا البريد الالكتروني موجود لدينا بالفعل' :"email is already teken" ,
            'email.regex'=>$lang=='ar'? 'من فضلك ادخل بريد الكتروني صالح' : 'The email must be a valid email address',
            'last_name.required' => $lang == 'ar' ?  'من فضلك ادخل اسم العائلة' :"last name name is required" ,
            'phone.min' => $lang == 'ar' ?  'رقم الهاتف يجب ان لا يقل عن 7 ارقام' :"The phone must be at least 7 numbers" ,
            'phone.numeric' => $lang == 'ar' ?  'رقم الهاتف يجب ان يكون رقما' :"The phone must be a number" ,
        ];

        $validator = Validator::make($input, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:users|regex:/(.+)@(.+)\.(.+)/i',
        ], $validationMessages);

        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 200);
        }

        $user = new User();
        $user->email = $request->email;
        $user->lat  = $request->lat;
        $user->lng  = $request->lng;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->phone = $request->phone;
        $user->fire_base_token  = $request->fire_base_token;
        $user->status  = 0;
        $user->social  = 1;
        $user->lang=$lang;
        $user->is_active  = 1;
        if($request->image){
            $name=BaseController::saveImage('users',$request->file('image'));
            $user->image=$name;
        }
        $user->save();
        $token = $user->createToken('TutsForWeb')->accessToken;
        $user['token']=$token;
        $this->first_notfication($user->fire_base_token,$user->lang,$user->id);
        $msg=$lang == 'ar' ? 'تم التسجيل بنجاح' : 'register success';
        return response()->json([ 'status'=>1,'message'=> $msg, 'data'=>new UserResource($user)]);
    }
    /*
     * login method
     * return user opject and token
     */

    public function login(Request $request)
    {
        $lang = $request->header('lang');
        $user=User::where('phone',$request->eamilOrPhone)->where('status','!=','2')->first();
        if(is_null($user))
        {
            $user=User::where('email',$request->eamilOrPhone)->where('status','!=','2')->first();
            if(is_null($user))
            {
                $msg=$lang=='ar' ?  'البيانات المدخلة غير موجودة لدينا ':'user not exist' ;
                return $this->apiResponseMessage( 0,$msg, 200);
            }
        }
        $password=Hash::check($request->password,$user->password);
        if($password==true){
            $user->is_active  = 1;
            if($request->fire_base_token) {
                $user->fire_base_token = $request->fire_base_token;
            }
            $user->save();
            $token = $user->createToken('TutsForWeb')->accessToken;
            $user['token']=$token;

            Auth::loginUsingId($user->id);

            $msg=$lang=='ar' ? 'تم تسجيل الدخول بنجاح':'login success' ;
            return response()->json([ 'status'=>1,'message'=> $msg, 'data'=>new UserResource($user)]);
        }
        $msg=$lang=='ar' ?  'كلمة السر غير صحيحة' :'Password is not correct' ;
        return $this->apiResponseMessage( 0,$msg, 200);
    }


    public function rep_login(Request $request)
    {
        $lang = $request->header('lang');
        $user=User::where('phone',$request->eamilOrPhone)->where('status','2')->first();
        if(is_null($user))
        {
            $user=User::where('email',$request->eamilOrPhone)->where('status','2')->first();
            if(is_null($user))
            {
                $msg=$lang=='ar' ?  'البيانات المدخلة غير موجودة لدينا ':'user not exist' ;
                return $this->apiResponseMessage( 0,$msg, 200);
            }
        }
        $password=Hash::check($request->password,$user->password);
        if($password==true){
            $user->is_active  = 1;
            if($request->fire_base_token) {
                $user->fire_base_token = $request->fire_base_token;
            }
            $user->save();
            $token = $user->createToken('TutsForWeb')->accessToken;
            $user['token']=$token;

            Auth::loginUsingId($user->id);

            $msg=$lang=='ar' ? 'تم تسجيل الدخول بنجاح':'login success' ;
            return response()->json([ 'status'=>1,'message'=> $msg, 'data'=>new UserResource($user)]);
        }
        $msg=$lang=='ar' ?  'كلمة السر غير صحيحة' :'Password is not correct' ;
        return $this->apiResponseMessage( 0,$msg, 200);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function login_social(Request $request)
    {
        $lang = $request->header('lang');
        $user=User::where('email',$request->email)->where('social',1)->where('status','!=','2')->first();
        if(is_null($user))
        {
            $msg=$lang=='ar' ?  'البريد الالكتروني غير موجود':'Email not exist' ;
            return $this->apiResponseMessage( 0,$msg, 200);
        }
        $user->is_active  = 1;
        if($request->fire_base_token)
            $user->fire_base_token  = $request->fire_base_token;
        $user->save();
        $token = $user->createToken('TutsForWeb')->accessToken;
        $user['token']=$token;
        $msg=$lang=='ar' ?  'تم تسجيل الدخول بنجاح' :'login success' ;
        return response()->json([ 'status'=>1,'message'=> $msg, 'data'=>new UserResource($user)]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function change_password(Request $request)
    {
        $lang = $request->header('lang');
        $user = Auth::user();
        $check=$this->not_found($user,'العضو','user',$lang);
        if(isset($check))
        {
            return $check;
        }
        if(!$request->newPassword){
            $msg=$lang=='ar' ? 'يجب ادخال كلمة السر الجديدة' : 'new password is required';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $password=Hash::check($request->oldPassword,$user->password);
        if($password==true){
            $user->password=Hash::make($request->newPassword);
            $user->save();
            $msg=$lang=='ar' ? 'تم تغيير كلمة السر بنجاح' : 'password changed successfully';
            return $this->apiResponseMessage( 1,$msg, 200);

        }else{
            $msg=$lang=='ar' ? 'كلمة السر القديمة غير صحيحة' : 'invalid old password';
            return $this->apiResponseMessage(0,$msg, 200);

        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function edit_profile(Request $request)
    {
        $lang = $request->header('lang');
        $user = Auth::user();

        $check=$this->not_found($user,'العضو','user',$lang);
        if(isset($check))
        {
            return $check;
        }
        $id=Auth::user()->id;

        $input = $request->all();
        $validationMessages = [
            'first_name.required' => $lang == 'ar' ?  'من فضلك ادخل اسمك الاول' :"frist name is required" ,
            'email.required' => $lang == 'ar' ? 'من فضلك ادخل البريد الالكتروني' :"email is required"  ,
            'email.unique' => $lang == 'ar' ? 'هذا البريد الالكتروني موجود لدينا بالفعل' :"email is already teken" ,
            'email.regex'=>$lang=='ar'? 'من فضلك ادخل بريد الكتروني صالح' : 'The email must be a valid email address',
            'phone.required' => $lang == 'ar' ? 'من فضلك ادخل البريد رقم الهاتف' :"phone is required"  ,
            'phone.unique' => $lang == 'ar' ? 'رقم الهاتف موجود لدينا بالفعل' :"phone is already teken" ,
            'last_name.required' => $lang == 'ar' ?  'من فضلك ادخل اسم العائلة' :"last name name is required" ,
            'phone.min' => $lang == 'ar' ?  'رقم الهاتف يجب ان لا يقل عن 7 ارقام' :"The phone must be at least 7 numbers" ,
            'phone.numeric' => $lang == 'ar' ?  'رقم الهاتف يجب ان يكون رقما' :"The phone must be a number" ,

        ];

        $validator = Validator::make($input, [
            'phone' => 'required|min:7|numeric|unique:users,phone,'.$id,
            'email' => 'required|unique:users,email,'.$id.'|regex:/(.+)@(.+)\.(.+)/i',
            'first_name' => 'required',
            'last_name'=>'required'
        ], $validationMessages);
        
        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 400);
        }


        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->lat  = $request->lat;
        $user->lng  = $request->lng;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        if($request->image){
            BaseController::deleteFile('users',$user->image);
            $name=BaseController::saveImage('users',$request->file('image'));
            $user->image=$name;
        }
        $user->save();
        $user['token']=null;
        $msg=$lang=='ar' ?  'تمت العملية بنجاح' :'success' ;
        return $this->apiResponseData( new UserResource($user),  $msg);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function save_image(Request $request)
    {
        $user=Auth::user();
        $lang=$request->header('lang');
        if($request->image){
            BaseController::deleteFile('users',$user->image);
            $name=BaseController::saveImage('users',$request->file('image'));
            $user->image=$name;
        }else{
            $msg=$lang=='ar' ? 'من فضلك ارفع الصورة' : 'please upload image';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $user->save();
        $user['token']=null;
        $msg=$lang=='ar' ? 'تم رفع الصورة بنجاح' : 'image uploaded successfully';
        return $this->apiResponseData(new UserResource($user),$msg,200);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
        public function my_wishlist(Request $request){
            $lang = $request->header('lang');
            $user=Auth::user();
            $msg=$lang=='ar' ?  'تمت العملية بنجاح' :'success' ;
            return $this->apiResponseData(Products_MResource::collection($user->my_wishlist),$msg,200);
        }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function my_cart(Request $request){
        $lang = $request->header('lang');
        $user=Auth::user();
        $pro_ids = $user->my_cart->pluck('id');
        if($user->status == "1"){
            $price_type = 'selling_price';
        }elseif($user->status == "3"){
            $price_type = 'wholesale_price';
        }elseif($user->status == "4"){
            $price_type = 'wholesale_wholesale_price';
        }
        $sub_total_price = 0;
            foreach($user->my_cart as $proo){
                $price_id = Product_details_price::where('product_detail_id',$proo->id)->value('price_id');
                $price = Price::where('id',$price_id)->value($price_type);
                $pr_price = ($proo->pivot->quantity)*($price);
                $sub_total_price += $pr_price;
            }
        $settings = Settings::first();
        $del_charges = $settings->shipping_price;
        $tax = $settings->tax;
        $tax_val = $sub_total_price*($settings->tax/100);
        $total_price = $sub_total_price+$del_charges+$tax_val;
        $msg=$lang=='ar' ?  'تمت العملية بنجاح' :'success' ;
        return response()->json(['data'=>['products' => CartResource::collection($user->my_cart),
        'delivery_charges'=>(int)$del_charges,'tax'=>(int)$tax,'sub_total_price'=>$sub_total_price,'total_price'=>$total_price],
        'message'=>$msg,'status'=>1]); 
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function settings(Request $request){
        $lang = $request->header('lang');
        $user=Auth::user();
        if($request->currency_id){
            $currency=Currency::find($request->currency_id);
            $check=$this->not_found($currency, 'العملة','currency',$lang);
            if(isset($check)){
                return $check;
            }
            $user->currency_id=$request->currency_id;
        }
        if($request->lang) {
            $user->lang = $request->lang;
        }
        if($request->message != null) {
            $user->message = $request->message;
        }
        if($request->notification != null) {
            $user->notification = $request->notification;
        }
        $user->save();
        $user['token']=null;
        $msg=$lang=='ar' ?  'تم حفظ الاعدادات بنجاح' :'settings saved successfully' ;
        return response()->json([ 'status'=>1,'message'=> $msg, 'data'=>new UserResource($user)]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function my_info(Request $request)
    {
        $lang = $request->header('lang');
        $user=Auth::user();
        $user['token']=null;
        $msg=$lang=='ar' ?  'تمت العملية بنجاح' :'success' ;
        return $this->apiResponseData(new UserResource($user),$msg);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function forget_password(Request $request){
        $lang=$request->header('lang');
        $user=User::where('email',$request->email)->first();
        $check=$this->not_found($user,'البريد الالكتروني','Email Address',$lang);
        if(isset($check)){
            return $check;
        }
        $code=mt_rand(999,9999);
        $user->code=$code;
        $user->save();
        EmailsController::forget_password($user,$lang);
        $msg=$lang=='ar' ? 'تفحص بريدك الالكتروني' : 'check your mail';
        return $this->apiResponseMessage(1,$msg,200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function reset_password(Request $request)
    {
        $lang=$request->header('lang');
        if(!$request->code){
            $msg=$lang=='ar' ? 'من فضلك ادخل الكود' : 'code is required';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $user=User::where('code',$request->code)->first();
        if(is_null($user)){
            $msg=$lang=='ar' ? 'الكود غير صحيح' : 'code is incorrect';
            return $this->apiResponseMessage(0,$msg,200);
        }
        if(!$request->password){
            $msg=$lang=='ar' ? 'من فضلك ادخل كلمة السر الجديدة' : 'new password is required';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $user->password=Hash::make($request->password);
        $user->code=null;
        $user->save();
        $msg=$lang=='ar' ? 'تم تغيير كلمة السر بنجاح' : 'password changed successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_notification(Request $request)
    {
        $user=Auth::user();
        $lang=$request->header('lang');
        $page=$request->page * 15;
        $notification=Notfication::where('user_id',$user->id)->skip($page)->take(15)->get();
        $msg=$lang=='ar' ? 'تمت العملية بنجاح' : 'success';
        return $this->apiResponseData(NotficationResource::collection($notification),$msg);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function save_fire_base_token(Request $request)
    {
        $user=Auth::user();
        $welcome=true;
        if($user->fire_base_token == null){
            $welcome=false;
        }
        $user->fire_base_token=$request->fire_base_token;
        $user->Save();
        if($welcome==false)
        {
            $this->first_notfication($user->fire_base_token,$user->lang,$user->id);
        }
        $msg=$user->lang=='ar' ? 'تم حفظ المعلومات بنجاح' : 'success';
        return $this->apiResponseMessage(1,$msg,200);
    }

    /**
     * @param $token
     * @param $lang
     * @param $id
     */
    private function first_notfication($token,$lang,$id)
    {
        $notfcaion=new Notfication();
        if($lang=='ar'){
            $notfcaion->name='اهلا بك';
            $notfcaion->desc='مرحبا بك في قمه';
        }else{
            $notfcaion->name='welcome';
            $notfcaion->desc='welcome in Qimma';
        }
        $notfcaion->user_id=$id;
        $notfcaion->save();
        $title=$lang=='ar' ? 'مرحبا بك ' : 'welcome';
        $desc=$lang=='ar' ? 'مرحبا بك في قمه' : 'welcome in Qimma';
        NotificationMethods::senNotificationToSingleUser($token,$title,$desc,null,1,0);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $lang=$request->header('lang');
        $user=Auth::user();
        $user->is_active=0;
        $user->save();
        Auth::user()->tokens->each(function($token, $key) {
            $token->delete();
        });
        $msg=$lang=='ar' ? 'تم تسجيل الخروج بنجاح' : 'logout successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function count_of_cart(Request $request)
    {
        $lang=$request->header('lang');
        $user=Auth::user();
        $notfction_count=Notfication::where('user_id',$user->id)->count();
        $message_count=Message::where('user_id',$user->id)->count();
        $data=['cart_count'=>$user->my_cart->count(),'count_wishlist'=>$user->my_wishlist->count(),'notfction_count'=>$notfction_count,'message_count'=>$message_count];
        return $this->apiResponseData($data,'success',200);
    }

    /*
     *  delete email for rana
     */

    public function delete_mostafa(Request $request)
    {
        $user=User::where('email',$request->email)->delete();
        return $user;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add_address(Request $request)
    {
        $lang=$request->header('lang');
        $user=Auth::user();
        $address=new order_locations();
        $address->lat=$request->lat;
        $address->lng=$request->lng;
        $address->address=$request->address;
        $address->user_id=$user->id;
        $address->save();
        $msg=$lang=='ar' ? 'تم تسجيل العنوان بنجاح' : 'location saved successfully';
        return $this->apiResponseData(new locationResource($address),$msg,200);
    }

    /*
     * get All user for Auth shop
     */
    public function all_users(Request $request)
    {
        $shop=Auth::user();
        // $page=$request->page * 20;
        $users=User::where('status' , '!=' , '2')->orderBy('id','desc')->get();
        // return $users->user_address;
        // foreach($users as $row){$row['image']=BaseController::getImageUrl('users',$row->image); return $row->user_address;}
        // $data = ['users' => $users , 'addresses' => $users->user_address];
        return $this->apiResponseData(UserAddressResource::collection($users),'success');
    }


    public function sentNotifcationToCoustomUser($id)
    {
        $token=User::where('id',$id)->value('fire_base_token');
        return NotificationMethods::senNotificationToSingleUser($token,'custom','custom user',null,1,1);
    }

    public function add_attendence(Request $request)
    {
        $representative = Auth::user();

        $attendence = new Rep_attendence;
        $attendence->representative_id = $representative->id;
        $attendence->user_id = $request->user_id;
        $attendence->lng = $request->lng;
        $attendence->lat = $request->lat;
        $attendence->status = $request->status;
        $attendence->note = $request->note;
        $attendence->save();

        return $this->apiResponseData(new Rep_attendenceResource($attendence),'success');
    }


    public function my_attendence(Request $request)
    {
        $representative = Auth::user();

        $attendence = Rep_attendence::where('representative_id' , $representative->id)->orderBy('created_at' , 'desc')->get();

        return $this->apiResponseData(Rep_attendenceResource::collection($attendence),'success');
    }

    public function filter_my_attendence_by_date(Request $request)
    {
        $representative = Auth::user();

        $from = date($request->from);
        $to = date($request->to);
        $dated_orders = Rep_attendence::where('representative_id' , $representative->id)->whereBetween('created_at', [$from." 00:00:00",$to." 23:59:59"])->get();
        return $this->apiResponseData(Rep_attendenceResource::collection($dated_orders),'success');
    }

    public function user_register_inRepApp(Request $request)
    {
        $lang = $request->header('lang');

        $representative = Auth::user();

        $input = $request->all();

        $validationMessages = [
            'first_name.required' => $lang == 'ar' ?  'من فضلك ادخل  اسمك الاول' :"first name is required" ,
            'password.required' => $lang == 'ar' ? 'من فضلك ادخل كلمة السر' :"password is required"  ,
            'password.confirmed' => $lang == 'ar' ? 'كلمتا السر غير متطابقتان' :"The password confirmation does not match"  ,
            'email.required' => $lang == 'ar' ? 'من فضلك ادخل البريد الالكتروني' :"email is required"  ,
            'email.unique' => $lang == 'ar' ? 'هذا البريد الالكتروني موجود لدينا بالفعل' :"email is already teken" ,
            'email.regex'=>$lang=='ar'? 'من فضلك ادخل بريد الكتروني صالح' : 'The email must be a valid email address',
            'phone.required' => $lang == 'ar' ? 'من فضك ادخل رقم الهاتف' :"phone is required"  ,
            'phone.unique' => $lang == 'ar' ? 'رقم الهاتف موجود لدينا بالفعل' :"phone is already teken" ,
            'last_name.required' => $lang == 'ar' ?  'من فضلك ادخل اسم العائلة' :"last name name is required" ,
            'phone.min' => $lang == 'ar' ?  'رقم الهاتف يجب ان لا يقل عن 7 ارقام' :"The phone must be at least 7 numbers" ,
            'phone.numeric' => $lang == 'ar' ?  ' الهاتف يجب ان يكون رقما' :"The phone must be a number" ,
            'password.min' => $lang == 'ar' ?  'كلمة السر يجب ان لا تقل عن 7 احرف' :"The password must be at least 6 character" ,
        ];

        $validator = Validator::make($input, [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required|unique:users|numeric|min:7',
            'email' => 'required|unique:users|regex:/(.+)@(.+)\.(.+)/i',
            'password' => 'required|confirmed|min:6',
        ], $validationMessages);

        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 200);
        }

        $user = new User();
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->lat  = $request->lat;
        $user->lng  = $request->lng;
        $user->fire_base_token  = $request->fire_base_token;
        if(!$request->status){
            $user->status = 1;
        }elseif($request->status == null){
            $user->status = 1;
        }else{
            $user->status  = $request->status;
        }
        $user->social  = 0;
        $user->is_active  = 1;
        $user->lang=$lang;
        $user->password = Hash::make($request->password);
        if($request->image){
            $name=BaseController::saveImage('users',$request->file('image'));
            $user->image=$name;
        }
        $code=mt_rand(999,9999);
        $user->code=$code;
        $user->save();

        $n_user_id = $user->id;

        $user_representative = new Representative_User();
        $user_representative->representative_id = $representative->id;
        $user_representative->user_id = $n_user_id;
        $user_representative->save();
        
        $phone = $request->phone;
        $mob_num = "966$phone";
        $message = "كود التفعيل لهذا الرقم هو $code";
        $this->send__sms($mob_num,$message);

        // return $mob_num;
        // return $message;
        // $url = "https://www.hisms.ws/api.php?send_sms&username=966530700488&password=966530700488&numbers=$mob_num&sender=Active-code&message=$message";     
        // $ch = curl_init($url);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // $curl_scraped_page = curl_exec($ch);
        // curl_close($ch);
        // return response()->json($curl_scraped_page);
        
        $token = $user->createToken('TutsForWeb')->accessToken;
        $user['token']=$token;
       // EmailsController::verify_email($user->id,$lang);
        $this->first_notfication($user->fire_base_token,$user->lang,$user->id);
        $msg=$lang == 'ar' ? ' تم التسجيل بنجاح ... تاكد من تفعيل الحساب بادخال الكود المرسل ' : 'register success .... activate your account by the sent code';
        return response()->json([ 'status'=>1,'message'=> $msg, 'data'=>$user]);
    }

}
