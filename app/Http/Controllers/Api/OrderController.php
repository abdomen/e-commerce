<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Manage\EmailsController;
use App\Http\Controllers\Manage\BaseController;
use Validator,Auth,Artisan,Hash,File,DB;
use App\User;
use App\Models\Discount;
use App\Models\order_locations;
use App\Models\Settings;
use App\Models\Shop;
use App\Models\Representative_User;
use App\Models\Products;
use App\Models\Product_details;
use App\Models\Order_product_details;
use App\Models\Product_detail_inventory;
use App\Models\Cart;
use App\Models\Order;
use App\Models\P_d_orders;
use App\Models\user_Discount;
use App\Models\Order_status;
use App\Http\Resources\Product_detailsResource;
use App\Http\Resources\locationResource;
use App\Http\Resources\OrderResource;
use App\Http\Resources\P_d_ordersResource;
use App\Http\Resources\CartResource;
use App\Http\Resources\UserResource;
use App\Http\Resources\Orders_MResource;


class OrderController extends Controller
{
    use ApiResponseTrait;

    /*
     * get all user Addresses
     */
    public function user_address(Request $request)
    {
        $lang=$request->header('lang');
        $msg=$lang=='ar' ? 'تمت العملية بنجاح'  : 'success';
        $user=Auth::user();
        return $this->apiResponseData(locationResource::collection($user->my_address),$msg);
    }



    /*
     * custom function to validate Address To orders
     */

    private function validate_address($request,$lang,$user_id)
    {

        if(!$request->address_id)
        {
            $msg=$lang=='ar' ? 'من فضلك ادخل العنوان'  : 'please send location';
            return $this->apiResponseMessage(0,$msg,200);
        }
            $order_location=order_locations::find($request->address_id);
            $check=$this->not_found($order_location,'العنوان','Adress',$lang);
            if(isset($check))
            {
                return $check;
            }

            if($user_id != $order_location->user_id){
                $msg=$lang=='ar' ? 'العنوان لا يخصك'  : 'location not belongs to you';
                return $this->apiResponseMessage(0,$msg,200);
            }

    }



    /*
     * Add order one time (product and address and payment method)
     * validate discount code
     * validate address for user
     */
   public function add_order_once(Request $request)
    {
        $Settings=Settings::first();
        $lang=$request->header('lang');
        $user=Auth::user();

        // return $user->catr_price;
        // if($user->status == 0)
        // {
        //         $msg=$lang=='ar' ? 'لا يمكن عمل الطلب الا بعد تفعيل الحساب' : 'The order can only be made after activating the account ';
        //     return $this->apiResponseMessage(0,$msg, 200);

        // }
        
         if(count($user->my_cart)==0){
            $msg=$lang=='ar' ? 'لا توجد منتجات في عربة الشراء' : 'No products in your cart';
            return $this->apiResponseMessage(0,$msg, 200);
        }
        // $validate_address=$this->validate_address($request,$lang,$user->id);
        // if(isset($validate_address))
        // {
        //     return $validate_address;
        // }
        if($request->payment_method == 3)
        {
            $msg=$lang=='ar' ? 'خدمة الدفع عبر الانترنت غير مفعلة' : 'Online payment is not activated ';
            return $this->apiResponseMessage(0,$msg, 200);
        }

        $pro_ids = $user->my_cart->pluck('id');
        $pro_price = Product_details::select('price')->whereIn('id' , $pro_ids)->get();
        $price = 0;
        foreach ($pro_price as $row) {
            foreach($user->my_cart as $proo){
                $pr_price = ($proo->pivot->quantity)*($row->price);
                $price += $pr_price;
            }
        }
        
        if($user->status == '4'){
            $price_type = 'wholesale_wholesale_price';
        }elseif($user->status == '3'){
            $price_type = 'wholesale_price';
        }elseif($user->status == '1'){
            $price_type = 'selling_price';
        }

        $tax = $price*($Settings->tax/100);

        $total = $price+$Settings->shipping_price+$tax;

        $order=new P_d_orders();
        $order->user_id=$user->id;
        if(Representative_User::where('user_id',$user->id)->value('representative_id') != null){
            $rep_id = Representative_User::where('user_id',$user->id)->value('representative_id');
            $order->representative_id=$rep_id;
        }
        $order->safe_id = 1;
        $order->status=1;
        $order->pre_price = $price;
        $order->tax1 = $Settings->tax;
        $order->tax1_type = 2;
        $order->tax2_type = 1;
        $order->dis_type = 1;
        $order->total_price = $total;
        $order->rest = $total;
        $order->shipping_price=$Settings->shipping_price;
        $order->payment_method=$request->payment_method;
        $order->address_id=$request->address_id;
        $order->price_type = $price_type;
        if($request->bank_trans_receipt){
            $name=BaseController::saveImage('Order',$request->file('bank_trans_receipt'));
            $order->bank_trans_receipt = $name;
        }
        $order->bank_name = $request->bank_name;
        $order->notes = $request->notes;
        if($request->code_id != 0)
        {
            $code=Discount::find($request->code_id);
            $validate_code=$this->validate_code($code,$lang,$user);
            if(isset($validate_code))
            {
                return $validate_code;
            }
            $order->code_id=$request->code_id;
            $order->total_price=$user->catr_price;
        }
        $order->save();
        DB::table('carts')->where('type',0)->where('user_id',$user->id)->update([
            'type'=>1,
            'p_d_order_id'=>$order->id,
        ]);
        
        $order_prod = Cart::where('p_d_order_id',$order->id)->get();

        foreach($order_prod as $row){
            
            $p_d_price = Product_details::where('id',$row->product_detail_id)->value('price');

            $order_products = new Order_product_details;
            $order_products->P_d_order_id = $row->p_d_order_id;
            $order_products->product_detail_id = $row->product_detail_id;
            $order_products->inventory_id = 1;
            $order_products->quantity = $row->quantity;
            $order_products->price = $p_d_price;
            $order_products->color_id = $row->color_id;
            $order_products->size_id = $row->size_id;
            $order_products->cart_id = $row->id;
            $order_products->save();

        }

        // make user cart and code null
        $user->catr_price=null;
        $user->discount_id=null;
        $user->save();
        // relate between order and code
        $user_Discount=user_Discount::where('code_id',$request->code_id)->where('user_id',$user->id)->first();
        if(!is_null($user_Discount))
        {
            $user_Discount->p_d_order_id=$order->id;
            $user_Discount->save();
        }

        $this->handle_quantity($order->products);
        // EmailsController::order($user->email,$order,$lang);


        $msg=$lang=='ar' ?  'تم تسجيل الطلب بنجاح' :'order save successfully' ;
        return $this->apiResponseData(new Orders_MResource($order),$msg);
    }
    
    /** handle_quantity **/
    private function handle_quantity($products)
    {
        
        foreach($products as $row)
        {
           
            $p_d_qty = Product_detail_inventory::where('product_detail_id',$row->product_detail_id)->first();
            $qty = $p_d_qty->quantity;
            $n_qty = $qty-$row->quantity;

            $p_d_qty->quantity = $n_qty;
            $p_d_qty->save();

            // $row->quantity -= $row->quantity;
            // $row->save();
        }
    }

    /*
     * rate Order
     * after order is done
     */

    public function rate_order(Request $request,$order_id){
        $lang=$request->header('lang');
        $user=Auth::user();
        $order=P_d_orders::find($order_id);
        $validate_order=$this->validate_order($user,$order,$lang);
        if(isset($validate_order)){
            return $validate_order;
        }
        $order->rate=$request->rate;
        $order->save();
        $msg=$lang=='ar' ?  'تم حفظ تقييمك بنجاح' :'your rate saved successfully' ;
        return $this->apiResponseData(new Orders_MResource($order),$msg);
    }

    /*
      * raport Order
      * after order is done
     */

    public function raport_order(Request $request,$order_id){
        $lang=$request->header('lang');
        $user=Auth::user();
        $order=P_d_orders::find($order_id);
        $validate_order=$this->validate_order($user,$order,$lang);
        if(isset($validate_order)){
            return $validate_order;
        }
        $order->report=$request->report;
        $order->save();
        $msg=$lang=='ar' ?  'تم حفظ الشكوى بنجاح' :'your Complaint saved successfully' ;
        return $this->apiResponseData(new Orders_MResource($order),$msg);
    }

    /*
     * My Orders
     */
    public function my_order(Request $request){
        $lang=$request->header('lang');
        $user=Auth::user();
        $orders=P_d_orders::where('user_id',$user->id)->orderBy('created_at','desc')->get();
        $msg=$lang=='ar' ?  'طلباتك' :'your orders' ;
        return response()->json(['data'=>Orders_MResource::collection($orders),'message'=>$msg,'status'=>1]);
    }


    /*
     * filter by status
     */
    public function filter_my_order_byStatus(Request $request)
    {   
        $lang=$request->header('lang');
        $user = Auth::user();
        if ($request->status == 0){
            $orders = P_d_orders::where('user_id',$user->id)->orderBy('created_at','desc')->get();
        }else{
            $orders = P_d_orders::where('user_id',$user->id)->orderBy('created_at','desc')->where('status' , $request->status)->get();
        }
        $msg=$lang=='ar' ?  'طلباتك' :'your orders' ;
        return response()->json(['data'=>Orders_MResource::collection($orders),'message'=>$msg,'status'=>1]);
    }


    /*
     * Show order status
     */
    public function show_order_status(Request $request)
    {
        $status = Order_status::get();
        return $this->apiResponseData($status,'success');
    }



    /*
     * single_order
     */
    public function single_order(Request $request,$order_id){
        $lang=$request->header('lang');
        $user=Auth::user();
        $order=P_d_orders::find($order_id);
        $validate_order=$this->validate_order($user,$order,$lang);
        if(isset($validate_order)){
            return $validate_order;
        }
        $msg=$lang=='ar' ?  'تمت العملية بنجاح' :'success' ;
        return $this->apiResponseData(new Orders_MResource($order),$msg);
    }

    /*
     * @pram $user , $order , $lang
     * To validate isset order or user own this order to make changes
     * @return message
     */
    private function validate_order($user,$order,$lang){
        $check=$this->not_found($order,'الطلب','Order',$lang);
        if(isset($check))
        {
            return $check;
        }
        if($user->id != $order->user_id){
            $msg=$lang=='ar' ? 'هذا الطلب لا يخصك' : 'this order not belongs to you';
            return $this->apiResponseMessage(0,$msg, 200);
        }
    }

    /*
     * check discount code if user in a code
     * one to many code with orders
     */

    public function check_discount_code(Request $request)
    {
        $lang=$request->header('lang');
        $user=Auth::user();

        $code=Discount::where('code',$request->code)->whereDate('end_date','>=',now())->first();
            $validate_code=$this->validate_code($code,$lang,$user);
            if(isset($validate_code)){
                return $validate_code;
            }
            $use=user_Discount::where('code_id',$code->id)->where('user_id',$user->id)->first();
            $check_use=$use->is_use;
            if($check_use ==1)
            {
                $msg=$lang=='ar' ? 'تم استخدام الكود بالفعل' : 'code is already use';
                return $this->apiResponseMessage(0,$msg,200);
            }
            $total_price=(double)BaseController::get_total_price($user->my_cart,$user);
            $discount=OrderController::getAmount($code,$total_price);
            $use->is_use=1;
            $use->save();
            $user->catr_price=$discount[0];
            $user->discount_id=$code->id;
            $user->save();

            $data=['code_id'=>$code->id , 'total_price'=>$user->catr_price];
            $msg=$lang=='ar' ?  'تم خصم قيمة ' . $discount[1]: 'Thank You.... You Have Save ' . $discount[1] . ' From Your Order';
            return $this->apiResponseData($data,$msg,200);

    }

    /*
     * validate code
     */

    private function validate_code($code,$lang,$user)
    {
        $check=$this->not_found($code,'الكود','code',$lang);
        if(isset($check)){
            return $check;
        }

        $is_you = $code->user_code->contains($user->id);
        if($is_you == false){
            $msg=$lang=='ar' ? 'الكود غير موجود' : 'code not found for you';
            return $this->apiResponseMessage(0,$msg,200);
        }

    }

    /*
     * get Amount by check is prcentage or just number
     */
    public static function getAmount($code,$total_price)
    {
        if($code->amount_type == 'percentage')
        {
            $price_amount=(double)$total_price * (double)$code->amount / 100;
            $price_after_discount=(double)$total_price-(double)$price_amount;
        }else{
            $price_amount=$code->amount;
            $price_after_discount=(double)$total_price - (double)$code->amount ;
        }
        return [(double)$price_after_discount,(double)$price_amount];
    }

    /*
     * remove code
     */
    public function remove_code(Request $request,$code_id)
    {
        $lang=$request->header('lang');
        $user=Auth::user();

        $code=Discount::find($code_id);
        $validate_code=$this->validate_code($code,$lang,$user);
        if(isset($validate_code)){
            return $validate_code;
        }
        $use=user_Discount::where('code_id',$code->id)->where('user_id',$user->id)->where('p_d_order_id',null)->first();
        if(is_null($use))
        {
            $msg=$lang=='ar' ? 'لا يمكن الحذف' : 'cant delete code';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $total_price=(double)BaseController::get_total_price($user->my_cart,$user);
        if($use->is_use == 0)
        {
            $msg = $lang == 'ar' ? 'هذا الكود غير مستخدم'  :'code is not use';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $discount=$this->getAmount($code,$total_price);
        $use->is_use=0;
        $use->save();
        $user->catr_price=$total_price;
        $user->discount_id=null;
        $user->save();

        $data=['code_id'=>$code->id , 'total_price'=>$total_price];
        $msg=$lang=='ar' ? 'تم حذف الخصم' : 'code removed';
        return $this->apiResponseData($data,$msg,200);


    }


    public function order_user_addresses (Request $request , $user_id){
        $lang=$request->header('lang');
        $user=User::where('id',$user_id)->first();
        $msg=$lang=='ar' ? 'تمت العملية بنجاح'  : 'success';
        return $this->apiResponseData(locationResource::collection($user->my_address),$msg);
    }

    public function add_user_address(Request $request , $user_id)
    {
        $lang=$request->header('lang');
        $address=new order_locations();
        $address->lat=$request->lat;
        $address->lng=$request->lng;
        $address->address=$request->address;
        $address->user_id=$user_id;
        $address->save();
        $msg=$lang=='ar' ? 'تم تسجيل العنوان بنجاح' : 'location saved successfully';
        return $this->apiResponseData(new locationResource($address),$msg,200);
    }

    
}