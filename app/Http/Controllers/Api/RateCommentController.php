<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\CartResource;
use App\Http\Resources\CommentResource;
use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File;
use App\Models\Products;
use App\Models\Product_details;
use App\Http\Resources\Product_detailsResource;
use App\Models\Rate;
use App\Http\Controllers\Manage\BaseController;
use App\Http\Resources\ProductResource;

class RateCommentController extends Controller
{
    use ApiResponseTrait;

    /*
     * @pram request array & product_id
     * new rate  ==>to calcluate new rate .
     * @return  response()->json
     */

    public function save_my_rate(Request $request,$product_id)
    {
        $lang = $request->header('lang');
        $user=Auth::user();
        $product=Product_details::where('id',$product_id)->where('status',1)->first();
        $check=$this->not_found($product,'المنتج','product',$lang);
        if(isset($check))
        {
            return $check;
        }
        if(!$request->rate)
        {
            $msg=$lang=='ar' ? 'من فضلك اضف النقييم'  : 'rate is required';
            return $this->apiResponseMessage(0,$msg,200);
        }
        if($request->rate > 5)
        {
            $msg=$lang=='ar' ? 'التقيم لا يمكن ان يكون اكبر من 5'  : 'rate cannot be higher than 5';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $rate=Rate::where('user_id',$user->id)->where('product_detail_id',$product_id)->first();
        if(is_null($rate))
        {
            $rate=new Rate;
            $rate->user_id=$user->id;
            $rate->product_detail_id=$product_id;
            $rate->rate=$request->rate;
            $rate->comment=$request->comment;
            $rate->save();
            $msg=$lang=='ar' ? 'تم اضافة التقييم بنجاح'  : 'your rate added successfully';
        }else{
            $rate->rate=$request->rate;
            $rate->comment=$request->comment;
            $rate->save();
            $msg=$lang=='ar' ? 'تم تعديل تقييمك بنجاح'  : 'your rate edited successfully';
        }
        $new_rate=BaseController::get_rate($product_id);
        $product->rate=$new_rate;
        $product->save();
        return $this->apiResponseMessage(1,$msg,200);
    }

    /*
     * make comment
     */
    public function save_my_comment(Request $request,$product_id)
    {
        $lang = $request->header('lang');
        $user=auth('api')->user();
        $product=Product_details::where('id',$product_id)->where('status',1)->first();
        $check=$this->not_found($product,'المنتج','product',$lang);
        if(isset($check))
        {
            return $check;
        }
        if(!$request->comment)
        {
            $msg=$lang=='ar' ? 'من فضلك اضف التعليق'  : 'comment is required';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $comment=new Comment();
        $comment->product_detail_id=$product_id;
        $comment->user_id=$user->id;
        $comment->comment=$request->comment;
        $comment->save();
        $msg=$lang=='ar' ? 'تم اضافة التعليق' : 'comment added successfully';
        return $this->apiResponseData($comment,$msg,200);
    }
    /*
     * edit_my_comment
     */
    public function edit_my_comment(Request $request,$comment_id)
    {
        $lang = $request->header('lang');
        $user=auth('api')->user();
        $comment=Comment::find($comment_id);
        $validate_comment=$this->validate_comment($comment,$user,$lang);

        if(isset($validate_comment))
        {
            return $validate_comment;
        }
        if(!$request->comment){
            $msg=$lang=='ar' ? 'من فضلك اضف التعليق'  : 'comment is required';
            return $this->apiResponseMessage(0,$msg,200);
        }

        $comment->comment=$request->comment;
        $comment->save();
        $msg=$lang=='ar' ? 'تم تعديل التعليق' : 'comment edited successfully';
        return $this->apiResponseMessage($comment,$msg,200);
    }

    /*
     * delete_my_comment
     */
    public function delete_my_comment(Request $request,$comment_id)
    {
        $lang = $request->header('lang');
        $user=auth('api')->user();
        $comment=Comment::find($comment_id);
        $validate_comment=$this->validate_comment($comment,$user,$lang);
        if(isset($validate_comment))
        {
            return $validate_comment;
        }
        $comment->delete();
        $msg=$lang=='ar' ? 'تم حذف التعليق'  : 'comment deleted successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }

    /*
     * validate comment
     * validate exists comment ,user has this comment
     */

    private function validate_comment($comment,$user,$lang)
    {
        if(is_null($comment)){
            $msg=$lang=='ar' ? 'هذا التعليق غير موجود'  : 'this comment dose not exist';
            return $this->apiResponseMessage(0,$msg,200);
        }
        if($comment->user_id != $user->id)
        {
            $msg=$lang=='ar' ? 'عفوا لا يمكنك التعديل على هذا التعليق'  : 'sorry you cannot edit this comment';
            return $this->apiResponseMessage(0,$msg,200);
        }
    }

    /*
     * get all comment of custom product
     */
    public function get_comments(Request $request,$product_id)
    {
        $comments=Comment::where('product_detail_id',$product_id)->get();
        return $this->apiResponseData(CommentResource::collection($comments),'success',200);
    }
}
