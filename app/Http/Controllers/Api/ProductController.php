<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\CartResource;
use App\Models\Discount;
use App\Models\Shop;
use App\Models\Product_detail_inventory;
use App\Models\Order_product_details;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,DB;
use App\Http\Resources\UserResource;
use App\Http\Resources\Inventory_product_details_resource;
use App\User;
use App\Models\Products;
use App\Models\Product_details;
use App\Models\Rep_Inv;
use App\Models\Inventory;
use App\Http\Resources\Product_detailsResource;
use App\Http\Resources\Products_MResource;
use App\Http\Controllers\Manage\BaseController;
use App\Http\Resources\ProductResource;
use App\Models\Whishlist;
use App\Models\Cart;
use App\Models\color;
use App\Models\Sizes;


class ProductController extends Controller
{
    use ApiResponseTrait;

    /*
     * @pram request array & product_id
     * product_view  ==>to save new view form user to product (Data analysis).
     * @return  response()->json
     */

    public function prduct_detials(Request $request,$product_id)
    {

        $lang = $request->header('lang');
        $user=auth('api')->user();
        $product=Product_details::where('id',$product_id)->where('status',1)->first();
        $check=$this->not_found($product,'المنتج','product',$lang);
        if(isset($check))
        {
            return $check;
        }
        if(!is_null($user)) {
            BaseController::product_view($product_id, $user->id);
        }
        $msg=$lang=='ar' ? 'تمت العملية بنجاح'  : 'success';
        return $this->apiResponseData(new Products_MResource($product),$msg,200);
    }

        /*
         * @pram request array & cat_id & shop_id
         * @return  response()->json
         */
    public function products_by_category(Request $request,$cat_id)
    {
        // $page=$request->page *20;
        $lang = $request->header('lang');

        $invs = Inventory::where('status',1)->pluck('id');
        $invPro = Product_detail_inventory::where('inventory_id',1)->where('quantity','!=',0)->pluck('product_detail_id');
    
        if($cat_id == 1){
            $product_details = Product_details::whereIn('id',$invPro)->where('status',1)->orderBy('id','DESC')->get();
        }else{
            $pro = Products::where('cat_id', $cat_id)->pluck('id');
            $product_details = Product_details::whereIn('product_id' , $pro)->whereIn('id',$invPro)->where('status',1)->orderBy('id','DESC')->get();
        }
        
        $msg=$lang=='ar' ? 'تمت العملية بنجاح'  : 'success';
        return $this->apiResponseData(Products_MResource::collection($product_details),$msg,200);
    }

    /*
       * @pram request array & $product_id
       * @return  response()->json
       */
    public function wishlist(Request $request,$product_id){
        $lang = $request->header('lang');
        $user=Auth::user();
        $product=Product_details::where('id',$product_id)->where('status',1)->first();
        $check=$this->not_found($product,'المنتج','product',$lang);
        if(isset($check))
        {
            return $check;
        }
        $Whishlist=Whishlist::where('product_detail_id',$product_id)->where('user_id',$user->id)->first();
        if(is_null($Whishlist)){
            $msg=$lang=='ar' ? 'تم اضافة المنتج في المفضلات' : 'Product added in Whishlist';
            $check=1;
            $Whishlist=new Whishlist;
            $Whishlist->product_detail_id=$product_id;
            $Whishlist->user_id=$user->id;
            $Whishlist->save();

        }else{
            $check=0;
            $Whishlist->delete();
            $msg=$lang=='ar' ? 'تم حذف المنتج من المفضلات' : 'Product deleted from Whishlist';

        }
        if($request->getCount==1)
        {
            $data=['get_count'=>Auth::user()->my_wishlist->count(),'check'=>$check];
            return $this->apiResponseData($data,$msg,200);

        }
        return $this->apiResponseMessage(1,$msg,200);

    }


    /*
       * @pram request array & $product_id [color_id,size_id,quantity]
       * @return  response()->json
     */

    public function add_to_cart(Request $request,$product_id){
        $lang = $request->header('lang');
        $user=Auth::user();
        $product=Product_details::where('id',$product_id)->where('status',1)->first();
        $check=$this->not_found($product,'تفاصيل المنتج','product_details',$lang);
        if(isset($check))
        {
            return $check;
        }

        $pro_qty = Product_detail_inventory::where('product_detail_id',$product_id)->get();
        $product_qty = $pro_qty->sum('quantity');

        $input = $request->all();
        
        $validationMessages = [
            'quantity.required' => $lang == 'ar' ? ' من فضلك ادخل الكمية' :"required is required"  ,
        ];

        $validator = Validator::make($input, [
            'quantity' => 'required',
        ], $validationMessages);

        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 200);
        }
        if($request->quantity > $product_qty){
            $msg=$lang == 'ar' ? 'الكمية المطلوبة غير متوفرة' :"Required quantity is not available"  ;
            return $this->apiResponseMessage(0, $msg, 200);
        }
        
        // $p_d_qty = Product_detail_inventory::where('product_detail_id',$product_id)->first();
        // $qty = $p_d_qty->quantity;
        // if ($request->quantity >= $qty){
        //     $msg=$lang == 'ar' ?'الكمية بالمخزن أقل من المطلوبة' : 'Quantity in inventory less than demanded';
        //     return $this->apiResponseMessage(0,$msg,200);
        // }
            
        $Cart=Cart::where('user_id',$user->id)->where('product_detail_id',$product_id)->where('type',0)->first();
        if(is_null($Cart)) {
            $Cart = new Cart;
            $Cart->product_detail_id = $product_id;
            $Cart->user_id = $user->id;
            $Cart->color_id = $request->color_id;
            $Cart->size_id = $request->size_id;
            $Cart->quantity = $request->quantity;
            $Cart->type = 0;
            $Cart->save();

            $Cart_id = $Cart->id;

            $msg = $lang == 'ar' ? 'تم اضافة المنتج في عربة التسوق' : 'Product added in Cart';
            return $this->apiResponseMessage(1, $msg, 200);
        }
        $Cart->color_id = $request->color_id;
        $Cart->size_id = $request->size_id;
        $Cart->quantity = $request->quantity;
        $Cart->save();

        $msg = $lang == 'ar' ? 'تم التعديل بنجاح' : 'Edited successfully';
        return $this->apiResponseMessage(1, $msg, 200);
    }

    // delete from cart
    public function delete_from_cart(Request $request,$product_id){
        $lang = $request->header('lang');
        $user=Auth::user();
        $product=Product_details::where('id',$product_id)->where('status',1)->first();
        $check=$this->not_found($product,'تفاصيل المنتج','product_detail',$lang);
        if(isset($check))
        {
            return $check;
        }
        
        $Cart=Cart::where('user_id' , $user->id)->where('product_detail_id' , $product_id)->where('type' , 0)->first();
        $check=$this->not_found($Cart,'المنتج بالعربة','product in cart',$lang);
        if(isset($check))
        {
            return $check;
        }
        $Cart->delete();
        $msg = $lang == 'ar' ? 'تم الحذف بنجاح' : 'deleted successfully';
        return $this->apiResponseMessage(1, $msg, 200);
    }

    /*
     * all products high rated, rated , nearby
     */
    public function get_products(Request $request)
    {
        $lang = $request->header('lang');
        // $page=$request->page;
        // $products=Product_details::where('status',1);
        if($request->type== 1){
            $products = Product_details::where('status',1)->orderBy('rate', 'desc')->get();
        }elseif($request->type==2){
            $products = Product_details::where('status',1)->whereHas('orders')->withCount('orders')->orderBy('orders_count','desc')->get();
        }elseif($request->type==3){
            $products = Product_details::where('status',1)->where('is_offer', 1)->get();
        }elseif($request->type == 4){
            $products = Product_details::where('status',1)->orderBy('created_at', 'desc')->get();
        }
        // $product=$products->orderBy('created_at', 'desc')->get();
        $msg = $lang == 'ar' ? 'تمت العملية بنجاح' : 'success';
        return $this->apiResponseData(Product_detailsResource::collection($products),$msg,200);
    }

    /*
     * update cart from custom user
     * @pram recive array of products objects
     * @return my new cart
     */

    public function update_cart(Request $request)
    {
        $user=Auth::user();
        $products=$request->products;
        if(gettype($request->products)=='string')
        {
            $products=json_decode($request->products,true);
        }
        $lang=$request->header('lang');
        $shop=Shop::find(1);
        Cart::where('user_id',$user->id)->where('type',0)->delete();
        foreach($products as $row)
        {
            $product=Product_details::find($row['product_detail_id']);
            if(is_null($product)) {
                $msg=$lang=='ar' ? ' المنتج رقم ' .$row['product_detail_id'].' غير موجود ': 'product number ' . $row['product_detail_id'] .' not found';
                Cart::where('user_id',$user->id)->where('type',0)->delete();
                return $this->apiResponseMessage('0',$msg,200);
               
            }else{
                $product_qty = Product_detail_inventory::where('product_detail_id',$row['product_detail_id'])->where('inventory_id',1)->first();
                // return $product_qty; 
                if($row['quantity'] > $product_qty->quantity)
                {
                    $main_product = Product::where('id',$product->product_id)->first();
                    $name=$lang=='ar' ? $main_product->name_ar : $main_product->name_en;
                    $msg=$lang=='ar' ?  $name.' الكمية المطلوبة غير متوفرة للمنتج  ': 'The required quantity of the product  ' . $name .' is not available';
                    return $this->apiResponseMessage('0',$msg,200);
                }
                $cart=new Cart;
                $cart->user_id=$user->id;
                $cart->product_detail_id=$row['product_detail_id'];
                if(array_key_exists('color_id',$row))
                    $cart->color_id=$row['color_id'];
                if(array_key_exists('size_id',$row))
                    $cart->size_id=$row['size_id'];
                $cart->quantity=$row['quantity']? $row['quantity']:1;
                $cart->save();
            }
            // return $cart;
        }
        $discount=Discount::find($user->discount_id);
        if(is_null($discount))
        {
            $price =0;
            foreach($user->my_cart as $product){
                $price += $product->price;
            }
        }else{
            $price=OrderController::getAmount($discount,BaseController::get_total_price($user->my_cart,$user));
            $price=$price[0];
        }
        $user->catr_price=$price;
        $user->save();
        $msg=$lang=='ar' ? 'تمت اضافة المنتجات بنجاح' : 'products added successfully';
        $total_price=$user->catr_price;
        $data=['my_cart'=>CartResource::collection($user->my_cart),'products_price'=>$total_price,'shipping_price'=>(double)$shop->shipping_price];
        return $this->apiResponseData($data,$msg);
    }



    public function all_Product_details(Request $request)
    {
        $user=Auth::user();
        $inv_id = Rep_Inv::where('representative_id',$user->id)->value('inventory_id');
        $Product_details = Product_detail_inventory::where('inventory_id' , $inv_id)->get();
        return $this->apiResponseData(Inventory_product_details_resource::collection($Product_details),'success');
    }


    /*
     * Show single Product_details
     */
    public function single_Product_details(Request $request,$Product_details_id){
        $lang=$request->header('lang');
        $Product_details=Product_details::find($Product_details_id);
        $check=$this->not_found($Product_details,'تفاصيل المنتج','Product_details',$lang);
        if(isset($check)){
            return $check;
        }
        $msg=$lang=='ar' ?'تمت العملية بنجاح' : 'success';
        return $this->apiResponseData(new Products_MResource($Product_details),$msg);
    }


    public function similar_Product_details(Request $request,$Product_details_id){
        $lang=$request->header('lang');
        $Product_details=Product_details::find($Product_details_id);
        $invs = Inventory::where('status',1)->pluck('id');
        $invPro = Product_detail_inventory::where('inventory_id',1)->where('quantity','!=',0)->pluck('product_detail_id');
        $similar_products = Product_details::where('product_id',$Product_details->product_id)->whereIn('id',$invPro)->where('id','!=',$Product_details_id)->get();
        $msg=$lang=='ar' ?'تمت العملية بنجاح' : 'success';
        return $this->apiResponseData(Products_MResource::collection($similar_products),$msg);
    }

}
