<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\NotificationMethods;
use App\Http\Controllers\Shop_Api\SafeController;
use App\Http\Resources\ColorResource;
use App\Http\Resources\Order_product_contResource;
use App\Http\Resources\MostsellResource;
use App\Http\Resources\Product_detailsResource;
use App\Http\Resources\UserAddressResource;
use App\Models\Order_product_details;
use App\Models\Product_details;
use App\Models\Products;
use App\Models\Price;
use App\Models\Product_details_price;
use App\Models\Product_detail_inventory;
use App\Models\Safe;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\order_locations;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Resources\P_d_ordersResource;
use App\Http\Resources\Order_product_detailsResource;
use App\Http\Resources\locationResource;
use App\Http\Resources\Order_returnResource;
use App\Http\Resources\Representative_UserResource;
use App\Http\Resources\Shop\UserResource;
use App\Http\Resources\Representative_debt_logResource;
use App\Http\Resources\Orders_MResource;
use App\User;
use App\Models\P_d_orders;
use App\Models\Shop;
use App\Models\color;
use App\Models\Sizes;
use App\Models\Rep_Inv;
use App\Models\Order_return;
use App\Models\Representative_User;
use App\Models\Representative_debt_log;
use DB;
use App\Http\Controllers\Manage\BaseController;
use Carbon\Carbon;

class Rep_orderController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    /*
     * get all P_d_orders
     */
    public function all_p_d_Order (Request $request){
        $user = Auth::user(); 
        $P_d_orders = P_d_orders::where('status','1')->where('representative_id',$user->id)->has('Products')->orderBy('created_at','desc')->get();
        return $this->apiResponseData(Orders_MResource::collection($P_d_orders),'success');
    }


    public function finished_orders(Request $request)
    {
        $user = Auth::user();
        $P_d_orders = P_d_orders::orderBy('created_at','desc')->where('status','10')->where('representative_id',$user->id)->get();
        return $this->apiResponseData(Orders_MResource::collection($P_d_orders),'success');
    }




    /*
     * Filter P_d_order
     */
    public function filter_P_d_orders(Request $request)
    {
        $user = Auth::user();
        $P_d_orders=P_d_orders::where('representative_id',$user->id)->orderby('created_at','desc');
        if ($request->type == 1)
        {
            $P_d_orders = $P_d_orders->whereDay('created_at', now());
        }elseif ($request->type==2) {
            $P_d_orders = $P_d_orders->whereMonth('created_at', now());
        }elseif ($request->type==3){
            $P_d_order= $P_d_orders->whereYear('created_at', now());
        }else{
            return $this->apiResponseMessage(0,'هذا النوع غير صحيح',200);
        }

        $P_d_orders=$P_d_orders->get();
        return $this->apiResponseData(Orders_MResource::collection($P_d_orders),'success');

    }


    
    /*
     * Filter P_d_order
     */
    public function filter_P_d_orders_by_date(Request $request)
    {
        $user = Auth::user();
        $from = date($request->from);
        $to = date($request->to);
        $dated_orders = P_d_orders::where('representative_id',$user->id)->whereBetween('created_at', [$from." 00:00:00",$to." 23:59:59"])->get();
        return $this->apiResponseData(Orders_MResource::collection($dated_orders),'success');

    }

    /*
     * filter by status
     */
    public function filter_by_status(Request $request)
    {
        $user = Auth::user();

        if ($request->status == 0){
            $P_d_orders=P_d_orders::where('representative_id',$user->id)->orderBy('created_at','desc')->get();
        }else{
            $P_d_orders=P_d_orders::where('representative_id',$user->id)->orderBy('created_at','desc')->where('status',$request->status)->get();
        }
        return $this->apiResponseData(Orders_MResource::collection($P_d_orders),'success');
    }


    /*
    * Delete P_d_orders
    */
    public function delete_P_d_order(Request $request,$P_d_order_id)
    {
        $lang=$request->header('lang');

        $P_d_order=P_d_orders::find($P_d_order_id);
        $check=$this->not_found($P_d_order,'الطلب','P_d_order',$lang);
        if(isset($check)){
            return $check;
        }

        if($P_d_order->bank_trans_receipt != null) {
            BaseController::deleteFile('Order',$P_d_order->bank_trans_receipt);
        }

        $old_products = Order_product_details::where('p_d_order_id',$P_d_order_id)->get();
        if(isset($old_products)){
            foreach($old_products as $row){
                
                $return_quntuty = $row->quantity;

                $qty_handling = Product_detail_inventory::where('product_detail_id',$row->product_detail_id)->where('inventory_id',$row->inventory_id)->first();
                if(!is_null($qty_handling)){
                    $qty_handling->quantity += $return_quntuty;
                    $qty_handling->save();
                }else{
                    $proInInv = Product_detail_inventory::where('product_detail_id',$row->product_detail_id)->first();
                    $proInInv->quantity += $return_quntuty;
                    $proInInv->save();
                }

                $row->delete();
            }
        }

        if($P_d_order->user_id != null){
            $user = User::find($P_d_order->user_id);
            $user_debt = ($user->debt)-($P_d_order->rest);
            $user->debt = $user_debt;
            $user->save();
        }

        if($P_d_order->representative_id != null){
            $rep = User::find($P_d_order->representative_id);
            $representative_debt = ($rep->debt)-($P_d_order->paid);
            $rep->debt = $representative_debt;
            $rep->save();
        }

        if($P_d_order->paid != 0 ){
            $safe_detail = new \stdClass();
            $safe_detail->safe_id = $P_d_order->safe_id;
            $safe_detail->process_name_ar = 'حذف طلب';
            $safe_detail->process_name_en = 'Order Deleting';
            $safe_detail->process_type = 2;
            $safe_detail->value = $P_d_order->paid;
            $safe_detail->notes = "حذف طلب رقم $P_d_order_id";
            SafeController::add_safe_process($safe_detail);
        }

        $P_d_order->delete();

        $msg=$lang=='ar' ? 'تم حذف الطلب بنجاح'  : 'P_d_order Deleted successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }

    /*
     * Edit status
     */

    public function edit_status(Request $request,$P_d_order_id)
    {
        $user = Auth::user();

        $lang=$request->header('lang');
        $P_d_order=P_d_orders::find($P_d_order_id);
        $check=$this->not_found($P_d_order,'الطلب','P_d_order',$lang);
        if(isset($check)){
            return $check;
        }
        $P_d_order->status=$request->status;
        $P_d_order->save();
        $msg=$lang=='ar' ? 'تم تعديل الحالة بنجاح'  : 'status updated successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }

    /*
     * filrt
     */
    private function sentNotifcation($P_d_order)
    {

        $redirct_id=$P_d_order->id;
        $user=User::find($P_d_order->user_id);
        $token=$user->fire_base_token;
        $lang=$user->lang;
        if($P_d_order->status == 2)
        {
            $title=$lang=='ar' ? 'الطلب قيد التنفيذ'  : 'P_d_order in progress';
            $desc=$lang=='ar' ? 'وهو الان قيد التنفيذ '.$P_d_order->id.'تم قبول الطلب رقم '  : 'P_d_order in progress';
        }elseif ($P_d_order->status == 3)
        {
            $title=$lang=='ar' ? 'الطلب اكتمل'  : 'P_d_order  completed';
            $desc=$lang=='ar' ? $P_d_order->id.'تم اكتمال الطلب رقم '  : 'P_d_order completed';
        }
        NotificationMethods::senNotificationToSingleUser($token,$title,$desc,null,3,$redirct_id);
    }

    /*
     * Single P_d_order to show data
     */
    public function single_P_d_order(Request $request,$P_d_order_id)
    {
        $user = Auth::user();

        $lang=$request->header('lang');
        $P_d_order=P_d_orders::find($P_d_order_id);
        $check=$this->not_found($P_d_order,'الطلب','P_d_order',$lang);
        if(isset($check)){
            return $check;
        }
        return $this->apiResponseData(new Orders_MResource($P_d_order),'success');
    }

    /*
     * Add new P_d_order
     * add general P_d_order column , add products to this P_d_order
     */

    public function add_P_d_order(Request $request)
    {
        $representative=Auth::user();
        $lang=$request->header('lang');

        $validate_P_d_order=$this->validate_P_d_order($request);
        if(isset($validate_P_d_order)){
            return $validate_P_d_order;
        }

        $user=$request->user_id;
        $user_d = User::find($user);

        $check=$this->not_found($user_d,'العضو','user',200);
        if(isset($check))
        {
            return $check;
        }

        $validate_address=$this->validate_address($request,$lang,$request->user_id);
        if(isset($validate_address))
        {
            return $validate_address;
        }

        if($user_d->status == '4'){
            $price_type = 'wholesale_wholesale_price';
        }elseif($user_d->status == '3'){
            $price_type = 'wholesale_price';
        }elseif($user_d->status == '1'){
            $price_type = 'selling_price';
        }

        $price_type_req=$request->price_type;
        // if($price_type_req == '1'){
        //     $price_type = 'wholesale_wholesale_price';
        // }elseif($price_type_req == '2'){
        //     $price_type = 'wholesale_price';
        // }elseif($price_type_req == '3'){
        //     $price_type = 'selling_price';
        // }
        $shop = Shop::where('id',$request->shop_id)->first();

        if($representative->safe_id == null){
            $safe = new Safe;
            $safe->name_ar = "خزنة $representative->first_name $representative->last_name";
            $safe->name_en = "$representative->first_name $representative->last_name Safe";
            $safe->status = 1;
            $safe->total_money = $representative->debt;
            $safe->save();

            $representative->safe_id = $safe->id;
            $representative->save();
        }

        $P_d_order=new P_d_orders();
        $P_d_order->user_id=$user;
        $P_d_order->representative_id=$representative->id;
        $P_d_order->status=$request->status;
        $P_d_order->shipping_price=0;
        $P_d_order->payment_method=$request->payment_method;
        $P_d_order->address_id=$request->address_id;
        $P_d_order->price_type=$price_type;
        $P_d_order->safe_id = $representative->safe_id;
        $P_d_order->save();
        $msg=$lang=='ar' ?  'تم تسجيل الطلب بنجاح' :'P_d_order saved successfully' ;
        return $this->apiResponseData(new Orders_MResource($P_d_order),$msg);
    }

    /*
     * custom function to validate Address To P_d_orders
     */

    private function validate_address($request,$lang,$user_id)
    {

        if(!$request->address_id)
        {
            $msg=$lang=='ar' ? 'من فضلك ادخل العنوان'  : 'please send location';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $P_d_order_location=order_locations::find($request->address_id);
        $check=$this->not_found($P_d_order_location,'العنوان','Adress',$lang);
        if(isset($check))
        {
            return $check;
        }

        if($user_id != $P_d_order_location->user_id)
        {
            $msg=$lang=='ar' ? 'العنوان لا يخص العضو'  : 'location not belongs to this user';
            return $this->apiResponseMessage(0,$msg,200);
        }

    }

    /*
     * Add Product To P_d_order
     */

    public function add_product_to_P_d_order(Request $request,$P_d_order_id)
    {
        $lang=$request->header('lang');
        $representative=Auth::user();

        $P_d_order=P_d_orders::find($P_d_order_id);
        $check=$this->not_found($P_d_order,'الطلب','P_d_order',$lang);
        if(isset($check)){
            return $check;
        }

        $rep_inv_id = Rep_Inv::where('representative_id' , $representative->id)->value('inventory_id');

        $order_req = $request->all();
        foreach($order_req['orders'] as $orders)
        {
            $product_detail=Product_details::find($orders['product_detail_id']);
            $product_detail_price=Product_details_price::where('product_detail_id',$orders['product_detail_id'])->first();
            $p_d_qty = Product_detail_inventory::where('product_detail_id',$orders['product_detail_id'])->where('inventory_id',$rep_inv_id)->first();
            
            $check=$this->not_found($p_d_qty,'المنتج','Product_detail',$lang);
            if(isset($check)){
                $ch = $orders['product_detail_id'];
                $msg=$lang == 'ar' ?"المنتج رقم $ch غير موجود بالمخزن":"Product number $ch not found in inventory";
                return $this->apiResponseMessage(1,$msg,200);
            }

            if($orders['color_id']){
                $color=color::find($orders['color_id']);
                $check=$this->not_found($color,'اللون','Color',$lang);
                if(isset($check)){
                    $ch = $orders['color_id'];
                    $msg=$lang == 'ar' ?"اللون رقم $ch غير موجود":"Color number $ch not found";
                    return $this->apiResponseMessage(1,$msg,200);
                }
            }

            if($orders['size_id']){
                $size=Sizes::find($orders['size_id']);
                $check=$this->not_found($size,'الحجم','Size',$lang);
                if(isset($check)){
                    $ch = $orders['size_id'];
                    $msg=$lang == 'ar' ?"الحجم رقم $ch غير موجود":"Size number $ch not found";
                    return $this->apiResponseMessage(1,$msg,200);
                }
            }

            $P_d_order_product=Order_product_details::where('P_d_order_id',$P_d_order_id)->where('product_detail_id',$orders['product_detail_id'])->first();
            if(!is_null($P_d_order_product)){
                $ch = $orders['product_detail_id'];
                $msg=$lang == 'ar' ?"المنتج $ch موجود في هذا الطلب" : "Product $ch already exist in this P_d_order";
                return $this->apiResponseMessage(0,$msg,200);
            }

            // $p_d_qty = Product_detail_inventory::where('product_detail_id',$orders['product_detail_id'])->first();
            $qty = $p_d_qty->quantity;
            if ($orders['quantity'] > $qty){
                $ch = $orders['product_detail_id'];
                $msg=$lang == 'ar' ?"كمية المنتج $ch بالمخزن أقل من المطلوبة" : "Quantity of product $ch in inventory less than demanded";
                return $this->apiResponseMessage(0,$msg,200);
            }
            $n_qty = $qty-$orders['quantity'];
            // return $n_qty;
            
            $p_d_qty->quantity = $n_qty;
            $p_d_qty->save();

            $price_type = $P_d_order->price_type;
            $price = Price::select($price_type)->where('id',$product_detail_price->price_id)->first();
            // return $price->$price_type;

            $order_products = new Order_product_details;
            $order_products->P_d_order_id = $P_d_order_id;
            $order_products->product_detail_id = $orders['product_detail_id'];
            $order_products->inventory_id = $rep_inv_id;
            $order_products->quantity = $orders['quantity'];
            $order_products->price = $price->$price_type;
            $order_products->color_id = $orders['color_id'];
            $order_products->size_id = $orders['size_id'];
            $order_products->save();

        }

        $pro_qtys = Order_product_details::select()->where('p_d_order_id',$P_d_order_id)->get();
        $priceSum = 0;
        foreach ($pro_qtys as $row) {
            $pro_price = ($row->quantity)*($row->price);
            $priceSum += $pro_price;
        }
        $P_d_order->pre_price = $priceSum;
        $P_d_order->total_price = $priceSum;
        $P_d_order->save();

        $order = P_d_orders::find($P_d_order_id);
        $msg=$lang == 'ar' ?'تم اضافة المنتجات للطلب بنجاح' : 'Products added to order successfully';
        return $this->apiResponseData(new Orders_MResource($order),$msg);
    }

    public function make_bill(Request $request, $p_d_order_id){
        $lang=$request->header('lang');
        $p_d_order = Order_product_details::where('p_d_order_id',$p_d_order_id)->first();
        $check=$this->not_found($p_d_order,'الطلب','P_d_order',$lang);
        if(isset($check)){
            $msg=$lang == 'ar' ?'الطلب لا يحتوي علي منتجات' : 'order has no products';
            return $this->apiResponseMessage(0,$msg,200);
        }

        $pro_qtys = Order_product_details::select()->where('p_d_order_id',$p_d_order_id)->get();
        $price = 0;
        foreach ($pro_qtys as $row) {
            $pro_price = ($row->quantity)*($row->price);
            $price += $pro_price;
        }

        $discount_type = $request->discount_type;
        $disV = $request->discount;
        $tax1_type = $request->tax1_type;
        $tax1V = $request->tax1;
        $tax2_type = $request->tax2_type;
        $tax2V = $request->tax2;

        if ($discount_type == '2'){
            $dis = $price*($disV/100);
        }else{
            $dis = $disV;
        }

        if ($tax1_type == '2'){
            $tax1 = $price*($tax1V/100);
        }else{
            $tax1 = $tax1V;
        }

        if ($tax2_type == '2'){
            $tax2 = $price*($tax2V/100);
        }else{
            $tax2 = $tax2V;
        } 

        $total = $price+$tax1+$tax2-$dis;

        $paid = $request->paid;
        // if($paid > $total){
        //     $msg=$lang == 'ar' ?"($total) المدفوع اكبر من اجمالى الفاتورة" : "Cash is greater than bill cost ($total)";
        //     return $this->apiResponseMessage(0,$msg,200);
        // }

        $rest = $total-$paid;

        $order = P_d_orders::find($p_d_order_id);

        //handle paid, rest, user_debt and representative_debt

        $order->total_price = $total;
        $order->paid = $paid;
        $order->rest = $rest;
        $order->dis_type = $discount_type;
        $order->discount = $disV;
        $order->tax1_type = $tax1_type;
        $order->tax1 = $tax1V;
        $order->tax2_type = $tax2_type;
        $order->tax2 = $tax2V;
        $order->save();

        $user = User::find($order->user_id);
        $representative = User::find($order->representative_id);

        $user_new_debt = ($user->debt)+$rest;
        $representative_new_debt = ($representative->debt)+$paid;
        
        $user->debt =  $user_new_debt;
        $user->save();

        $representative->debt = $representative_new_debt;
        $representative->save();

        $safe_detail = new \stdClass();
        $safe_detail->safe_id = $representative->safe_id;
        $safe_detail->process_name_ar = 'مبيعات';
        $safe_detail->process_name_en = 'Sales';
        $safe_detail->process_type = 1;
        $safe_detail->value = $paid;
        SafeController::add_safe_process($safe_detail);

        $msg=$lang=='ar' ? 'تم إضافة الفاتورة بنجاح'  : 'Bill added successfully';
        return $this->apiResponseData(new Orders_MResource($order),$msg);

    }


    public function all_p_d_Order_with_products (Request $request){
        $P_d_orders=Order_product_details::orderBy('created_at','desc')->get();
        return $this->apiResponseData(Order_product_detailsResource::collection($P_d_orders),'success');
    }

    public function single_P_d_order_with_products(Request $request,$P_d_order_id)
    {
        $lang=$request->header('lang');
        $P_d_order=Order_product_details::where('P_d_order_id', '=' ,$P_d_order_id)->first();
        $check=$this->not_found($P_d_order,'الطلب','P_d_order',$lang);
        if(isset($check)){
            return $check;
        }
        $query = Order_product_details::select()->where('P_d_order_id', $P_d_order_id)->get();
        return $this->apiResponseData(Order_product_detailsResource::collection($query),'success');
    }

    /*
     * @pram $request
     * @return Error message or check if user is null
    */

    private function validate_P_d_order($request){
        $lang = $request->header('lang');
        $input = $request->all();
        $validationMessages = [
            'user_id.required' => $lang == 'ar' ?  'من فضلك ادخل رقم العميل' :"client is required" ,
            'address_id.required' => $lang == 'ar' ? 'من فضلك ادخل العنوان ' :"address is required",
        ];

        $validator = Validator::make($input, [
            'user_id' => 'required',
            'address_id' => 'required',
        ], $validationMessages);

        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 400);
        }

        $user=User::find($request->user_id);
        $check=$this->not_found($user,'العميل','Client',$lang);
        if(isset($check)){
            return $check;
        }
    }
    public function most_sell(Request $request)
    {
        $most_sell = DB::table('order_product_details')->select(DB::raw('COUNT(product_detail_id) as count'),'product_detail_id')
        ->groupBy('product_detail_id')
        ->orderBy('count','desc')
        ->take(5)
        ->get(); 

        $mosts_ids = $most_sell->pluck('product_detail_id');

        $products = Product_details::whereIn('id', $mosts_ids)->get();
        
        return  $this->apiResponseData(MostsellResource::collection($products),'success');

    }


    public function order_user_addresses (Request $request , $user_id){
        $lang=$request->header('lang');
        $user=User::where('id',$user_id)->first();
        $msg=$lang=='ar' ? 'تمت العملية بنجاح'  : 'success';
        return $this->apiResponseData(locationResource::collection($user->my_address),$msg);
    }

    public function add_user_address(Request $request , $user_id)
    {
        $lang=$request->header('lang');
        $address=new order_locations();
        $address->lat=$request->lat;
        $address->lng=$request->lng;
        $address->address=$request->address;
        $address->user_id=$user_id;
        $address->save();
        $msg=$lang=='ar' ? 'تم تسجيل العنوان بنجاح' : 'location saved successfully';
        return $this->apiResponseData(new locationResource($address),$msg,200);
    }
 
    public function representative_orders(Request $request)
    {
        $lang=$request->header('lang');
        $representative=Auth::user();
        
        $P_d_order=P_d_orders::where('representative_id', '=' ,$representative->id)->orderBy('created_at' , 'DESC')->get();
    
        return $this->apiResponseData(Orders_MResource::collection($P_d_order),'success');
    }

    public function user_orders(Request $request)
    {
        $lang=$request->header('lang');
        $user=Auth::user();

        $P_d_order=P_d_orders::where('user_id', '=' ,$user->id)->orderBy('created_at' , 'DESC')->get();
    
        return $this->apiResponseData(Orders_MResource::collection($P_d_order),'success');
    }

    public function order_return(Request $request , $order_id)
    {
        
        $lang=$request->header('lang');
        $representative=Auth::user();

        $order = P_d_orders::find($order_id);
        $check=$this->not_found($order,'الطلب','P_d_order',$lang);
        if(isset($check)){
            return $check;
        }

        $order_pieces = Order_product_details::select('quantity')->where('p_d_order_id',$order_id)->get();
        $qty_suuum = 0;
        foreach ($order_pieces as $row){
            $qty_suuum += $row->quantity;
        }

        $total_price = $order->total_price;
        $pre_price = $order->pre_price;
        $old_rest = $order->rest;
        $old_paid = $order->paid;

        $diff = $total_price - $pre_price;
        $suuum = 0;
        $pre_sum = 0;
        
        $order_req = $request->all();
        foreach($order_req['orders'] as $orders)
        {
            $pro_id = $orders['product_detail_id'];

            $product_detail=Order_product_details::where('product_detail_id',$orders['product_detail_id'] )->where('p_d_order_id', $order_id)->first();
            $check=$this->not_found($product_detail,'المنتج','Product_detail',$lang);
            if(isset($check)){
                $ch = $orders['product_detail_id'];
                $msg=$lang == 'ar' ?"المنتج رقم $ch غير موجود":"Product number $ch not found";
                return $this->apiResponseMessage(1,$msg,200);
            }
            
            $return_quntuty = $orders['ret_quantity'];
            $old_quantity = $product_detail->quantity;
            if($return_quntuty > $old_quantity){
                $msg=$lang == 'ar' ?"الكمية بالمنتج $pro_id أقل من المطلوبة" : "Quantity in product $pro_id less than demanded";
                return $this->apiResponseMessage(0,$msg,200);
            }

            $price = $product_detail->price;

            $diff_percentage = $price*$diff/$pre_price;

            $retsum = ($price + $diff_percentage) * $return_quntuty;

            $new_quntity = $old_quantity - $return_quntuty;
            
            if($new_quntity == 0){
                $product_detail->delete();
            }else{
                $product_detail->quantity=$new_quntity;
                $product_detail->save();
            }

            $return_order=new Order_return;
            $return_order->ret_quantity= $return_quntuty;
            $return_order->ret_sum= $retsum;
            $return_order->representative_id = $representative->id;
            $return_order->user_id=$order->user_id;
            $return_order->product_detail_id = $pro_id;
            $return_order->p_d_order_id=$order_id;
            $return_order->save();

            $qty_handling = Product_detail_inventory::where('product_detail_id',$pro_id)->where('inventory_id',$product_detail->inventory_id)->first();
            $qty_handling->quantity += $return_quntuty;
            $qty_handling->save();

            $suuum += $return_order->ret_sum;

            $soft_sum = $price * $return_quntuty;
            $pre_sum += $soft_sum;

            $retData[] = $return_order;
        }
        
        $new_total_sum = $order->total_price - $suuum;

        if($suuum <= $old_rest){
            $new_rest = $old_rest - $suuum;
            $ret_cash = 0;
        }else{
            $new_rest = 0;
            $ret_cash = $suuum - $old_rest;
        }

        $order->total_price = $new_total_sum;
        $order->pre_price = $pre_price - $pre_sum;
        $order->rest = $new_rest;
        $order->paid = $old_paid-$ret_cash;
        $order->save();

        $user = User::find($order->user_id);
        $userDebt = $user->debt;
        $user->debt = $userDebt - $old_rest + $new_rest;
        $user->save();

        if($ret_cash != 0){
            $safe_detail = new \stdClass();
            if(isset($order_req['safe_id'])){
                $safe = Safe::find($order_req['safe_id']);
                $check=$this->not_found($user_d,'الخزنة','Safe',200);
                if(isset($check))
                {
                    return $check;
                }
                $safe_detail->safe_id = $order_req['safe_id'];
            }else{
                $safe_detail->safe_id = $order->safe_id;
            }
            $safe_detail->process_name_ar = 'مرتجعات';
            $safe_detail->process_name_en = 'Returns';
            $safe_detail->process_type = 2;
            $safe_detail->value = $ret_cash;
            $safe_detail->notes = "مرتجع طلب رقم $order_id";
            SafeController::add_safe_process($safe_detail);
        }

        $data = ['Order total price' => $order->total_price , 'Return Cost' => (double)$suuum ,'Return data' => $retData , 'User debt' => $user->debt];

        return  $this->apiResponseData($data,'success');
    }

    public function debt_set(Request $request,$user_id)
    {
        $representative = Auth::user();
        
        $user = User::find($user_id);
        $check=$this->not_found($user,'العضو','user',200);
        if(isset($check))
        {
            return $check;
        }

        if($representative->safe_id == null){
            $safe = new Safe;
            $safe->name_ar = "خزنة $representative->first_name $representative->last_name";
            $safe->name_en = "$representative->first_name $representative->last_name Safe";
            $safe->status = 1;
            $safe->total_money = $representative->debt;
            $safe->save();

            $representative->safe_id = $safe->id;
            $representative->save();
        }

        if($request->type == '2'){
            $user_debt = ($user->debt)-($request->paid);
            $representative_debt = ($representative->debt)+($request->paid);

            $safe_detail = new \stdClass();
            $safe_detail->safe_id = $representative->safe_id;
            $safe_detail->process_name_ar = 'تحصيل المندوب من مديونية عميل ';
            $safe_detail->process_name_en = 'User Debt Setting by Representative';
            $safe_detail->process_type = 1;
            $safe_detail->value = $request->paid;
            SafeController::add_safe_process($safe_detail);

        }else{
            $user_debt = ($user->debt)+($request->paid);
            $representative_debt = ($representative->debt)-($request->paid);

            $safe_detail = new \stdClass();
            $safe_detail->safe_id = $representative->safe_id;
            $safe_detail->process_name_ar = 'اضافة المندوب علي مديونية عميل ';
            $safe_detail->process_name_en = 'User Debt adding by Representative';
            $safe_detail->process_type = 2;
            $safe_detail->value = $request->paid;
            SafeController::add_safe_process($safe_detail);
        }
        
        $user->debt = $user_debt;
        $user->save();

        $representative->debt = $representative_debt;
        $representative->save();
        
        $log = new Representative_debt_log;
        $log->representative_id = $representative->id;
        $log->user_id = $user_id;
        $log->type = $request->type == 2 ? 'subtraction' : 'addition'; 
        $log->value = $request->paid;
        $log->save();

        return $this->apiResponseData(new Representative_debt_logResource($log),'success');

    }
 
    public function users_of_representative(Request $request)
    {
        $representative = Auth::user();
        $user_representative=Representative_User::where('representative_id',$representative->id)->pluck('user_id');
        $users = User::whereIn('id' , $user_representative)->get();
        return $this->apiResponseData(UserAddressResource::collection($users),'success');
    }

    public function add_user_representative(Request $request)
    {
        $lang = $request->header('lang');
        $representative = Auth::user();

        $user = $request->user_id;
        $user_d = User::where('id' , $user)->where('status' ,'!=', '2')->first();
        $check=$this->not_found($user_d,'العضو','user',200);
        if(isset($check))
        {
            return $check;
        }

        if(Representative_User::where('user_id',$user)->where('representative_id',$representative->id)->exists()){
            $msg = $lang=='ar' ? 'هذاالعميل مضاف لقائمة عملاء المندوب من قبل'  : 'This user is already exists in representative users list';
            return $this->apiResponseMessage(1,$msg,200);
        }elseif(Representative_User::where('user_id',$user)->exists()){
            $msg = $lang=='ar' ? 'هذاالعميل مضاف لقائمة عملاء مندوب اخر'  : 'This user is already exists in another representative users list';
            return $this->apiResponseMessage(1,$msg,200);
        }else{
            $user_representative = new Representative_User();
            $user_representative->representative_id = $representative->id;
            $user_representative->user_id = $user;
            $user_representative->save();

            $msg = $lang=='ar' ? 'تم اضافةالعميل للمندوب بنجاح'  : 'user_representative added successfully';
            return $this->apiResponseData(new Representative_UserResource($user_representative),$msg);
        }
    }

    public function delete_user_representative(Request $request,$user_id)
    {
        $lang=$request->header('lang');
        $representative = Auth::user();

        $user_representative = Representative_User::where('user_id',$user_id)->where('representative_id',$representative->id)->first();

        $check=$this->not_found($user_representative,'عميل_مندوب','user_representative',$lang);
        if(isset($check))
        {
            return $check;
        }

        $user_representative->delete();

        $msg=$lang=='ar' ? 'تم حذف عميل_مندوب'  : 'user_representative Deleted successfully';

        return $this->apiResponseMessage(1,$msg,200);
    }


    private function validate_user_representative($request)
    {
        $lang = $request->header('lang');
        $input = $request->all();
        $validationMessages = [
            'user_id.required' => $lang == 'ar' ? 'من فضلك ادخل كود العميل' :"User_id is required"  ,
        ];
        $validator = Validator::make($input, [
            'user_id' => 'required',
        ], $validationMessages);
        if ($validator->fails())
        {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 400);
        }

    }

    ///////////////Edit_Order//////////////////////////////////////////////////////////

    public function edit_p_d_order(Request $request , $order_id)
    {
        $lang=$request->header('lang');

        $order_req = $request->all();

        // $validate_P_d_order=$this->validate_P_d_order($request);
        // if(isset($validate_P_d_order)){
        //     return $validate_P_d_order;
        // }

        $user = $order_req['user_id'];
        $user_d = User::find($user);
        $representative_d = Auth::user();

        $user_status = $user_d->status;
        if($user_status == '4'){
            $price_type = 'wholesale_wholesale_price';
        }elseif($user_status == '3'){
            $price_type = 'wholesale_price';
        }elseif($user_status == '1'){
            $price_type = 'selling_price';
        }

        $P_d_order = P_d_orders::where('id',$order_id)->whereIn('status',[1,2])->first();
        $check=$this->not_found($P_d_order,'الطلب','P_d_order',$lang);
        if(isset($check)){
            $msg=$lang == 'ar' ?'الطلب غير موجود او تم شحنه او تسليمه' : 'Order not found or shipped or delivered';
                return $this->apiResponseMessage(0,$msg,200);
        }


        $P_d_order->user_id = $user;
        $P_d_order->payment_method=$order_req['payment_method'];
        if(isset($order_req['address_id'])){
            $P_d_order->address_id=$order_req['address_id'];
        }
        // $P_d_order->address_id=$order_req['address_id'];
        $P_d_order->price_type=$price_type;
        if(isset($order_req['notes'])){
            $P_d_order->notes = $order_req['notes'];
        }else{
            $P_d_order->notes = null;
        }
        $P_d_order->save();
        $this->addingProducts($order_req['products'],$order_id,$representative_d,$lang);
        $this->order_Bill($order_req,$order_id,$representative_d,$lang);

        $order_data = P_d_orders::find($order_id);

        $msg=$lang=='ar' ?  'تم تعديل الطلب بنجاح' :'Order edited successfully' ;
        return $this->apiResponseData(new Orders_MResource($order_data),$msg);
    }
    

        
    private function addingProducts($products,$p_d_order_id,$representative,$lang)
    {
        $old_products = Order_product_details::where('p_d_order_id',$p_d_order_id)->get();
        if(isset($old_products)){
            foreach($old_products as $row){
            
                $return_quntuty = $row->quantity;

                $qty_handling = Product_detail_inventory::where('product_detail_id',$row->product_detail_id)->where('inventory_id',$row->inventory_id)->first();
                $qty_handling->quantity += $return_quntuty;
                $qty_handling->save();
            
                $row->delete();
            }
        }

        $P_d_order=P_d_orders::find($p_d_order_id);

        $rep_inv_id = Rep_Inv::where('representative_id' , $representative->id)->value('inventory_id');

        foreach($products as $orders)
        {
            $product_detail=Product_details::find($orders['id']);
            $product_detail_price=Product_details_price::where('product_detail_id',$orders['id'])->first();
            $p_d_qty = Product_detail_inventory::where('product_detail_id',$orders['id'])->where('inventory_id',$rep_inv_id)->first();
            
            $check=$this->not_found($p_d_qty,'المنتج','Product_detail',$lang);
            if(isset($check)){
                $ch = $orders['id'];
                $msg=$lang == 'ar' ?"المنتج رقم $ch غير موجود بالمخزن":"Product number $ch not found in inventory";
                return $this->apiResponseMessage(1,$msg,200);
            }

            if($orders['color_id']){
                $color=color::find($orders['color_id']);
                $check=$this->not_found($color,'اللون','Color',$lang);
                if(isset($check)){
                    $ch = $orders['color_id'];
                    $msg=$lang == 'ar' ?"اللون رقم $ch غير موجود":"Color number $ch not found";
                    return $this->apiResponseMessage(1,$msg,200);
                }
            }

            if($orders['size_id']){
                $size=Sizes::find($orders['size_id']);
                $check=$this->not_found($size,'الحجم','Size',$lang);
                if(isset($check)){
                    $ch = $orders['size_id'];
                    $msg=$lang == 'ar' ?"الحجم رقم $ch غير موجود":"Size number $ch not found";
                    return $this->apiResponseMessage(1,$msg,200);
                }
            }

            $P_d_order_product=Order_product_details::where('P_d_order_id',$p_d_order_id)->where('product_detail_id',$orders['id'])->first();
            if(!is_null($P_d_order_product)){
                $ch = $orders['id'];
                $msg=$lang == 'ar' ?"المنتج $ch موجود في هذا الطلب" : "Product $ch already exist in this P_d_order";
                return $this->apiResponseMessage(0,$msg,200);
            }

            // $p_d_qty = Product_detail_inventory::where('product_detail_id',$orders['id'])->first();
            $qty = $p_d_qty->quantity;
            if ($orders['quantity'] > $qty){
                $ch = $orders['id'];
                $msg=$lang == 'ar' ?"كمية المنتج $ch بالمخزن أقل من المطلوبة" : "Quantity of product $ch in inventory less than demanded";
                return $this->apiResponseMessage(0,$msg,200);
            }
            $n_qty = $qty-$orders['quantity'];
            // return $n_qty;
            
            $p_d_qty->quantity = $n_qty;
            $p_d_qty->save();

            $price_type = $P_d_order->price_type;
            $price = Price::select($price_type)->where('id',$product_detail_price->price_id)->first();

            if($orders['quantity'] == 0){}else{
                $order_products = new Order_product_details;
                $order_products->P_d_order_id = $p_d_order_id;
                $order_products->product_detail_id = $orders['id'];
                $order_products->inventory_id = $rep_inv_id;
                $order_products->quantity = $orders['quantity'];
                $order_products->price = $price->$price_type;
                $order_products->color_id = $orders['color_id'];
                $order_products->size_id = $orders['size_id'];
                $order_products->save();
            }
        }
    }

    private function order_Bill($order_req,$p_d_order_id,$representative_d,$lang)
    {
        $order = P_d_orders::find($p_d_order_id);

        $old_rest = $order->rest;
        $old_paid = $order->paid;

        $shipp = $order->shipping_price;
        
        $pro_qtys = Order_product_details::select()->where('p_d_order_id',$p_d_order_id)->get();
        $price = 0;
        foreach ($pro_qtys as $row) {
            $pro_price = ($row->quantity)*($row->price);
            $price += $pro_price;
        }

        $discount_type = $order_req['discount_type'];
        $disV = $order_req['discount'];
        $tax1_type = $order_req['tax1_type'];
        $tax1V = $order_req['tax1'];
        $tax2_type = $order_req['tax2_type'];
        $tax2V = $order_req['tax2'];

        if ($discount_type == '2'){
            $dis = $price*($disV/100);
        }else{
            $dis = $disV;
        }

        if ($tax1_type == '2'){
            $tax1 = $price*($tax1V/100);
        }else{
            $tax1 = $tax1V;
        }

        if ($tax2_type == '2'){
            $tax2 = $price*($tax2V/100);
        }else{
            $tax2 = $tax2V;
        }

        $total = $price+$shipp+$tax1+$tax2-$dis;

        $paid = $order_req['paid'];
        // if($paid > $total){
        //     $msg=$lang == 'ar' ?"($total) المدفوع اكبر من اجمالى الفاتورة" : "Cash is greater than bill cost ($total)";
        //     return $this->apiResponseMessage(0,$msg,200);
        // }

        $rest = $total-$paid;

        //handle paid, rest, user_debt and representative_debt

        $order->pre_price = $price;
        $order->total_price = $total;
        $order->paid = $paid;
        $order->rest = $rest;
        $order->dis_type = $discount_type;
        $order->discount = $disV;
        $order->tax1_type = $tax1_type;
        $order->tax1 = $tax1V;
        $order->tax2_type = $tax2_type;
        $order->tax2 = $tax2V;
        $order->save();

        $user_data = User::find($order->user_id);
        $representative_data = User::find($representative_d->id);
        
        $user_new_debt = ($user_data->debt)-$old_rest+$rest;
        $representative_new_debt = ($representative_data->debt)-$old_paid+$paid;
        
        $user_data->debt = $user_new_debt;
        $user_data->save();

        $representative_d->debt = $representative_new_debt;
        $representative_d->save();

    }


    public function html_to_image(Request $request){

        $link = $request->link;

        $image = mt_rand(). time().'.'.$link.'.png';

        $conv = new \Anam\PhantomMagick\Converter();
        $conv->source($link)
            ->toPng()
            ->save(public_path("public/images/Order/$image"));

        $image_link = BaseController::getImageUrl('Order', $image);
        return $image_link;
        // return $this->apiResponseData($image_link,'success');
    }
}
