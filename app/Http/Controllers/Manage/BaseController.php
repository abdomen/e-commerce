<?php

namespace App\Http\Controllers\Manage;

use App\Models\Product;
use App\Models\Product_Currency;
use App\Models\Products;
use App\Models\Product_details;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth,File;
use App\Models\Produc_view_user;

class BaseController extends Controller
{
    //site url
    public  static function get_url(){
        return 'http://testapi.qimmajomla.com';
        // return 'https://qimmajoomlaapi.codecaique.com';
    }

    public  static function getImageUrl($folder,$image){
        if($image){return BaseController::get_url() . '/public/images/'.$folder .'/'.$image;
        }else{return BaseController::get_url() . '/public/images/1.jfif';} 
    }


    public static function saveImage($folder,$file)
    {
        $image = $file;
        $input['image'] = mt_rand(). time().'.'.$image->getClientOriginalExtension();
        $dist = public_path('public/images/'.$folder.'/');
        $image->move($dist, $input['image']);
        return $input['image'];

    }
    /*
     * TO Delete File From server storage
     */
    public static function deleteFile($folder,$file)
    {
        $file = public_path('public/images/'.$folder.'/'.$file);
        if(file_exists($file))
        {
            File::delete($file);
        }
    }

    /*
     * to save new veiw product
     */
    public static function product_view($product_id,$user_id)
    {
        $Produc_view_user=Produc_view_user::where('product_detail_id',$product_id)->where('user_id',$user_id)->first();
        if(is_null($Produc_view_user)){
            $Produc_view_user=new Produc_view_user;
            $Produc_view_user->user_id=$user_id;
            $Produc_view_user->product_detail_id=$product_id;
            $Produc_view_user->count=1;
            $Produc_view_user->save();
        }else {
            $Produc_view_user->count += 1;
            $Produc_view_user->save();
        }
    }


    /*
     *@var array of my cart product
     * return total price
     */

    public  static function get_total_price($product,$user)
    {
        $price = 0;
        foreach ($product as $row) {
            $price_producct=BaseController::get_price($user,$row);
            $price += $price_producct * $row->pivot->quantity;
        }
        return (double)$price ;
    }

    /*
       * get currency & price depended on currency_id of custom user
       * get general currency & price
     */
    public static function get_price($user,$product)
    {
        $price=$product->is_offer==1 ?$product->price_product[0]->pivot->price_after_offer : $product->price_product[0]->pivot->price;
        return $price;
//            $currency_id = $user->currency_id;
//            if ($currency_id) {
//                $has_price = $product->price_product->contains($currency_id);
//                if ($has_price) {
//                    $price = $product->price_product->where('id', $currency_id)->first();
//                    $price=$product->is_offer==0 ? (double)$price->pivot->price : (double)$price->pivot->price_after_offer;
//                    return  (double)$price;
//                }
//                return 0;
//            }
//
//        $price= $product->price_product->where('defualt',1)->first();
//        if(is_null($price)){
//            return 0;
//        }else{
//            $price=$product->is_offer==0 ? (double)$price->pivot->price : (double)$price->pivot->price_after_offer;
//            return  (double)$price;
//        }
    }


    /*
     * calculate rate of product
     */
    public static function get_rate($id){

            $rateModel=Product_details::find($id);


        $allCount=$rateModel->user_rate()->count() * 5;

        $rate=0;
        foreach($rateModel->user_rate as $row)
        {
            //return $row;
            $rate+=$row->pivot->rate;

        }
        if($rate > 0){
            $rate_new= $rate * 5 / $allCount;
        }else{
            $rate_new=0;
        }

        return $rate_new;
    }

    /*
     * calculate offer of cutom product
     * @pram product
     */

    public static function set_offer($product_id)
    {
        $product=Product_details::find($product_id);
        $Product_Currency=Product_Currency::where('product_detail_id',$product_id)->get();
        if($product->is_offer == 1)
        {
            foreach($Product_Currency as $row)
            {
                $row->price_after_offer=$row->price - ($row->price * $product->offer_amount /100);
                $row->save();
            }
        }else{
            foreach($Product_Currency as $row)
            {
                $row->price_after_offer=0;
                $row->save();
            }
            $product->offer_amount=0;
            $product->save();
        }
    }

}
