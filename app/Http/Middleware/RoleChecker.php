<?php

namespace App\Http\Middleware;

use Closure,Auth;
use Illuminate\Http\RedirectResponse;

class RoleChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $Shops, $Admins)
    {
        $roles = Auth::check() ? Auth::user()->roles->pluck('name_en')->toArray() : [];
    
        if (in_array($Shops, $roles)) {
            return $next($request);
        } else if (in_array($Admins, $roles)) {
            return $next($request);
        }

        return response()->json([
            'status' =>  0,
            'message' => 'Not allowed for you',
            'data'=> null
        ]);
    
        // return redirect()->route('home');
    }
}
