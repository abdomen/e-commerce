<?php

namespace App\Http\Resources\Shop;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers\Manage\BaseController;
use DB;
use App\Http\Resources\ColorResource;
use App\Http\Resources\ImagesResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'name_ar' =>  $this->name_ar ,
            'name_en' =>  $this->name_en ,
            'desc_ar' =>  $this->desc_ar ,
            'desc_en' =>  $this->desc_en ,
            'image' => BaseController::getImageUrl('Products',$this->image),
            'rate' => $this->rate,
            'status' => (int)$this->status,
            'quantity'=>(int)$this->quantity,
            'is_offer'=>$this->is_offer ? $this->is_offer : 0,
            'offer_amount'=>(int)$this->offer_amount,
            'lat'=>(double)$this->lat,
            'lng'=>(double)$this->lng,
            'price_product' => PriceResource::collection($this->price_product),
        ];
    }
}
