<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers\Manage\BaseController;

class JopResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');
        $title=$this->title_en;
        $desc=$this->desc_en;
        if($lang=='ar'){
            $title=$this->title_ar;
            $desc=$this->desc_ar;
        }elseif($lang=='ca') {
            $title = $this->title_ca;
            $desc = $this->desc_ca;
        }

        return [
            'id' => $this->id,
            'title' => $title,
            'desc' => $desc,
            'icon' => BaseController::getImageUrl('Jop',$this->icon),
            'count' => $this->num,

        ];
    }
}
