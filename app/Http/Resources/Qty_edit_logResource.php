<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Product_details;
use App\Models\Shop;
use App\Models\Inventory;

class Qty_edit_logResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');

        $time = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d/m/Y').' '.\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('H:i:s');

        if(Shop::find($this->admin_id) != null){
            $admin = Shop::find($this->admin_id);
        }else{
            $admin = new \stdClass();
            $admin->name = null;
        }

        if(Product_details::find($this->product_detail_id) != null){
            $pro = Product_details::find($this->product_detail_id);
        }else{
            $pro = new \stdClass();
            $pro->Main_Product['name_ar'] = null;
            $pro->Main_Product['name_en'] = null;
            $pro->difference_ar = null;
            $pro->difference_en = null;
        }

        if(Inventory::find($this->inventory_id) != null){
            $inv = Inventory::find($this->inventory_id);
        }else{
            $inv = new \stdClass();
            $inv->name_ar = null;
            $inv->name_en = null;
        }
        
        return [
            'id' => $this->id,
            'product_id' => $this->product_detail_id,
            'product_name' => $lang == 'ar' ? (string)$pro->Main_Product['name_ar'] : (string)$pro->Main_Product['name_en'],
            'difference' => $lang == 'ar' ? (string)$pro->difference_ar : (string)$pro->difference_en,
            'inventory_name' => $lang == 'ar' ? $inv->name_ar : $inv->name_en,
            'admin_name'=> $admin->name,
            'old_qty'=> $this->old_qty,
            'new_qty'=> $this->new_qty,
            'date' => $time,

        ];
    }
}
