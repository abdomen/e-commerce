<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SafeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');

        if($this->status == 1){
            $status = $lang == 'ar' ? 'متاحه' : 'available';
        }else{
            $status = $lang == 'ar' ? 'غير متاحه' : 'unavailable';
        }

        return [
            'id' => $this->id,
            'name' => $lang == 'ar' ? $this->name_ar : $this->name_en,
            'status'=> $status,
            'total_money' => $this->total_money,
        ];
    }
}
