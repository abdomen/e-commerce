<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Shop\UserResource;

class Order_returnResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'p_d_order_id' => $this->p_d_order_id,
            'product_detail_id'=>$this->product_detail_id,
            'returned_quantity'=>(int)$this->ret_quantity,
            'returned_sum'=>(double)$this->ret_sum,
            'representative_id'=>$this->representative_id,
            'user_id'=>$this->user_id,
            'order'=>new P_d_ordersResource($this->Order),
            // 'products'=>new Product_detailsResource($this->Product_detail),
            // 'representative'=>new RepShowResource($this->representative),
            // 'user'=>new UserResource($this->user),
        ];
    }
}
