<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Order_product_detailsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            // 'Order'=>new P_d_ordersResource($this->Order),
            'Product_detail'=>new Product_detailsResource($this->Product_detail),
            'p_d_order_id' => $this->p_d_order_id,
            'product_detail_id'=>$this->product_detail_id,
            'inventory_id'=>$this->inventory_id,
            'quantity'=>(int)$this->quantity,
            'color_id'=>$this->color_id,
            'size_id'=>$this->size_id,
            'color'=>new ColorResource($this->Color),
            'size'=>new SizeResource($this->Size),
            'price'=>$this->price,
        ];
    }
}
