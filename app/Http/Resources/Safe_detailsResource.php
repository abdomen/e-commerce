<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Models\Safe;

class Safe_detailsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');

        if($this->process_type == 'addition'){
            $processType = $lang == 'ar' ? 'اضافه' : $this->process_type;
        }elseif($this->process_type == 'subtraction'){
            $processType = $lang == 'ar' ? 'خصم' : $this->process_type;
        }

        if(Safe::find($this->safe_id) != null){
            $safe = Safe::find($this->safe_id);
        }else{
            $safe = new \stdClass();
            $safe->id = null;
            $safe->name_ar = null;
            $safe->name_en = null;
            $safe->status = null;
            $safe->total_money = null;
        }

        $time = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d/m/Y').' '.\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('H:i:s');

        return [
            'id' => $this->id,
            'process_name' => $lang == 'ar' ? $this->process_name_ar : $this->process_name_en,
            'process_type' => $processType,
            'money' => $this->value,
            'safe' => new SafeResource($safe),
            'date' => $time,
            'notes' => $this->notes,
        ];
    }
}
