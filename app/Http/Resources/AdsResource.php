<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers\Manage\BaseController;

class AdsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'image' => BaseController::getImageUrl('Ads',$this->image),
            'title_ar'=>$this->title_ar,
            'title_en'=>$this->title_ar,
            'sub_title_ar'=>$this->sub_title_ar,
            'sub_title_en'=>$this->sub_title_en,
            'btn_text_ar'=>$this->btn_text_ar,
            'btn_text_en'=>$this->btn_text_en,
            'btn_link'=>$this->btn_link,
            'place_type'=>$this->place_type
        ];
    }
}
