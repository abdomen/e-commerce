<?php

namespace App\Http\Resources\GeneralDash;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers\Manage\BaseController;

class MainShopResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');

        $address=$lang=='ar' ? $this->address_ar : $this->address_en;
        $about=$lang=='ar' ? $this->about_ar : $this->about_en;
        
        return [
            'id' => $this->id,
            'name' => $this->name,
            'active' => (boolean)$this->active,
            'email' => $this->email,
            'phone' => $this->phone,
            'logo' => BaseController::getImageUrl('MainShop',$this->logo),
            'address' => $address,
            'about' => $about,
        ];
    }
}
