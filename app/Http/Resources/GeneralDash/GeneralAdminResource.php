<?php

namespace App\Http\Resources\GeneralDash;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers\Manage\BaseController;

class GeneralAdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $arryOfRole=$this->getRoleIds();
        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'jop' => $this->jop,
            'token'=>$this->token,
            'image' => BaseController::getImageUrl('GeneralAdmin',$this->image),
            'role_1'=>in_array(1,$arryOfRole) ? true : false,
            'role_2'=>in_array(2,$arryOfRole) ? true : false,
            'roles'=>$this->roles,
        ];
    }
}
