<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Product_detail_inventoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');
        if ($this->product_detail != null){
            $products = new Product_detailsResource($this->Product_detail);
        }else{
            $products = null;
        }
        return $products;
    }
}
