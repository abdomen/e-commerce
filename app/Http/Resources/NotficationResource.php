<?php



namespace App\Http\Resources;



use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Controllers\Manage\BaseController;



class NotficationResource extends JsonResource

{

    /**

     * Transform the resource into an array.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return array

     */

    public function toArray($request)

    {


        $lang = $request->header('lang');


        return [

            'id' => $this->id,
            'name' =>  $this->name,
            'desc' =>  $this->desc,
            'click_action'=>$this->click_action,
            'redirect_id'=>$this->redirect_id,
            'status'=>$this->status,
            'image' => BaseController::getImageUrl('Notfication',$this->icon),

        ];

    }

}

