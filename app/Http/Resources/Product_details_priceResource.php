<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Product_details_priceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');
        return [
            'id' => $this->id,
            'product_detail_id' => $this->product_detail_id,
            'Product_detail' => $this->Product_detail,
            'price_id' => $this->price_id,
            'Price' => $this->Price,
        ];
    }
}
