<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers\Manage\BaseController;


class ShopResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');

        $address=$lang=='ar' ? $this->address_ar : $this->address_en;
        $about=$lang=='ar' ? $this->about_ar : $this->about_en;
        
        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'address' => $address,
            'about' => $about,
            'facebook' => $this->facebook,
            'snap' => $this->snap,
            'twitter' => $this->twitter,
            'instagram' => $this->instagram,
            'logo' => BaseController::getImageUrl('Shop',$this->logo),
        ];
    }
}
