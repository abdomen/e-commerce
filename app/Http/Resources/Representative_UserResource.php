<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Representative_UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'representative_id' => $this->representative_id,
            'user_id' => $this->user_id,
            'representative'=>$this->representative,
            'user'=>$this->user,
        ];
    }
}
