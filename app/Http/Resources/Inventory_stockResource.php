<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Models\Shop;
use App\Models\Product_details;
use App\Models\Inventory;

class Inventory_stockResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');

        if(Product_details::find($this->product_detail_id) != null){
            $pro = Product_details::find($this->product_detail_id);
        }else{
            $pro = new \stdClass();
            $pro->Main_Product['name_ar'] = null;
            $pro->Main_Product['name_en'] = null;
            $pro->difference_ar = null;
            $pro->difference_en = null;
        }

        if(Shop::find($this->user_id) != null){
            $admin = Shop::find($this->user_id);
        }else{
            $admin = new \stdClass();
            $admin->name = null;
        }
        if(Inventory::find($this->inventory->id) != null){
            $inv = Inventory::find($this->inventory->id);
        }else{
            $inv = new \stdClass();
            $inv->name_ar = null;
            $inv->name_en = null;
        }
        
        return [
            'id' => $this->id, 
            'inventory_name' => $lang == 'ar' ? $inv->name_ar : $inv->name_en,
            'admin_name' => $admin->name,
            'product_name' => $lang == 'ar' ? (string)$pro->Main_Product['name_ar'] : (string)$pro->Main_Product['name_en'],
            'difference' => $lang == 'ar' ? (string)$pro->difference_ar : (string)$pro->difference_en,
            'system_amount' => $this->system_amount,
            'real_amount' => (int)$this->real_amount,
            'diff_amount' => $this->diff_amount,
            'cost' => $this->total_price,
            'date' => \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d/m/Y').' '.\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('H:i:s'),
            'user_id' => $this->user->id,
            'inventory_id' => $this->inventory_id,
            'product_detail_id' => (int)$this->product_detail_id,
        ];    
    }
}
