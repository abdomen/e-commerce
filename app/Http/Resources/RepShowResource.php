<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Rep_Inv;
use App\Models\Inventory;
use App\Http\Controllers\Manage\BaseController;


class RepShowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');
        
        $carName = Rep_Inv::where('representative_id',$this->id)->value('car_num');
        if(isset($this->Rep_Inv[0])){
            $inv = Inventory::find($this->Rep_Inv[0]->inventory_id);
            $inventory = new InventoryResource($inv);
        }else{
            $inventory = new \stdClass();
            $inventory->id = null;
            $inventory->name = null;
            $inventory->address = null;
            $inventory->phone = null;
            $inventory->lng = null;
            $inventory->lat = null;
            $inventory->products = [];
        }
        
            return [
                'id' => $this->id,
                'first_name' => $this->first_name,
                'last_name' => $this->last_name,
                'image' => BaseController::getImageUrl('users',$this->image),
                'fire_base_token'=>$this->fire_base_token,
                'phone' => $this->phone,
                'email' => $this->email,
                'status' => (int)$this->status,
                'social'=>(int)$this->social,
                'notification'=>$this->notification ? 1 : 0,
                'message'=>$this->message ? 1 : 0,
                'lang'=>$lang,
                'lat' => $this->lat,
                'lng' => $this->lng,
                'debt' => (double)$this->debt,
                'currency_id' => (int)$this->currency_id,
                'car_num' => $carName,
                'inventory' => $inventory,
                // 'inv' => $this->Rep_Inv,
            ];
    }
}
