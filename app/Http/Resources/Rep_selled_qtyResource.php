<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Product_details;

class Rep_selled_qtyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');
        
        if(Product_details::find($this->product_detail_id) != null){
            $pro = Product_details::find($this->product_detail_id);
        }else{
            $pro = new \stdClass();
            $pro->Main_Product['name_ar'] = null;
            $pro->Main_Product['name_en'] = null;
            $pro->difference_ar = null;
            $pro->difference_en = null;
        }
            
        return [
            'id' => $this->product_detail_id,
            'product_name' => $lang == 'ar' ? (string)$pro->Main_Product['name_ar'] : (string)$pro->Main_Product['name_en'],
            'difference' => $lang == 'ar' ? (string)$pro->difference_ar : (string)$pro->difference_en,
            'quantity' => $this->quantity,
        ];
    }
}
