<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Rep_InvResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');
        return [
            'id' => $this->id,
            'representative_id' => $this->representative_id,
            'inventory_id' => $this->inventory_id,
            // 'car_num' => $this->car_num,
            'Representative' => new RepShowResource($this->Representative),
            'Inventory' => new InventoryResource($this->Inventory),
        ];
    }
}
