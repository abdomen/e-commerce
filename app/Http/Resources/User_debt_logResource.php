<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Models\Shop;
use App\User;

class User_debt_logResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');

        $time = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d/m/Y').' '.\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('H:i:s');

        if ($this->type == 'subtraction'){
            $type = $lang == 'ar' ?'استلام' : 'subtraction';
        }else{
            $type = $lang == 'ar' ?'اضافة' : 'addition';
        }

        if(User::find($this->user_id) != null){
            $user = User::find($this->user_id);
        }else{
            $user = new \stdClass();
            $user->first_name = null;
            $user->last_name = null;
        }

        if(Shop::find($this->admin_id) != null){
            $admin = Shop::find($this->admin_id);
        }else{
            $admin = new \stdClass();
            $admin->name = null;
        }
            
        return [
            'id' => $this->id,
            'admin_name'=>$admin->name,
            'user_name' => $user->first_name.' '.$user->last_name,
            'type'=> $type,
            'value'=> $this->value,
            'date' => $time,

        ];
    }
}
