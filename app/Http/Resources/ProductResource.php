<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers\Manage\BaseController;
use DB,Auth;
use App\Models\Groups;
class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');
        $distance=0;
        if ($request->lat && $request->lng) {
            $distance=$this->select(DB::raw('*, ( 6367 * acos( cos( radians('.$request->lat.') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('.$request->lng.') ) + sin( radians('.$request->lat.') )* sin( radians( lat ) ) ) ) AS distance'))->where('id', $this->id)->value('distance');
        }
        $user=Auth::user();
        $is_fav=false;
        if($user) {
            $is_fav = $this->user_favorite->contains($user->id);
            
            }

            
            //     $group = $this->group;
            
            // return $group;
        return [
            'id' => $this->id,
            'name' => $lang == 'ar' ?(string) $this->name_ar : (string)$this->name_en,
            'desc' => $lang == 'ar' ?(string) $this->desc_ar : (string)$this->desc_en,
            'image' => BaseController::getImageUrl('Products',$this->image),
            'cat_id' => $this->cat_id,
            'inventory_id' => $this->inventory_id,
            'rate' => (int)$this->rate,
            'price_product'=>(string)$this->get_price($user,$lang)[0],
            'price_after_offer'=>$this->is_offer ==1 ?(string)$this->get_price($user,$lang)[1] : (string)"0",
            'quantity'=>(int)$this->quantity,
            'is_offer'=>$this->is_offer ?(int) $this->is_offer : 0,
            'offer_amount'=>$this->offer_amount .'%',
            'lat'=>(double)$this->lat,
            'lng'=>(double)$this->lng,
            'distance'=>(int)$distance . ' km',
            'is_favorite'=>$is_fav,
            'barcode'=>$this->barcode,
            'group' => ProductGroupResource::collection($this->group),
            'product_details' => Product_detailsResource::collection($this->product_details),
            // 'inventory' => $this->inventory,
            // 'categories'=> CategoryResource::collection($this->categories),

        ];
    }
}