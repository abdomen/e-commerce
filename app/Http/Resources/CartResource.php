<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers\Manage\BaseController;
use DB,Auth;
use App\Models\Price;
use App\Models\color;
use App\Models\Sizes;
use App\Models\Products;
class CartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');
        $user=Auth::user();
        $color_code=null;
        $color=color::find($this->pivot->color_id);
        $size_name=null;
        if(!is_null($color)){
            $color_code=new ColorResource($color);
        }
        $size=Sizes::find($this->pivot->size_id);
        if(!is_null($size)){
            $size_name=new SizeResource($size);
        }
        $mPro = Products::find($this->product_id);

        if(isset($this->priceeee[0])) {
            if($user->status == "1"){
                $pro_price = Price::find($this->priceeee[0]->price_id)->selling_price;
            }elseif($user->status == "3"){
                $pro_price = Price::find($this->priceeee[0]->price_id)->wholesale_price;
            }elseif($user->status == "4"){
                $pro_price = Price::find($this->priceeee[0]->price_id)->wholesale_wholesale_price;
            }else{}
        }else{
            $pro_price = false;
        }

        return [
            'id' => $this->id,
            'name' => $lang == 'ar' ? $mPro->name_ar : $mPro->name_en,
            'desc' => $lang == 'ar' ? $mPro->desc_ar : $mPro->desc_en,
            'product_detail_desc' => $lang == 'ar' ? $this->desc_ar : $this->desc_en,
            'product_detail_difference' => $lang == 'ar' ? $this->difference_ar : $this->difference_en,
            'image' => BaseController::getImageUrl('Product_image',$this->image),
            'rate' => $this->rate,
            'is_offer'=>$this->is_offer ? (int)$this->is_offer : 0,
            'offer_amount'=>$this->offer_amount,
            'lat'=>(double)$this->lat,
            'lng'=>(double)$this->lng,
            'is_favorite'=>$this->user_favorite->contains($user->id),
            'color' => $color_code,
            'size'=>$size_name,
            'price' => (int)$pro_price,
            'quantity'=>(int)$this->pivot->quantity,
        ];
    }
}
