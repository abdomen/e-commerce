<?php

use Illuminate\Http\Request;

Route::middleware('auth:api-Shop')->group(function () {

    /*
     * GEt General  info to add product [colors , sizes, categories, Currency]
    */

    Route::post('/edit_info', 'Shop_Api\LoginController@edit_info')->name('shop.edit_info');
    Route::get('/get_info', 'Shop_Api\LoginController@get_info')->name('shop.get_info');
    Route::get('/shop_info', 'Shop_Api\LoginController@shop_info')->name('shop.shop_info');
    Route::post('/edit_settings', 'Shop_Api\LoginController@edit_settings')->name('shop.edit_settings');

    Route::options('/edit_info', 'Shop_Api\LoginController@edit_info')->name('shop.edit_info');
    Route::options('/get_info', 'Shop_Api\LoginController@get_info')->name('shop.get_info');
    Route::options('/shop_info', 'Shop_Api\LoginController@shop_info')->name('shop.shop_info');
    Route::options('/edit_settings', 'Shop_Api\LoginController@edit_settings')->name('shop.edit_settings');

    Route::prefix('general_info')->group(function () {
        Route::get('/get_colors', 'Shop_Api\GeneralInfoController@get_colors')->name('general_info.get_colors');
        Route::get('/get_sizes', 'Shop_Api\GeneralInfoController@get_sizes')->name('general_info.get_sizes');
        Route::get('/get_currency', 'Shop_Api\GeneralInfoController@get_currency')->name('general_info.get_currency');
        Route::get('/get_category', 'Shop_Api\GeneralInfoController@get_category')->name('general_info.get_category');

        Route::options('/get_colors', 'Shop_Api\GeneralInfoController@get_colors')->name('general_info.get_colors');
        Route::options('/get_sizes', 'Shop_Api\GeneralInfoController@get_sizes')->name('general_info.get_sizes');
        Route::options('/get_currency', 'Shop_Api\GeneralInfoController@get_currency')->name('general_info.get_currency');
        Route::options('/get_category', 'Shop_Api\GeneralInfoController@get_category')->name('general_info.get_category');
    });

    /*

     * Add Color

     */

    Route::prefix('Color')->group(function () {
        Route::post('/add_color', 'Shop_Api\ColorController@add_color')->name('Color.add_Color');
        Route::post('/edit_color/{Color_id}', 'Shop_Api\ColorController@edit_color')->name('Color.edit_color');
        Route::get('/delete_color/{Color_id}', 'Shop_Api\ColorController@delete_color')->name('Color.delete_color');
        Route::get('/all_colors', 'Shop_Api\ColorController@all_colors')->name('Color.all_colors');
        Route::get('/single_color/{Color_id}', 'Shop_Api\ColorController@single_color')->name('Color.single_color');

        Route::options('/add_color', 'Shop_Api\ColorController@add_color')->name('Color.add_Color');
        Route::options('/edit_color/{Color_id}', 'Shop_Api\ColorController@edit_color')->name('Color.edit_color');
        Route::options('/delete_color/{Color_id}', 'Shop_Api\ColorController@delete_color')->name('Color.delete_color');
        Route::options('/all_colors', 'Shop_Api\ColorController@all_colors')->name('Color.all_colors');
        Route::options('/single_color/{Color_id}', 'Shop_Api\ColorController@single_color')->name('Color.single_color');
    });

    /***************************************لسه**********************************************************************************/

    Route::prefix('notification')->group(function () {
        Route::get('/get_notification', 'Shop_Api\NotificationController@get_notification')->name('notification.get_notification');
        Route::get('/single_notification/{id}', 'Shop_Api\NotificationController@single_notification')->name('notification.single_notification');
        Route::post('/delete_notification/{id}', 'Shop_Api\NotificationController@delete_notification')->name('notification.delete_notification');

        Route::options('/get_notification', 'Shop_Api\NotificationController@get_notification')->name('notification.get_notification');
        Route::options('/single_notification/{id}', 'Shop_Api\NotificationController@single_notification')->name('notification.single_notification');
        Route::options('/delete_notification/{id}', 'Shop_Api\NotificationController@delete_notification')->name('notification.delete_notification');
    });

    /****************************************************************************************************************************/


    Route::prefix('Size')->group(function () {
        Route::post('/add_Size', 'Shop_Api\SizeController@add_Size')->name('Size.add_Size');
        Route::post('/edit_size/{size_id}', 'Shop_Api\SizeController@edit_size')->name('Size.edit_size');
        Route::get('/delete_size/{Color_id}', 'Shop_Api\SizeController@delete_size')->name('Size.delete_size');
        Route::get('/all_sizes', 'Shop_Api\SizeController@all_Sizes')->name('Size.all_Size');
        Route::get('/single_Size/{Size_id}', 'Shop_Api\SizeController@single_Size')->name('Size.single_Size');

        Route::options('/add_Size', 'Shop_Api\SizeController@add_Size')->name('Size.add_Size');
        Route::options('/edit_size/{size_id}', 'Shop_Api\SizeController@edit_size')->name('Size.edit_size');
        Route::options('/delete_size/{Color_id}', 'Shop_Api\SizeController@delete_size')->name('Size.delete_size');
        Route::options('/all_sizes', 'Shop_Api\SizeController@all_Sizes')->name('Size.all_Size');
        Route::options('/single_Size/{Size_id}', 'Shop_Api\SizeController@single_Size')->name('Size.single_Size');
    });

    /*
     * currency route
     */

    Route::prefix('currency')->group(function () {
        Route::post('/add_currency', 'Shop_Api\CurrencyController@add_currency')->name('currency.add_currency');
        Route::post('/edit_currency/{currency_id}', 'Shop_Api\CurrencyController@edit_currency')->name('currency.edit_currency');
        Route::get('/delete_Currency/{currency_id}', 'Shop_Api\CurrencyController@delete_Currency')->name('currency.delete_Currency');
        Route::get('/single_Currency/{currency_id}', 'Shop_Api\CurrencyController@single_Currency')->name('currency.single_Currency');

        Route::options('/add_currency', 'Shop_Api\CurrencyController@add_currency')->name('currency.add_currency');
        Route::options('/edit_currency/{currency_id}', 'Shop_Api\CurrencyController@edit_currency')->name('currency.edit_currency');
        Route::options('/delete_Currency/{currency_id}', 'Shop_Api\CurrencyController@delete_Currency')->name('currency.delete_Currency');
        Route::options('/single_Currency/{currency_id}', 'Shop_Api\CurrencyController@single_Currency')->name('currency.single_Currency');
    });

    /*
        * role route
    */

    Route::prefix('Role')->group(function () {
        Route::post('/add_Role', 'Shop_Api\RoleController@add_Role')->name('Role.add_Role');
        Route::post('/edit_Role/{Role_id}', 'Shop_Api\RoleController@edit_Role')->name('Role.edit_Role');
        Route::get('/delete_Role/{Role_id}', 'Shop_Api\RoleController@delete_Role')->name('Role.delete_Role');
        Route::get('/single_Role/{Role_id}', 'Shop_Api\RoleController@single_Role')->name('Role.single_Role');
        Route::get('/all_Role', 'Shop_Api\RoleController@all_Role')->name('Role.all_Role');

        Route::options('/add_Role', 'Shop_Api\RoleController@add_Role')->name('Role.add_Role');
        Route::options('/edit_Role/{Role_id}', 'Shop_Api\RoleController@edit_Role')->name('Role.edit_Role');
        Route::options('/delete_Role/{Role_id}', 'Shop_Api\RoleController@delete_Role')->name('Role.delete_Role');
        Route::options('/single_Role/{Role_id}', 'Shop_Api\RoleController@single_Role')->name('Role.single_Role');
        Route::options('/all_Role', 'Shop_Api\RoleController@all_Role')->name('Role.all_Role');
    });

    /*
    * Slider route
*/

    Route::prefix('Role')->group(function () {
        Route::post('/add_Role', 'Shop_Api\RoleController@add_Role')->name('Role.add_Role');
        Route::post('/edit_Role/{Role_id}', 'Shop_Api\RoleController@edit_Role')->name('Role.edit_Role');
        Route::get('/delete_Role/{Role_id}', 'Shop_Api\RoleController@delete_Role')->name('Role.delete_Role');
        Route::get('/single_Role/{Role_id}', 'Shop_Api\RoleController@single_Role')->name('Role.single_Role');
        Route::get('/all_Role', 'Shop_Api\RoleController@all_Role')->name('Role.all_Role');

        Route::options('/add_Role', 'Shop_Api\RoleController@add_Role')->name('Role.add_Role');
        Route::options('/edit_Role/{Role_id}', 'Shop_Api\RoleController@edit_Role')->name('Role.edit_Role');
        Route::options('/delete_Role/{Role_id}', 'Shop_Api\RoleController@delete_Role')->name('Role.delete_Role');
        Route::options('/single_Role/{Role_id}', 'Shop_Api\RoleController@single_Role')->name('Role.single_Role');
        Route::options('/all_Role', 'Shop_Api\RoleController@all_Role')->name('Role.all_Role');
    });

    /*
     * Add Products , My products
     */
    Route::prefix('products')->group(function () {
        Route::post('/add_product', 'Shop_Api\ProductController@add_product')->name('products.add_product');
        Route::post('/edit_product/{product_id}', 'Shop_Api\ProductController@edit_product')->name('products.edit_product');
        Route::get('/delete_product/{product_id}', 'Shop_Api\ProductController@delete_product')->name('products.delete_product');
        Route::get('/all_products', 'Shop_Api\ProductController@all_products')->name('products.all_products');
        Route::get('/product_by_cat_id/{cat_id}', 'Shop_Api\ProductController@product_by_cat_id')->name('products.product_by_cat_id');
        Route::get('/single_product/{product_id}', 'Shop_Api\ProductController@single_product')->name('products.single_product');

        Route::options('/add_product', 'Shop_Api\ProductController@add_product')->name('products.add_product');
        Route::options('/edit_product/{product_id}', 'Shop_Api\ProductController@edit_product')->name('products.edit_product');
        Route::options('/delete_product/{product_id}', 'Shop_Api\ProductController@delete_product')->name('products.delete_product');
        Route::options('/all_products', 'Shop_Api\ProductController@all_products')->name('products.all_products');
        Route::options('/product_by_cat_id/{cat_id}', 'Shop_Api\ProductController@product_by_cat_id')->name('products.product_by_cat_id');
        Route::options('/single_product/{product_id}', 'Shop_Api\ProductController@single_product')->name('products.single_product');
    });

    /*
        * Slider route
    */

    Route::prefix('Slider')->group(function () {
        Route::post('/add_Slider', 'Shop_Api\SliderController@add_Slider')->name('Slider.add_Slider');
        Route::post('/edit_Slider/{sloder_id}', 'Shop_Api\SliderController@edit_Slider')->name('Slider.edit_Slider');
        Route::get('/delete_Slider/{Slider_id}', 'Shop_Api\SliderController@delete_Slider')->name('Slider.delete_Slider');
        Route::get('/single_Slider/{Slider_id}', 'Shop_Api\SliderController@single_Slider')->name('Slider.single_Slider');
        Route::get('/all_Slider', 'Shop_Api\SliderController@all_Slider')->name('Slider.all_Slider');

        Route::options('/add_Slider', 'Shop_Api\SliderController@add_Slider')->name('Slider.add_Slider');
        Route::options('/edit_Slider/{sloder_id}', 'Shop_Api\SliderController@edit_Slider')->name('Slider.edit_Slider');
        Route::options('/delete_Slider/{Slider_id}', 'Shop_Api\SliderController@delete_Slider')->name('Slider.delete_Slider');
        Route::options('/single_Slider/{Slider_id}', 'Shop_Api\SliderController@single_Slider')->name('Slider.single_Slider');
        Route::options('/all_Slider', 'Shop_Api\SliderController@all_Slider')->name('Slider.all_Slider');
    });


    Route::prefix('Ads')->group(function () {
        Route::post('/add_Ads', 'Shop_Api\AdsController@add_Ads')->name('Ads.add_Ads');
        Route::post('/edit_Ads/{id}', 'Shop_Api\AdsController@edit_Ads')->name('Ads.edit_Ads');
        Route::get('/delete_Ads/{Slider_id}', 'Shop_Api\AdsController@delete_Ads')->name('Ads.delete_Ads');
        Route::get('/single_Ads/{Slider_id}', 'Shop_Api\AdsController@single_Ads')->name('Ads.single_Ads');
        Route::get('/all_Ads', 'Shop_Api\AdsController@all_Ads')->name('Ads.all_Ads');

        Route::options('/add_Ads', 'Shop_Api\AdsController@add_Ads')->name('Ads.add_Ads');
        Route::options('/edit_Ads/{id}', 'Shop_Api\AdsController@edit_Ads')->name('Ads.edit_Ads');
        Route::options('/delete_Ads/{Slider_id}', 'Shop_Api\AdsController@delete_Ads')->name('Ads.delete_Ads');
        Route::options('/single_Ads/{Slider_id}', 'Shop_Api\AdsController@single_Ads')->name('Ads.single_Ads');
        Route::options('/all_Ads', 'Shop_Api\AdsController@all_Ads')->name('Ads.all_Ads');
    });


    Route::prefix('brand')->group(function () {
        Route::post('/add_brand', 'Shop_Api\BrandController@add_Brands')->name('brand.add_brand');
        Route::get('/delete_brand/{id}', 'Shop_Api\BrandController@delete_Brands')->name('brand.delete_brand');
        Route::get('/single_brand/{id}', 'Shop_Api\BrandController@single_Brands')->name('brand.single_brand');
        Route::get('/all_brand', 'Shop_Api\BrandController@all_Brands')->name('brand.all_brand');

        Route::options('/add_brand', 'Shop_Api\BrandController@add_Brands')->name('brand.add_brand');
        Route::options('/delete_brand/{id}', 'Shop_Api\BrandController@delete_Brands')->name('brand.delete_brand');
        Route::options('/single_brand/{id}', 'Shop_Api\BrandController@single_Brands')->name('brand.single_brand');
        Route::options('/all_brand', 'Shop_Api\BrandController@all_Brands')->name('brand.all_brand');
    });

    Route::prefix('Gallary')->group(function () {
        Route::post('/add_Gallary', 'Shop_Api\GarallyController@add_Garally')->name('Garally.add_brand');
        Route::get('/delete_Gallary/{id}', 'Shop_Api\GarallyController@delete_Garally')->name('Garally.delete_Garally');
        Route::get('/single_Gallary/{id}', 'Shop_Api\GarallyController@single_Garally')->name('Garally.single_Garally');
        Route::get('/all_Gallary', 'Shop_Api\GarallyController@all_Garally')->name('Garally.all_Garally');

        Route::options('/add_Gallary', 'Shop_Api\GarallyController@add_Garally')->name('Garally.add_brand');
        Route::options('/delete_Gallary/{id}', 'Shop_Api\GarallyController@delete_Garally')->name('Garally.delete_Garally');
        Route::options('/single_Gallary/{id}', 'Shop_Api\GarallyController@single_Garally')->name('Garally.single_Garally');
        Route::options('/all_Gallary', 'Shop_Api\GarallyController@all_Garally')->name('Garally.all_Garally');
    });

    Route::prefix('Contact_us')->group(function () {
        Route::post('/add_Contact_us', 'Shop_Api\Contact_usController@add_Contact_us')->name('Contact_us.add_Contact_us');
        Route::get('/delete_Contact_us/{Contact_us_id}', 'Shop_Api\Contact_usController@delete_Contact_us')->name('Contact_us.delete_Contact_us');
        Route::get('/single_Contact_us/{Contact_us_id}', 'Shop_Api\Contact_usController@single_Contact_us')->name('Contact_us.single_Contact_us');
        Route::get('/all_Contact_us', 'Shop_Api\Contact_usController@all_Contact_us')->name('Contact_us.all_Contact_us');

        Route::options('/add_Contact_us', 'Shop_Api\Contact_usController@add_Contact_us')->name('Contact_us.add_Contact_us');
        Route::options('/delete_Contact_us/{Contact_us_id}', 'Shop_Api\Contact_usController@delete_Contact_us')->name('Contact_us.delete_Contact_us');
        Route::options('/single_Contact_us/{Contact_us_id}', 'Shop_Api\Contact_usController@single_Contact_us')->name('Contact_us.single_Contact_us');
        Route::options('/all_Contact_us', 'Shop_Api\Contact_usController@all_Contact_us')->name('Contact_us.all_Contact_us');
    });

    /*


     *  Add color , Remove Color

     * Add Size , Remove Size

     * Add Currency , Remove Currency

     * Add Image , Remove Image

    */


    Route::prefix('products_ditals')->group(function () {
        Route::post('/add_color/{product_id}', 'Shop_Api\ProductDetialsController@add_color')->name('products_ditals.add_color');
        Route::get('/remove_color/{product_id}', 'Shop_Api\ProductDetialsController@remove_color')->name('products_ditals.remove_color');
        Route::post('/add_size/{product_id}', 'Shop_Api\ProductDetialsController@add_size')->name('products_ditals.add_size');
        Route::get('/remove_size/{product_id}', 'Shop_Api\ProductDetialsController@remove_size')->name('products_ditals.remove_size');
        Route::post('/add_image/{product_id}', 'Shop_Api\ProductDetialsController@add_image')->name('products_ditals.add_image');
        Route::get('/remove_image/{product_id}', 'Shop_Api\ProductDetialsController@remove_image')->name('products_ditals.remove_image');
        Route::post('/add_currency/{product_id}', 'Shop_Api\ProductDetialsController@add_currency')->name('products_ditals.add_currency');
        Route::get('/remove_currency/{product_id}', 'Shop_Api\ProductDetialsController@remove_currency')->name('products_ditals.remove_currency');

        Route::options('/add_color/{product_id}', 'Shop_Api\ProductDetialsController@add_color')->name('products_ditals.add_color');
        Route::options('/remove_color/{product_id}', 'Shop_Api\ProductDetialsController@remove_color')->name('products_ditals.remove_color');
        Route::options('/add_size/{product_id}', 'Shop_Api\ProductDetialsController@add_size')->name('products_ditals.add_size');
        Route::options('/remove_size/{product_id}', 'Shop_Api\ProductDetialsController@remove_size')->name('products_ditals.remove_size');
        Route::options('/add_image/{product_id}', 'Shop_Api\ProductDetialsController@add_image')->name('products_ditals.add_image');
        Route::options('/remove_image/{product_id}', 'Shop_Api\ProductDetialsController@remove_image')->name('products_ditals.remove_image');
        Route::options('/add_currency/{product_id}', 'Shop_Api\ProductDetialsController@add_currency')->name('products_ditals.add_currency');
        Route::options('/remove_currency/{product_id}', 'Shop_Api\ProductDetialsController@remove_currency')->name('products_ditals.remove_currency');
    });

    /*
       * Add users , all user
    */


    Route::prefix('users')->group(function () {
        Route::post('/add_user', 'Shop_Api\UserController@add_user')->name('users.add_user');
        Route::post('/add_whole_seller', 'Shop_Api\UserController@add_whole_seller')->name('users.add_whole_seller');
        Route::post('/add_whole_whole_seller', 'Shop_Api\UserController@add_whole_whole_seller')->name('users.add_whole_whole_seller');
        Route::post('/edit_user/{user_id}', 'Shop_Api\UserController@edit_user')->name('users.edit_user');
        Route::get('/delete_user/{user_id}', 'Shop_Api\UserController@delete_user')->name('users.delete_user');
        Route::post('/send_notifcation/{user_id}', 'Shop_Api\UserController@send_notifcation')->name('users.send_notifcation');
        Route::get('/all_users', 'Shop_Api\UserController@all_users')->name('users.all_users');
        Route::get('/get_user_location/{user_id}', 'Shop_Api\UserController@get_user_location')->name('users.get_user_location');
        Route::get('/single_user/{user_id}', 'Shop_Api\UserController@single_user')->name('users.single_user');
        Route::post('/user_debt_set/{id}', 'Shop_Api\Representative_UserController@user_debt_set')->name('Representative.user_debt_set');

        Route::options('/add_user', 'Shop_Api\UserController@add_user')->name('users.add_user');
        Route::options('/edit_user/{user_id}', 'Shop_Api\UserController@edit_user')->name('users.edit_user');
        Route::options('/delete_user/{user_id}', 'Shop_Api\UserController@delete_user')->name('users.delete_user');
        Route::options('/send_notifcation/{user_id}', 'Shop_Api\UserController@send_notifcation')->name('users.send_notifcation');
        Route::options('/all_users', 'Shop_Api\UserController@all_users')->name('users.all_users');
        Route::options('/get_user_location/{user_id}', 'Shop_Api\UserController@get_user_location')->name('users.get_user_location');
        Route::options('/single_user/{user_id}', 'Shop_Api\UserController@single_user')->name('users.single_user');
    });

    Route::prefix('Admin')->group(function () {
        Route::post('/add_Admin', 'Shop_Api\AdminController@add_Admin')->name('Admin.add_user');
        Route::post('/edit_Admin/{Admin_id}', 'Shop_Api\AdminController@edit_Admin')->name('Admin.edit_Admin');
        Route::get('/delete_Admin/{Admin_id}', 'Shop_Api\AdminController@delete_Admin')->name('Admin.delete_Admin');
        Route::get('/all_Admins', 'Shop_Api\AdminController@all_Admins')->name('Admin.all_Admins');
        Route::get('/single_Admin/{Admin_id}', 'Shop_Api\AdminController@single_Admin')->name('Admin.single_Admin');
        Route::post('/add_role/{Admin_id}', 'Shop_Api\AdminController@add_role')->name('Admin.add_role');
        Route::get('/delete_role/{Admin_id}', 'Shop_Api\AdminController@delete_role')->name('Admin.delete_role');

        Route::options('/add_Admin', 'Shop_Api\AdminController@add_Admin')->name('Admin.add_user');
        Route::options('/edit_Admin/{Admin_id}', 'Shop_Api\AdminController@edit_Admin')->name('Admin.edit_Admin');
        Route::options('/delete_Admin/{Admin_id}', 'Shop_Api\AdminController@delete_Admin')->name('Admin.delete_Admin');
        Route::options('/all_Admins', 'Shop_Api\AdminController@all_Admins')->name('Admin.all_Admins');
        Route::options('/single_Admin/{Admin_id}', 'Shop_Api\AdminController@single_Admin')->name('Admin.single_Admin');
        Route::options('/add_role/{Admin_id}', 'Shop_Api\AdminController@add_role')->name('Admin.add_role');
        Route::options('/delete_role/{Admin_id}', 'Shop_Api\AdminController@delete_role')->name('Admin.delete_role');
    });

    /*
     * Report
     */
    Route::prefix('Report')->group(function () {
        Route::get('/get_counts', 'Shop_Api\ReportsController@get_counts')->name('Report.get_counts');
        //Admin-representative-debt-log
        Route::get('/admin_rep_debt_log', 'Shop_Api\ReportsController@admin_rep_debt_log')->name('Report.admin_rep_debt_log');
        Route::post('/adminRepDebtLog_byDate', 'Shop_Api\ReportsController@adminRepDebtLog_byDate')->name('Report.adminRepDebtLog_byDate');
        Route::get('/rep_debt_log/{representative_id}', 'Shop_Api\ReportsController@rep_debt_log')->name('Report.rep_debt_log');
        Route::get('/admin_debt_log/{admin_id}', 'Shop_Api\ReportsController@admin_debt_log')->name('Report.admin_debt_log');
        //Admin-user-debt-log
        Route::get('/admin_user_debt_log', 'Shop_Api\ReportsController@admin_user_debt_log')->name('Report.admin_user_debt_log');
        Route::post('/adminuserDebtLog_byDate', 'Shop_Api\ReportsController@adminuserDebtLog_byDate')->name('Report.adminuserDebtLog_byDate');
        Route::get('/admin_user_debt_log_by_user/{user_id}', 'Shop_Api\ReportsController@admin_user_debt_log_by_user')->name('Report.admin_user_debt_log_by_user');
        Route::get('/admin_user_debt_log_by_admin/{admin_id}', 'Shop_Api\ReportsController@admin_user_debt_log_by_admin')->name('Report.admin_user_debt_log_by_admin');
        
        //Representative-user-debt-log
        Route::get('/rep_user_debt_log', 'Shop_Api\ReportsController@rep_user_debt_log')->name('Report.rep_user_debt_log');
        Route::post('/repUserDebtLog_byDate', 'Shop_Api\ReportsController@repUserDebtLog_byDate')->name('Report.repUserDebtLog_byDate');
        Route::get('/user_debt_log/{user_id}', 'Shop_Api\ReportsController@user_debt_log')->name('Report.user_debt_log');
        Route::get('/repUser_debt_log/{representative_id}', 'Shop_Api\ReportsController@repUser_debt_log')->name('Report.repUser_debt_log');
        //Representative-selled-quantity
        Route::get('/rep_selled_quantity/{representative_id}', 'Shop_Api\ReportsController@rep_selled_quantity')->name('Report.rep_selled_quantity');
        Route::get('/profit_report', 'Shop_Api\ReportsController@profit_report')->name('Report.profit_report');
        Route::post('/profit_report_byDate', 'Shop_Api\ReportsController@profit_report_byDate')->name('Report.profit_report_byDate');
        Route::get('/total_profit', 'Shop_Api\ReportsController@total_profit')->name('Report.total_profit');
        Route::post('/total_profit_byDate', 'Shop_Api\ReportsController@total_profit_byDate')->name('Report.total_profit_byDate');

        //Safe processes Reports
        Route::get('/safes_log', 'Shop_Api\ReportsController@safes_log')->name('Report.safes_log');
        Route::post('/safes_log_byDate', 'Shop_Api\ReportsController@safes_log_byDate')->name('Report.safes_log_byDate');
        Route::get('/safes_log_bySafeID/{safe_id}', 'Shop_Api\ReportsController@safes_log_bySafeID')->name('Report.safes_log_bySafeID');
        Route::post('/safes_log_bySafeID_byDate/{safe_id}', 'Shop_Api\ReportsController@safes_log_bySafeID_byDate')->name('Report.safes_log_bySafeID_byDate');
        
        //Safe transactions Reports
        Route::get('/safes_transactions_log', 'Shop_Api\ReportsController@safes_transactions_log')->name('Report.safes_transactions_log');
        Route::post('/safes_transactions_byDate', 'Shop_Api\ReportsController@safes_transactions_byDate')->name('Report.safes_transactions_byDate');
        Route::get('/safes_transactions_bySafeID/{safe_id}', 'Shop_Api\ReportsController@safes_transactions_bySafeID')->name('Report.safes_transactions_bySafeID');
        Route::post('/safes_transactions_bySafeID_byDate/{safe_id}', 'Shop_Api\ReportsController@safes_transactions_bySafeID_byDate')->name('Report.safes_transactions_bySafeID_byDate');
    });

    /*
     * About Route
     */
    Route::prefix('About')->group(function () {
        Route::post('/add_About', 'Shop_Api\AboutController@add_About')->name('About.add_user');
        Route::post('/edit_About/{About_id}', 'Shop_Api\AboutController@edit_About')->name('About.edit_About');
        Route::get('/delete_About/{About_id}', 'Shop_Api\AboutController@delete_About')->name('About.delete_About');
        Route::get('/all_About', 'Shop_Api\AboutController@all_About')->name('About.all_About');
        Route::get('/single_About/{About_id}', 'Shop_Api\AboutController@single_About')->name('About.single_About');

        Route::options('/add_About', 'Shop_Api\AboutController@add_About')->name('About.add_user');
        Route::options('/edit_About/{About_id}', 'Shop_Api\AboutController@edit_About')->name('About.edit_About');
        Route::options('/delete_About/{About_id}', 'Shop_Api\AboutController@delete_About')->name('About.delete_About');
        Route::options('/all_About', 'Shop_Api\AboutController@all_About')->name('About.all_About');
        Route::options('/single_About/{About_id}', 'Shop_Api\AboutController@single_About')->name('About.single_About');
    });

    /*
        * Add Order , all Orders
    */

    Route::prefix('Order')->group(function () {
        Route::post('/add_order', 'Shop_Api\OrderController@add_order')->name('Order.add_order');
        Route::get('/delete_order/{Order_id}', 'Shop_Api\OrderController@delete_order')->name('Order.delete_order');
        Route::get('/all_Order', 'Shop_Api\OrderController@all_Order')->name('Order.all_Order');
        Route::get('/single_Order/{Order_id}', 'Shop_Api\OrderController@single_order')->name('Order.single_Order');
        Route::get('/filter_Orders', 'Shop_Api\OrderController@filter_Orders')->name('Order.filter_Orders');
        Route::post('/add_product_to_order/{Order_id}', 'Shop_Api\OrderController@add_product_to_order')->name('Order.add_product_to_order');
        Route::post('/edit_order/{Order_id}', 'Shop_Api\OrderController@edit_order')->name('Order.edit_order');
        Route::post('/filter_by_status', 'Shop_Api\OrderController@filter_by_status')->name('Order.filter_by_status');
        Route::post('/edit_status/{Order_id}', 'Shop_Api\OrderController@edit_status')->name('Order.edit_status');

        Route::options('/add_order', 'Shop_Api\OrderController@add_order')->name('Order.add_order');
        Route::options('/delete_order/{Order_id}', 'Shop_Api\OrderController@delete_order')->name('Order.delete_order');
        Route::options('/all_Order', 'Shop_Api\OrderController@all_Order')->name('Order.all_Order');
        Route::options('/single_Order/{Order_id}', 'Shop_Api\OrderController@single_order')->name('Order.single_Order');
        Route::options('/filter_Orders', 'Shop_Api\OrderController@filter_Orders')->name('Order.filter_Orders');
        Route::options('/add_product_to_order/{Order_id}', 'Shop_Api\OrderController@add_product_to_order')->name('Order.add_product_to_order');
        Route::options('/edit_order/{Order_id}', 'Shop_Api\OrderController@edit_order')->name('Order.edit_order');
        Route::options('/filter_by_status', 'Shop_Api\OrderController@filter_by_status')->name('Order.filter_by_status');
        Route::options('/edit_status/{Order_id}', 'Shop_Api\OrderController@edit_status')->name('Order.edit_status');
    });

    /*
     * category route
     */
    Route::prefix('category')->group(function () {
        Route::post('/add_category', 'Shop_Api\categoryController@add_category')->name('category.add_category');
        Route::post('/edit_category/{category_id}', 'Shop_Api\categoryController@edit_category')->name('category.edit_category');
        Route::get('/delete_category/{category_id}', 'Shop_Api\categoryController@delete_category')->name('category.delete_category');
        Route::get('/single_category/{category_id}', 'Shop_Api\categoryController@single_category')->name('category.single_category');

        Route::options('/add_category', 'Shop_Api\categoryController@add_category')->name('category.add_category');
        Route::options('/edit_category/{category_id}', 'Shop_Api\categoryController@edit_category')->name('category.edit_category');
        Route::options('/delete_category/{category_id}', 'Shop_Api\categoryController@delete_category')->name('category.delete_category');
        Route::options('/single_category/{category_id}', 'Shop_Api\categoryController@single_category')->name('category.single_category');
    });

    /*
        * Discount System
    */

    Route::prefix('Discount')->group(function () {
        Route::post('/create_code', 'Shop_Api\DiscountController@create_code')->name('Discount.create_code');
        Route::get('/get_codes', 'Shop_Api\DiscountController@get_codes')->name('Discount.get_codes');
        Route::get('/delete_code/{code_id}', 'Shop_Api\DiscountController@delete_code')->name('Discount.delete_code');
        Route::get('/single_code/{code_id}', 'Shop_Api\DiscountController@single_code')->name('Discount.single_code');
        Route::post('/edit_code/{code_id}', 'Shop_Api\DiscountController@edit_code')->name('Discount.edit_code');
        Route::post('/add_users_to_code/{code_id}', 'Shop_Api\DiscountController@add_users_to_code')->name('Discount.add_users_to_code');
        Route::get('/delete_users_from_code/{code_id}', 'Shop_Api\DiscountController@delete_users_from_code')->name('Discount.delete_users_from_code');

        Route::options('/create_code', 'Shop_Api\DiscountController@create_code')->name('Discount.create_code');
        Route::options('/get_codes', 'Shop_Api\DiscountController@get_codes')->name('Discount.get_codes');
        Route::options('/delete_code/{code_id}', 'Shop_Api\DiscountController@delete_code')->name('Discount.delete_code');
        Route::options('/single_code/{code_id}', 'Shop_Api\DiscountController@single_code')->name('Discount.single_code');
        Route::options('/edit_code/{code_id}', 'Shop_Api\DiscountController@edit_code')->name('Discount.edit_code');
        Route::options('/add_users_to_code/{code_id}', 'Shop_Api\DiscountController@add_users_to_code')->name('Discount.add_users_to_code');
        Route::options('/delete_users_from_code/{code_id}', 'Shop_Api\DiscountController@delete_users_from_code')->name('Discount.delete_users_from_code');
    });


    /*
        * Discount System
    */

    Route::prefix('Group')->group(function () {
        Route::post('/add_group', 'Shop_Api\GroupsController@add_group')->name('Group.add_group');
        Route::post('/edit_group/{group_id}', 'Shop_Api\GroupsController@edit_group')->name('Group.edit_group');
        Route::get('/all_groups', 'Shop_Api\GroupsController@all_groups')->name('Group.all_groups');
        Route::get('/single_group/{group_id}', 'Shop_Api\GroupsController@single_group')->name('Group.single_group');
        Route::get('/delete_group/{group_id}', 'Shop_Api\GroupsController@delete_group')->name('Group.delete_group');

        Route::options('/add_group', 'Shop_Api\GroupsController@add_group')->name('Group.add_group');
        Route::options('/edit_group/{group_id}', 'Shop_Api\GroupsController@edit_group')->name('Group.edit_group');
        Route::options('/all_groups', 'Shop_Api\GroupsController@all_groups')->name('Group.all_groups');
        Route::options('/single_group/{group_id}', 'Shop_Api\GroupsController@single_group')->name('Group.single_group');
        Route::options('/delete_group/{group_id}', 'Shop_Api\GroupsController@delete_group')->name('Group.delete_group');
    });


    Route::prefix('Group_gallery')->group(function () {

        Route::post('/add_Group_gallery', 'Shop_Api\Group_galleryController@add_Group_gallery')->name('Group_gallery.add_Group_gallery');
        Route::get('/delete_Group_gallery_image/{Group_gallery_id}', 'Shop_Api\Group_galleryController@delete_Group_gallery_image')->name('Group_gallery.delete_Group_gallery_image');
        Route::get('/single_Group_gallery_image/{Group_gallery_id}', 'Shop_Api\Group_galleryController@single_Group_gallery_image')->name('Group_gallery.single_Group_gallery_image');
        Route::get('/all_Group_Gallary_images', 'Shop_Api\Group_galleryController@all_Group_Gallary_images')->name('Group_gallery.all_Group_Gallary_images');

        Route::options('/add_Group_gallery', 'Shop_Api\Group_galleryController@add_Group_gallery')->name('Group_gallery.add_Group_gallery');
        Route::options('/delete_Group_gallery_image/{Group_gallery_id}', 'Shop_Api\Group_galleryController@delete_Group_gallery_image')->name('Group_gallery.delete_Group_gallery_image');
        Route::options('/single_Group_gallery_image/{Group_gallery_id}', 'Shop_Api\Group_galleryController@single_Group_gallery_image')->name('Group_gallery.single_Group_gallery_image');
        Route::options('/all_Group_Gallary_images', 'Shop_Api\Group_galleryController@all_Group_Gallary_images')->name('Group_gallery.all_Group_Gallary_images');
    });

    Route::prefix('Product_group')->group(function () {
        Route::post('/add_Product_group', 'Shop_Api\Product_groupController@add_Product_group')->name('Product_group.add_Product_group');
        Route::post('/edit_Product_group/{Product_group_id}', 'Shop_Api\Product_groupController@edit_Product_group')->name('Product_group.edit_Product_group');
        Route::get('/all_Product_group', 'Shop_Api\Product_groupController@all_Product_group')->name('Product_group.all_Product_group');
        Route::get('/single_Product_group/{Product_group_id}', 'Shop_Api\Product_groupController@single_Product_group')->name('Product_group.single_Product_group');
        Route::get('/delete_Product_group/{Product_group_id}', 'Shop_Api\Product_groupController@delete_Product_group')->name('Product_group.delete_Product_group');

        Route::options('/add_Product_group', 'Shop_Api\Product_groupController@add_Product_group')->name('Product_group.add_Product_group');
        Route::options('/edit_Product_group/{Product_group_id}', 'Shop_Api\Product_groupController@edit_Product_group')->name('Product_group.edit_Product_group');
        Route::options('/all_Product_group', 'Shop_Api\Product_groupController@all_Product_group')->name('Product_group.all_Product_group');
        Route::options('/single_Product_group/{Product_group_id}', 'Shop_Api\Product_groupController@single_Product_group')->name('Product_group.single_Product_group');
        Route::options('/delete_Product_group/{Product_group_id}', 'Shop_Api\Product_groupController@delete_Product_group')->name('Product_group.delete_Product_group');
    });


    Route::prefix('Group_discount')->group(function () {
        Route::post('/add_Group_discount', 'Shop_Api\Group_discountController@add_Group_discount')->name('Group_discount.add_Group_discount');
        Route::post('/edit_Group_discount/{Group_discount_id}', 'Shop_Api\Group_discountController@edit_Group_discount')->name('Group_discount.edit_Group_discount');
        Route::get('/all_Group_discount', 'Shop_Api\Group_discountController@all_Group_discount')->name('Group_discount.all_Group_discount');
        Route::get('/single_Group_discount/{Group_discount_id}', 'Shop_Api\Group_discountController@single_Group_discount')->name('Group_discount.single_Group_discount');
        Route::get('/delete_Group_discount/{Group_discount_id}', 'Shop_Api\Group_discountController@delete_Group_discount')->name('Group_discount.delete_Group_discount');

        Route::options('/add_Group_discount', 'Shop_Api\Group_discountController@add_Group_discount')->name('Group_discount.add_Group_discount');
        Route::options('/edit_Group_discount/{Group_discount_id}', 'Shop_Api\Group_discountController@edit_Group_discount')->name('Group_discount.edit_Group_discount');
        Route::options('/all_Group_discount', 'Shop_Api\Group_discountController@all_Group_discount')->name('Group_discount.all_Group_discount');
        Route::options('/single_Group_discount/{Group_discount_id}', 'Shop_Api\Group_discountController@single_Group_discount')->name('Group_discount.single_Group_discount');
        Route::options('/delete_Group_discount/{Group_discount_id}', 'Shop_Api\Group_discountController@delete_Group_discount')->name('Group_discount.delete_Group_discount');
    });


    Route::prefix('Price')->group(function () {
        Route::post('/add_Price', 'Shop_Api\PriceController@add_Price')->name('Price.add_Price');
        Route::post('/edit_Price/{Price_id}', 'Shop_Api\PriceController@edit_Price')->name('Price.edit_Price');
        Route::get('/all_Price', 'Shop_Api\PriceController@all_Price')->name('Price.all_Price');
        Route::get('/delete_Price/{Price_id}', 'Shop_Api\PriceController@delete_Price')->name('Price.delete_Price');
        Route::get('/single_Price/{Price_id}', 'Shop_Api\PriceController@single_Price')->name('Price.single_Price');

        Route::options('/add_Price', 'Shop_Api\PriceController@add_Price')->name('Price.add_Price');
        Route::options('/edit_Price/{Price_id}', 'Shop_Api\PriceController@edit_Price')->name('Price.edit_Price');
        Route::options('/all_Price', 'Shop_Api\PriceController@all_Price')->name('Price.all_Price');
        Route::options('/delete_Price/{Price_id}', 'Shop_Api\PriceController@delete_Price')->name('Price.delete_Price');
        Route::options('/single_Price/{Price_id}', 'Shop_Api\PriceController@single_Price')->name('Price.single_Price');
    });


    Route::prefix('Inventory')->group(function () {
        Route::post('/add_Inventory', 'Shop_Api\InventoryController@add_Inventory')->name('Inventory.add_Inventory');
        Route::post('/edit_Inventory/{Inventory_id}', 'Shop_Api\InventoryController@edit_Inventory')->name('Inventory.edit_Inventory');
        Route::get('/all_Inventory', 'Shop_Api\InventoryController@all_Inventory')->name('Inventory.all_Inventory');
        Route::get('/single_Inventory/{Inventory_id}', 'Shop_Api\InventoryController@single_Inventory')->name('Inventory.single_Inventory');
        Route::get('/delete_Inventory/{Inventory_id}', 'Shop_Api\InventoryController@delete_Inventory')->name('Inventory.delete_Inventory');
        Route::get('/Inventories_stock/{inv_id}', 'Shop_Api\InventoryController@Inventories_stock')->name('Inventory.Inventories_stock');
        Route::post('/inventory_transformation', 'Shop_Api\InventoryController@inventory_transformation')->name('Inventory.inventory_transformation');
        Route::post('/inventory_multi_transformation', 'Shop_Api\InventoryController@inventory_multi_transformation')->name('Inventory.inventory_multi_transformation');        
        Route::post('/add_ProductDetail_toInventory', 'Shop_Api\InventoryController@add_ProductDetail_toInventory')->name('Inventory.add_ProductDetail_toInventory');
        Route::get('/all_inventory_transformation', 'Shop_Api\InventoryController@all_inventory_transformation')->name('Inventory.all_inventory_transformation');
        Route::post('/inventoryTransformation_byDate', 'Shop_Api\InventoryController@inventoryTransformation_byDate')->name('Inventory.inventoryTransformation_byDate');
        Route::post('/Inventory_edit_stock', 'Shop_Api\InventoryController@Inventory_edit_stock')->name('Inventory.Inventory_edit_stock');
        Route::get('/Inventories_stockEdit_show', 'Shop_Api\InventoryController@Inventories_stockEdit_show')->name('Inventory.Inventories_stockEdit_show');
        Route::post('/Inventories_stockEdit_show_byDate', 'Shop_Api\InventoryController@Inventories_stockEdit_show_byDate')->name('Inventory.Inventories_stockEdit_show_byDate');
        Route::post('/product_qty_edit/{inv_id}/{product_detail_id}', 'Shop_Api\InventoryController@product_qty_edit')->name('Inventory.product_qty_edit');
        Route::get('/qty_edit_log', 'Shop_Api\InventoryController@qty_edit_log')->name('Inventory.qty_edit_log');
        Route::post('/qty_edit_log_byDate', 'Shop_Api\InventoryController@qty_edit_log_byDate')->name('Inventory.qty_edit_log_byDate');

        Route::options('/add_Inventory', 'Shop_Api\InventoryController@add_Inventory')->name('Inventory.add_Inventory');
        Route::options('/edit_Inventory/{Inventory_id}', 'Shop_Api\InventoryController@edit_Inventory')->name('Inventory.edit_Inventory');
        Route::options('/all_Inventory', 'Shop_Api\InventoryController@all_Inventory')->name('Inventory.all_Inventory');
        Route::options('/single_Inventory/{Inventory_id}', 'Shop_Api\InventoryController@single_Inventory')->name('Inventory.single_Inventory');
        Route::options('/delete_Inventory/{Inventory_id}', 'Shop_Api\InventoryController@delete_Inventory')->name('Inventory.delete_Inventory');
        Route::options('/Inventories_stock/{inv_id}', 'Shop_Api\InventoryController@Inventories_stock')->name('Inventory.Inventories_stock');
        Route::options('/Inventory_edit_stock/{inv_id}', 'Shop_Api\InventoryController@Inventory_edit_stock')->name('Inventory.Inventory_edit_stock');
        Route::options('/inventory_transformation/{product_detail_id}', 'Shop_Api\InventoryController@inventory_transformation')->name('Inventory.inventory_transformation');

    });


    Route::prefix('Product_details')->group(function () {
        Route::post('/add_Product_details', 'Shop_Api\Product_detailsController@add_Product_details')->name('Product_details.add_Product_details');
        Route::post('/edit_Product_details/{Product_details_id}', 'Shop_Api\Product_detailsController@edit_Product_details')->name('Product_details.edit_Product_details');
        Route::get('/all_Product_details', 'Shop_Api\Product_detailsController@all_Product_details')->name('Product_details.all_Product_details');
        Route::get('/single_Product_details/{Product_details_id}', 'Shop_Api\Product_detailsController@single_Product_details')->name('Product_details.single_Product_details');
        Route::get('/delete_Product_details/{Product_details_id}', 'Shop_Api\Product_detailsController@delete_Product_details')->name('Product_details.delete_Product_details');
        Route::get('/Product_details_by_inv/{inventory_id}', 'Shop_Api\Product_detailsController@Product_details_by_inv')->name('Product_details.Product_details_by_inv');
        Route::get('/Product_detail_inventories/{product_detail_id}', 'Shop_Api\Product_detailsController@Product_detail_inventories')->name('Product_details.Product_detail_inventories');
        Route::get('/product_Product_details/{product_id}', 'Shop_Api\Product_detailsController@product_Product_details')->name('Product_details.product_Product_details');

        Route::options('/add_Product_details', 'Shop_Api\Product_detailsController@add_Product_details')->name('Product_details.add_Product_details');
        Route::options('/edit_Product_details/{Product_details_id}', 'Shop_Api\Product_detailsController@edit_Product_details')->name('Product_details.edit_Product_details');
        Route::options('/all_Product_details', 'Shop_Api\Product_detailsController@all_Product_details')->name('Product_details.all_Product_details');
        Route::options('/single_Product_details/{Product_details_id}', 'Shop_Api\Product_detailsController@single_Product_details')->name('Product_details.single_Product_details');
        Route::options('/delete_Product_details/{Product_details_id}', 'Shop_Api\Product_detailsController@delete_Product_details')->name('Product_details.delete_Product_details');
        Route::options('/Product_details_by_inv/{inventory_id}', 'Shop_Api\Product_detailsController@Product_details_by_inv')->name('Product_details.Product_details_by_inv');
        Route::options('/product_Product_details/{product_id}', 'Shop_Api\Product_detailsController@product_Product_details')->name('Product_details.product_Product_details');

    });


    Route::prefix('Product_details_price')->group(function () {
        Route::post('/add_Product_details_price', 'Shop_Api\Product_details_priceController@add_Product_details_price')->name('Product_details_price.add_Product_details_price');
        Route::post('/edit_Product_details_price/{Product_details_price_id}', 'Shop_Api\Product_details_priceController@edit_Product_details_price')->name('Product_details_price.edit_Product_details_price');
        Route::get('/all_Product_details_price', 'Shop_Api\Product_details_priceController@all_Product_details_price')->name('Product_details_price.all_Product_details_price');
        Route::get('/single_Product_details_price/{Product_details_price_id}', 'Shop_Api\Product_details_priceController@single_Product_details_price')->name('Product_details_price.single_Product_details_price');
        Route::get('/delete_Product_details_price/{Product_details_price_id}', 'Shop_Api\Product_details_priceController@delete_Product_details_price')->name('Product_details_price.delete_Product_details_price');

        Route::options('/add_Product_details_price', 'Shop_Api\Product_details_priceController@add_Product_details_price')->name('Product_details_price.add_Product_details_price');
        Route::options('/edit_Product_details_price/{Product_details_price_id}', 'Shop_Api\Product_details_priceController@edit_Product_details_price')->name('Product_details_price.edit_Product_details_price');
        Route::options('/all_Product_details_price', 'Shop_Api\Product_details_priceController@all_Product_details_price')->name('Product_details_price.all_Product_details_price');
        Route::options('/single_Product_details_price/{Product_details_price_id}', 'Shop_Api\Product_details_priceController@single_Product_details_price')->name('Product_details_price.single_Product_details_price');
        Route::options('/delete_Product_details_price/{Product_details_price_id}', 'Shop_Api\Product_details_priceController@delete_Product_details_price')->name('Product_details_price.delete_Product_details_price');
    });
    
        
    Route::prefix('Representative')->group(function ()
    {
        Route::post('/add_representative', 'Shop_Api\Rep_InvController@add_representative')->name('Representative.add_representative');
        Route::post('/edit_representative/{representative_id}', 'Shop_Api\Rep_InvController@edit_representative')->name('Representative.edit_representative');
        Route::get('/all_representative', 'Shop_Api\Rep_InvController@all_representative')->name('Representative.all_representative');
        Route::get('/single_representative/{representative_id}', 'Shop_Api\Rep_InvController@single_representative')->name('Representative.single_representative');
        Route::get('/single_inv_representative/{representative_id}', 'Shop_Api\Rep_InvController@single_inv_representative')->name('Representative.single_inv_representative');
        Route::get('/delete_Representative/{representative_id}', 'Shop_Api\Rep_InvController@delete_Representative')->name('Representative.delete_Representative');
        Route::post('/add_user_representative/{representative_id}', 'Shop_Api\Representative_UserController@add_user_representative')->name('Representative.add_user_representative');
        Route::get('/all_user_representative', 'Shop_Api\Representative_UserController@all_user_representative')->name('Representative.all_user_representative');
        Route::get('/delete_user_representative/{user_id}', 'Shop_Api\Representative_UserController@delete_user_representative')->name('Representative.delete_user_representative');
        // Route::get('/delete_user_representative/{user_representative_id}', 'Shop_Api\Representative_UserController@delete_user_representative')->name('Representative.delete_user_representative');
        Route::get('/get_representative_location/{representative_id}', 'Shop_Api\Representative_UserController@get_representative_location')->name('Representative.get_representative_location');

        Route::get('/representatives_of_user/{user_id}', 'Shop_Api\Representative_UserController@representatives_of_user')->name('Representative.representatives_of_user');
        Route::get('/users_of_representative/{representative_id}', 'Shop_Api\Representative_UserController@users_of_representative')->name('Representative.users_of_representative');
    
        Route::get('/get_debt/{id}', 'Shop_Api\Representative_UserController@get_debt')->name('Representative.get_debt');
        Route::post('/debt_set/{id}', 'Shop_Api\Representative_UserController@debt_set')->name('Representative.debt_set');


        Route::options('/add_representative', 'Shop_Api\Rep_InvController@add_representative')->name('Representative.add_representative');
        Route::options('/edit_representative/{representative_id}', 'Shop_Api\Rep_InvController@edit_representative')->name('Representative.edit_representative');
        // Route::options('/all_representative', 'Shop_Api\Rep_InvController@all_representative')->name('Representative.all_representative');
        Route::options('/single_representative/{representative_id}', 'Shop_Api\Rep_InvController@single_representative')->name('Representative.single_representative');
        Route::options('/delete_Representative/{representative_id}', 'Shop_Api\Rep_InvController@delete_Representative')->name('Representative.delete_Representative');
        Route::options('/add_user_representative', 'Shop_Api\Representative_UserController@add_user_representative')->name('Representative.add_user_representative');
        Route::options('/all_user_representative', 'Shop_Api\Representative_UserController@all_user_representative')->name('Representative.all_user_representative');
        Route::options('/delete_user_representative', 'Shop_Api\Representative_UserController@delete_user_representative')->name('Representative.delete_user_representative');
        Route::options('/delete_user_representative/{user_representative_id}', 'Shop_Api\Representative_UserController@delete_user_representative')->name('Representative.delete_user_representative');
        Route::options('/get_representative_location/{representative_id}', 'Shop_Api\Representative_UserController@get_representative_location')->name('Representative.get_representative_location');

        Route::options('/representatives_of_user/{user_id}', 'Shop_Api\Representative_UserController@representatives_of_user')->name('Representative.representatives_of_user');
        Route::options('/users_of_representative/{representative_id}', 'Shop_Api\Representative_UserController@users_of_representative')->name('Representative.users_of_representative');
    
        Route::options('/get_debt/{id}', 'Shop_Api\Representative_UserController@get_debt')->name('Representative.get_debt');
        Route::options('/debt_set/{id}', 'Shop_Api\Representative_UserController@debt_set')->name('Representative.debt_set');

    });


    Route::prefix('p_d_Order')->group(function () {
        Route::post('/add_P_d_order', 'Shop_Api\P_d_orderController@add_P_d_order')->name('p_d_Order.add_P_d_order');
        Route::get('/delete_P_d_order/{P_d_order_id}', 'Shop_Api\P_d_orderController@delete_P_d_order')->name('p_d_Order.delete_P_d_order');
        Route::get('/all_p_d_Order', 'Shop_Api\P_d_orderController@all_p_d_Order')->name('p_d_Order.all_p_d_Order');
        Route::get('/single_P_d_order/{P_d_order_id}', 'Shop_Api\P_d_orderController@single_P_d_order')->name('p_d_Order.single_P_d_order');
        Route::get('/filter_P_d_orders_by_DMY', 'Shop_Api\P_d_orderController@filter_P_d_orders')->name('p_d_Order.filter_P_d_orders');
        Route::post('/filter_P_d_orders_by_date', 'Shop_Api\P_d_orderController@filter_P_d_orders_by_date')->name('p_d_Order.filter_P_d_orders_by_date');
        Route::post('/add_product_to_P_d_order/{P_d_order_id}', 'Shop_Api\P_d_orderController@add_product_to_P_d_order')->name('p_d_Order.add_product_to_P_d_order');
        Route::post('/edit_P_d_order/{P_d_order_id}', 'Shop_Api\P_d_orderController@edit_P_d_order')->name('p_d_Order.edit_P_d_order');
        Route::post('/filter_by_status', 'Shop_Api\P_d_orderController@filter_by_status')->name('p_d_Order.filter_by_status');
        Route::post('/edit_status/{P_d_order_id}', 'Shop_Api\P_d_orderController@edit_status')->name('p_d_Order.edit_status');
        Route::get('/all_p_d_Order_with_products', 'Shop_Api\P_d_orderController@all_p_d_Order_with_products')->name('p_d_Order.all_p_d_Order_with_products');
        Route::get('/single_P_d_order_with_products/{P_d_order_id}', 'Shop_Api\P_d_orderController@single_P_d_order_with_products')->name('p_d_Order.single_P_d_order_with_products');
        Route::get('/most_sell', 'Shop_Api\P_d_orderController@most_sell')->name('p_d_Order.most_sell');
        Route::get('/make_bill/{P_d_order_id}','Shop_Api\P_d_orderController@make_bill')->name('Accounts.make_bill');
        Route::post('/add_user_address/{user_id}', 'Shop_Api\P_d_orderController@add_user_address')->name('p_d_Order.add_user_address');
        Route::get('/order_user_addresses/{user_id}', 'Shop_Api\P_d_orderController@order_user_addresses')->name('p_d_Order.order_user_addresses');
        Route::get('/representative_orders/{representative_id}', 'Shop_Api\P_d_orderController@representative_orders')->name('p_d_Order.representative_orders');
        Route::get('/user_orders/{user_id}', 'Shop_Api\P_d_orderController@user_orders')->name('p_d_Order.user_orders');
        Route::post('/order_return/{P_d_order_id}', 'Shop_Api\P_d_orderController@order_return')->name('p_d_Order.order_return');
        Route::get('/product_inventories', 'Shop_Api\P_d_orderController@product_inventories')->name('p_d_Order.product_inventories');
        Route::get('/searchBy_nameORbarcode', 'Shop_Api\P_d_orderController@searchBy_nameORbarcode')->name('p_d_Order.searchBy_nameORbarcode');
        Route::post('/make_p_d_order', 'Shop_Api\P_d_orderController@make_p_d_order')->name('p_d_Order.make_p_d_order');
        Route::get('/bill_show/{P_d_order_id}', 'Shop_Api\P_d_orderController@bill_show')->name('p_d_Order.bill_show');
        Route::post('/edit_order_product/{order_id}', 'Shop_Api\P_d_orderController@edit_order_product')->name('p_d_Order.edit_order_product');

        Route::post('/add_order_status', 'Shop_Api\P_d_orderController@add_order_status')->name('p_d_Order.add_order_status');
        Route::post('/edit_order_status/{status_id}', 'Shop_Api\P_d_orderController@edit_order_status')->name('p_d_Order.edit_order_status');
        Route::get('/delete_order_status/{status_id}', 'Shop_Api\P_d_orderController@delete_order_status')->name('p_d_Order.delete_order_status');
        Route::get('/show_order_status', 'Shop_Api\P_d_orderController@show_order_status')->name('p_d_Order.show_order_status');

        Route::options('/add_P_d_order', 'Shop_Api\P_d_orderController@add_P_d_order')->name('p_d_Order.add_P_d_order');
        Route::options('/delete_P_d_order/{P_d_order_id}', 'Shop_Api\P_d_orderController@delete_P_d_order')->name('p_d_Order.delete_P_d_order');
        Route::options('/all_p_d_Order', 'Shop_Api\P_d_orderController@all_p_d_Order')->name('p_d_Order.all_p_d_Order');
        Route::options('/single_P_d_order/{P_d_order_id}', 'Shop_Api\P_d_orderController@single_P_d_order')->name('p_d_Order.single_P_d_order');
        Route::options('/filter_P_d_orders_by_DMY', 'Shop_Api\P_d_orderController@filter_P_d_orders')->name('p_d_Order.filter_P_d_orders');
        Route::options('/filter_P_d_orders_by_date', 'Shop_Api\P_d_orderController@filter_P_d_orders_by_date')->name('p_d_Order.filter_P_d_orders_by_date');
        Route::options('/add_product_to_P_d_order/{P_d_order_id}', 'Shop_Api\P_d_orderController@add_product_to_P_d_order')->name('p_d_Order.add_product_to_P_d_order');
        Route::options('/edit_P_d_order/{P_d_order_id}', 'Shop_Api\P_d_orderController@edit_P_d_order')->name('p_d_Order.edit_P_d_order');
        Route::options('/filter_by_status', 'Shop_Api\P_d_orderController@filter_by_status')->name('p_d_Order.filter_by_status');
        Route::options('/edit_status/{P_d_order_id}', 'Shop_Api\P_d_orderController@edit_status')->name('p_d_Order.edit_status');
        Route::options('/all_p_d_Order_with_products', 'Shop_Api\P_d_orderController@all_p_d_Order_with_products')->name('p_d_Order.all_p_d_Order_with_products');
        Route::options('/single_P_d_order_with_products/{P_d_order_id}', 'Shop_Api\P_d_orderController@single_P_d_order_with_products')->name('p_d_Order.single_P_d_order_with_products');
        Route::options('/most_sell', 'Shop_Api\P_d_orderController@most_sell')->name('p_d_Order.most_sell');
        Route::options('/make_bill/{P_d_order_id}','Shop_Api\P_d_orderController@make_bill')->name('Accounts.make_bill');
    });

    Route::prefix('Tax')->group(function () {
        Route::post('/add_Tax','Shop_Api\TaxController@add_Tax')->name('Tax.add_Tax');
        Route::post('/edit_Tax/{id}','Shop_Api\TaxController@edit_Tax')->name('Tax.edit_Tax');
        Route::get('/all_Tax','Shop_Api\TaxController@all_Tax')->name('Tax.all_Tax');
        Route::get('/single_Tax/{id}','Shop_Api\TaxController@single_Tax')->name('Tax.single_Tax');
        Route::get('/delete_Tax/{id}','Shop_Api\TaxController@delete_Tax')->name('Tax.delete_Tax');

        Route::options('/add_Tax','Shop_Api\TaxController@add_Tax')->name('Tax.add_Tax');
        Route::options('/edit_Tax/{id}','Shop_Api\TaxController@edit_Tax')->name('Tax.edit_Tax');
        Route::options('/all_Tax','Shop_Api\TaxController@all_Tax')->name('Tax.all_Tax');
        Route::options('/single_Tax/{id}','Shop_Api\TaxController@single_Tax')->name('Tax.single_Tax');
        Route::options('/delete_Tax/{id}','Shop_Api\TaxController@delete_Tax')->name('Tax.delete_Tax');
    });

    Route::prefix('Accounts')->group(function () {
        Route::get('/user_orders/{user_id}','Shop_Api\AccountsController@user_orders')->name('Accounts.user_orders');
        Route::get('/user_orders_sum/{user_id}','Shop_Api\AccountsController@user_orders_sum')->name('Accounts.user_orders_sum');
        Route::get('/representative_orders/{representative_id}','Shop_Api\AccountsController@representative_orders')->name('Accounts.representative_orders');
        Route::get('/representative_orders_sum/{representative_id}','Shop_Api\AccountsController@representative_orders_sum')->name('Accounts.representative_orders_sum');
        Route::get('/user_representative_orders','Shop_Api\AccountsController@user_representative_orders')->name('Accounts.user_representative_orders');
        Route::get('/user_representative_orders_sum','Shop_Api\AccountsController@user_representative_orders_sum')->name('Accounts.user_representative_orders_sum');
        Route::get('/representative_orders_of_days','Shop_Api\AccountsController@representative_orders_of_days')->name('Accounts.representative_orders_days');
        Route::get('/Price_filter','Shop_Api\AccountsController@Price_filter')->name('Accounts.Price_filter');
        Route::get('/make_bill','Shop_Api\AccountsController@make_bill')->name('Accounts.make_bill');

        Route::options('/user_orders/{user_id}','Shop_Api\AccountsController@user_orders')->name('Accounts.user_orders');
        Route::options('/user_orders_sum/{user_id}','Shop_Api\AccountsController@user_orders_sum')->name('Accounts.user_orders_sum');
        Route::options('/representative_orders/{representative_id}','Shop_Api\AccountsController@representative_orders')->name('Accounts.representative_orders');
        Route::options('/representative_orders_sum/{representative_id}','Shop_Api\AccountsController@representative_orders_sum')->name('Accounts.representative_orders_sum');
        Route::options('/user_representative_orders','Shop_Api\AccountsController@user_representative_orders')->name('Accounts.user_representative_orders');
        Route::options('/user_representative_orders_sum','Shop_Api\AccountsController@user_representative_orders_sum')->name('Accounts.user_representative_orders_sum');
        Route::options('/representative_orders_of_days','Shop_Api\AccountsController@representative_orders_of_days')->name('Accounts.representative_orders_days');
        Route::options('/Price_filter','Shop_Api\AccountsController@Price_filter')->name('Accounts.Price_filter');
        Route::options('/make_bill','Shop_Api\AccountsController@make_bill')->name('Accounts.make_bill');
    });


    Route::prefix('Attendence')->group(function () {
        Route::get('/all_attendence','Shop_Api\UserController@all_attendence')->name('Attendence.all_attendence');
        Route::get('/representative_attendence/{representative_id}','Shop_Api\UserController@representative_attendence')->name('Attendence.representative_attendence');
        Route::get('/single_attendence/{id}','Shop_Api\UserController@single_attendence')->name('Attendence.single_attendence');
        Route::post('/filter_attendence_by_date','Shop_Api\UserController@filter_attendence_by_date')->name('Attendence.filter_attendence_by_date');
    });


    Route::prefix('Safe')->group(function () {
        Route::post('/add_safe','Shop_Api\SafeController@add_safe')->name('Safe.add_safe');
        Route::post('/edit_safe/{safe_id}','Shop_Api\SafeController@edit_safe')->name('Safe.edit_safe');
        Route::get('/delete_safe/{safe_id}','Shop_Api\SafeController@delete_safe')->name('Safe.delete_safe');
        Route::get('/single_safe/{safe_id}','Shop_Api\SafeController@single_safe')->name('Safe.single_safe');
        Route::get('/all_safes','Shop_Api\SafeController@all_safes')->name('Safe.all_safes');
        Route::post('/add_safe_transaction','Shop_Api\SafeController@add_safe_transaction')->name('Safe.add_safe_transaction');
    });

 });

Route::post('/login', 'Shop_Api\LoginController@login');




