<?php

use Illuminate\Http\Request;

Route::middleware('auth:api-Admin')->group(function () {

    /*
     * all admin routes
    */
    
    Route::prefix('Admin')->middleware(['roleChecker:null,Admins'])->group(function(){

        Route::post('add_generalAdmin', 'GeneralDash\GeneralAdminController@add_generalAdmin');
        Route::post('edit_generalAdmin/{adminId}', 'GeneralDash\GeneralAdminController@edit_generalAdmin');
        Route::get('/delete_generalAdmin/{adminId}', 'GeneralDash\GeneralAdminController@delete_generalAdmin');
        Route::get('/all_generalAdmins', 'GeneralDash\GeneralAdminController@all_generalAdmins');
        Route::get('/single_generalAdmin/{adminId}', 'GeneralDash\GeneralAdminController@single_generalAdmin');
        Route::post('/add_role/{adminId}', 'GeneralDash\GeneralAdminController@add_role');
        Route::get('/delete_role/{adminId}', 'GeneralDash\GeneralAdminController@delete_role');

    });

    /*
     * all main shop routes
    */

    Route::prefix('MainShops')->middleware(['roleChecker:Shops,null'])->group(function(){

        Route::post('add_mainShop', 'GeneralDash\MainShopController@add_mainShop');
        Route::post('edit_mainShop/{MainShopId}', 'GeneralDash\MainShopController@edit_mainShop');
        Route::get('/delete_mainShop/{MainShopId}', 'GeneralDash\MainShopController@delete_mainShop');
        Route::get('/all_mainShops', 'GeneralDash\MainShopController@all_mainShops');
        Route::get('/single_mainShop/{MainShopId}', 'GeneralDash\MainShopController@single_mainShop');
        Route::post('/change_mainShop_status/{MainShopId}', 'GeneralDash\MainShopController@change_mainShop_status');
        Route::get('/mainShop_details/{MainShopId}', 'GeneralDash\MainShopController@mainShop_details');

    });

});

Route::post('/login', 'GeneralDash\GeneralAdminController@login');






        // // lang Routes
        // Route::prefix('city')->group(function()
        // {
        //     Route::get('/view', 'Api\Admin\CityController@view');
        //     Route::post('/store', 'Api\Admin\CityController@store');
        //     Route::get('/show/{id}', 'Api\Admin\CityController@show');
        //     Route::post('/update/{id}', 'Api\Admin\CityController@update');
        //     Route::post('/delete/{id}', 'Api\Admin\CityController@delete');
        // });


        // // lang Routes
        // Route::prefix('Section')->group(function()
        // {
        //     Route::get('/view', 'Api\Admin\SectionController@view');
        //     Route::post('/store', 'Api\Admin\SectionController@store');
        //     Route::get('/show/{id}', 'Api\Admin\SectionController@show');
        //     Route::post('/update/{id}', 'Api\Admin\SectionController@update');
        //     Route::post('/delete/{id}', 'Api\Admin\SectionController@delete');
        // });

        // // lang Routes
        // Route::prefix('Currency')->group(function()
        // {
        //     Route::get('/view', 'Api\Admin\CurrencyController@view');
        //     Route::post('/store', 'Api\Admin\CurrencyController@store');
        //     Route::get('/show/{id}', 'Api\Admin\CurrencyController@show');
        //     Route::post('/update/{id}', 'Api\Admin\CurrencyController@update');
        //     Route::post('/delete/{id}', 'Api\Admin\CurrencyController@delete');
        // });


        // // lang Routes
        // Route::prefix('Brand')->group(function()
        // {
        //     Route::get('/view', 'Api\Admin\BrandController@view');
        //     Route::post('/store', 'Api\Admin\BrandController@store');
        //     Route::get('/show/{id}', 'Api\Admin\BrandController@show');
        //     Route::post('/update/{id}', 'Api\Admin\BrandController@update');
        //     Route::post('/delete/{id}', 'Api\Admin\BrandController@delete');
        // });


        // // lang Routes
        // Route::prefix('Sliders')->group(function()
        // {
        //     Route::get('/view', 'Api\Admin\SlidersController@view');
        //     Route::post('/store', 'Api\Admin\SlidersController@store');
        //     Route::get('/show/{id}', 'Api\Admin\SlidersController@show');
        //     Route::post('/update/{id}', 'Api\Admin\SlidersController@update');
        //     Route::post('/delete/{id}', 'Api\Admin\SlidersController@delete');
        // });


        // // Category Routes
        // Route::prefix('Category')->group(function()
        // {
        //     Route::get('/view', 'Api\Admin\CategoryController@view');
        //     Route::post('/store', 'Api\Admin\CategoryController@store');
        //     Route::get('/show/{id}', 'Api\Admin\CategoryController@show');
        //     Route::post('/update/{id}', 'Api\Admin\CategoryController@update');
        //     Route::post('/delete/{id}', 'Api\Admin\CategoryController@delete');
        // });
