<?php


use Illuminate\Http\Request;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: *');
header('Access-Control-Allow-Headers: lang,Authorization,Content-Type');

header('Content-Type: application/json; charset=UTF-8', true);
/** Start Auth Route **/

Route::middleware('auth:api')->group(function () {
    //Auth_private
    Route::prefix('Auth_private')->group(function () {
        Route::post('/change_password', 'Api\UserController@change_password')->name('user.change_password');
        Route::post('/edit_profile', 'Api\UserController@edit_profile')->name('user.edit_profile');
        Route::get('/my_wishlist', 'Api\UserController@my_wishlist')->name('user.my_wishlist');
        Route::get('/my_cart', 'Api\UserController@my_cart')->name('user.my_cart');
        Route::post('/settings', 'Api\UserController@settings')->name('user.settings');
        Route::get('/my_info', 'Api\UserController@my_info')->name('user.my_info');
        Route::get('/get_notification', 'Api\UserController@get_notification')->name('user.get_notification');
        Route::post('/save_fire_base_token', 'Api\UserController@save_fire_base_token')->name('user.settings');
        Route::post('/save_image', 'Api\UserController@save_image')->name('user.save_image');
        Route::post('/add_address', 'Api\UserController@add_address')->name('user.add_address');
        Route::get('/logout', 'Api\UserController@logout')->name('user.logout');
        Route::get('/count_of_cart', 'Api\UserController@count_of_cart')->name('user.count_of_cart');
//      Route::get('/resend_verification_code', 'Api\UserController@resend_verification_code')->name('user.resend_verification_code');
        Route::get('/activate_account', 'Api\UserController@activate_account')->name('user.activate_account');
    });
    
    Route::prefix('Auth_general')->group(function () {
        Route::post('/user_register_inRepApp', 'Api\UserController@user_register_inRepApp')->name('user.user_register_inRepApp');
    });

    //rate & comment
    Route::prefix('rate_comment')->group(function () {
        Route::post('/save_my_rate/{product_id}', 'Api\RateCommentController@save_my_rate')->name('rate_comment.save_my_rate');
        Route::post('/save_my_comment/{product_id}', 'Api\RateCommentController@save_my_comment')->name('rate_comment.save_my_comment');
        Route::post('/edit_my_comment/{comment_id}', 'Api\RateCommentController@edit_my_comment')->name('rate_comment.edit_my_comment');
        Route::post('/delete_my_comment/{comment_id}', 'Api\RateCommentController@delete_my_comment')->name('rate_comment.delete_my_comment');
    });

    //Orders Route
    Route::prefix('Order')->group(function () {
        Route::post('/make_order', 'Api\OrderController@make_order')->name('Order.make_order');
        Route::post('/order_address/{order_id}', 'Api\OrderController@order_address')->name('Order.order_address');
        Route::post('/add_payment_method/{order_id}', 'Api\OrderController@add_payment_method')->name('Order.add_payment_method');
        Route::post('/rate_order/{order_id}', 'Api\OrderController@rate_order')->name('Order.rate_order');
        Route::post('/raport_order/{order_id}', 'Api\OrderController@raport_order')->name('Order.raport_order');
        Route::get('/my_order', 'Api\OrderController@my_order')->name('Order.my_order');
        Route::post('/filter_my_order_byStatus', 'Api\OrderController@filter_my_order_byStatus')->name('Order.filter_my_order_byStatus');
        Route::get('/single_order/{order_id}', 'Api\OrderController@single_order')->name('Order.single_order');
        Route::get('/user_address', 'Api\OrderController@user_address')->name('Order.user_address');
        Route::post('/add_order_once', 'Api\OrderController@add_order_once')->name('Order.add_order_once');
        Route::post('/check_discount_code', 'Api\OrderController@check_discount_code')->name('Order.check_discount_code');
        Route::post('/remove_code/{code_id}', 'Api\OrderController@remove_code')->name('Order.remove_code');
        Route::get('/all_p_d_Order', 'Api\Rep_orderController@all_p_d_Order')->name('Order.all_p_d_Order'); 
        Route::get('/filter_P_d_orders_by_DMY', 'Api\Rep_orderController@filter_P_d_orders')->name('Order.filter_P_d_orders');
        Route::get('/filter_P_d_orders_by_date', 'Api\Rep_orderController@filter_P_d_orders_by_date')->name('Order.filter_P_d_orders_by_date');
        Route::get('/show_order_status', 'Api\Rep_orderController@show_order_status')->name('Order.show_order_status');
        
        Route::post('/add_P_d_order', 'Api\Rep_orderController@add_P_d_order')->name('Order.add_P_d_order');
        Route::get('/delete_P_d_order/{P_d_order_id}', 'Api\Rep_orderController@delete_P_d_order')->name('Order.delete_P_d_order');
        Route::get('/single_P_d_order/{P_d_order_id}', 'Api\Rep_orderController@single_P_d_order')->name('Order.single_P_d_order');
        Route::post('/add_product_to_P_d_order/{P_d_order_id}', 'Api\Rep_orderController@add_product_to_P_d_order')->name('Order.add_product_to_P_d_order');
        Route::post('/edit_P_d_order/{order_id}', 'Api\Rep_orderController@edit_P_d_order')->name('Order.edit_P_d_order');
        Route::post('/filter_order_by_status', 'Api\Rep_orderController@filter_by_status')->name('Order.filter_by_status');
        Route::post('/edit_status/{P_d_order_id}', 'Api\Rep_orderController@edit_status')->name('Order.edit_status');
        Route::get('/all_p_d_Order_with_products', 'Api\Rep_orderController@all_p_d_Order_with_products')->name('Order.all_p_d_Order_with_products');
        Route::get('/single_P_d_order_with_products/{P_d_order_id}', 'Api\Rep_orderController@single_P_d_order_with_products')->name('Order.single_P_d_order_with_products');
        Route::get('/most_sell', 'Api\Rep_orderController@most_sell')->name('Order.most_sell');
        Route::get('/make_bill/{P_d_order_id}','Api\Rep_orderController@make_bill')->name('Accounts.make_bill');
        Route::post('/add_user_address/{user_id}', 'Api\Rep_orderController@add_user_address')->name('p_d_Order.add_user_address');
        Route::get('/order_user_addresses/{user_id}', 'Api\Rep_orderController@order_user_addresses')->name('p_d_Order.order_user_addresses');
        Route::get('/all_Product_details', 'Api\ProductController@all_Product_details')->name('p_d_Order.all_Product_details');
        Route::get('/single_Product_details/{Product_details_id}', 'Api\ProductController@single_Product_details')->name('p_d_Order.single_Product_details');
        Route::get('/all_users', 'Api\UserController@all_users')->name('p_d_Order.all_users');
        Route::get('/representative_orders', 'Api\Rep_orderController@representative_orders')->name('p_d_Order.representative_orders');
        Route::get('/user_orders', 'Api\Rep_orderController@user_orders')->name('p_d_Order.user_orders');
        Route::post('/order_return/{P_d_order_id}', 'Api\Rep_orderController@order_return')->name('p_d_Order.order_return');
        Route::get('/finished_orders', 'Api\Rep_orderController@finished_orders')->name('p_d_Order.finished_orders');

    });

    //Products Route
    Route::prefix('Product')->group(function () {
        Route::post('/wishlist/{product_id}', 'Api\ProductController@wishlist')->name('product.wishlist');
        Route::post('/add_to_cart/{product_id}', 'Api\ProductController@add_to_cart')->name('product.add_to_cart');
        Route::post('/delete_from_cart/{product_id}', 'Api\ProductController@delete_from_cart')->name('product.delete_from_cart');
        Route::post('/update_cart', 'Api\ProductController@update_cart')->name('product.update_cart');
    });
    //messages Route
    Route::prefix('message')->group(function () {
        Route::post('/send_message', 'Api\MessageController@send_message')->name('message.send_message');
        Route::get('/get_message', 'Api\MessageController@get_message')->name('message.get_message');
    });

    Route::prefix('Representative')->group(function () {
        Route::post('/add_user_representative', 'Api\Rep_orderController@add_user_representative')->name('Representative.add_user_representative');
        Route::get('/delete_user_representative/{user_id}', 'Api\Rep_orderController@delete_user_representative')->name('Representative.delete_user_representative');
        Route::get('/users_of_representative', 'Api\Rep_orderController@users_of_representative')->name('Representative.users_of_representative');
        Route::post('/debt_set/{user_id}', 'Api\Rep_orderController@debt_set')->name('Representative.debt_set');
    });

    Route::prefix('Attendence')->group(function () {
        Route::post('/add_attendence', 'Api\UserController@add_attendence')->name('Attendence.add_attendence');
        Route::get('/my_attendence', 'Api\UserController@my_attendence')->name('Attendence.my_attendence');
        Route::get('/filter_my_attendence_by_date', 'Api\UserController@filter_my_attendence_by_date')->name('Attendence.filter_my_attendence_by_date');
    });

    //Home Route
    Route::prefix('Home')->group(function () {
    Route::get('/home', 'Api\homeController@home')->name('home.home');
    Route::get('/home_website', 'Api\homeController@home_website')->name('home.home_website');
    Route::get('/search_by_name', 'Api\homeController@search_by_name')->name('home.search_by_name');
    Route::get('/get_main_cat', 'Api\homeController@get_main_cat')->name('home.get_main_cat');
    Route::get('/filter', 'Api\homeController@filter')->name('home.home');
    Route::get('/filter_website', 'Api\homeController@filter_website')->name('home.filter_website');
    Route::get('/new_item', 'Api\homeController@new_item')->name('general.new_item');
    Route::post('/mob_home_filter', 'Api\homeController@mob_home_filter')->name('home.mob_home_filter');
    Route::get('/mob_home_sort', 'Api\homeController@mob_home_sort')->name('home.mob_home_sort');
    Route::post('/mob_home_sorted_filter', 'Api\homeController@mob_home_sorted_filter')->name('home.mob_home_sorted_filter');
    
    });


    Route::prefix('Product')->group(function () {
        Route::get('/product_details/{product_id}', 'Api\ProductController@prduct_detials')->name('product.product_details');
        Route::get('/similar_Product_details/{Product_details_id}', 'Api\ProductController@similar_Product_details')->name('product.similar_Product_details');
        Route::get('/products_by_category/{cat_id}', 'Api\ProductController@products_by_category')->name('product.products');
        // Route::get('/get_products', 'Api\ProductController@get_products')->name('product.get_products');
        Route::get('/get_comments/{product_id}', 'Api\RateCommentController@get_comments')->name('product.get_comments');
    
    });

});
/** End Auth Route **/

//general Auth
Route::prefix('Auth_general')->group(function () {
    Route::post('/register', 'Api\UserController@register')->name('user.register');
    Route::post('/register_social', 'Api\UserController@register_social')->name('user.register_social');
    // Route::post('/add_representative', 'Api\UserController@add_representative')->name('user.add_representative');
    Route::post('/representative_register_social', 'Api\UserController@representative_register_social')->name('user.register_social');
    Route::post('/login', 'Api\UserController@login')->name('user.login');
    Route::post('/login_social', 'Api\UserController@login_social')->name('user.login_social');
    Route::post('/rep_login', 'Api\UserController@rep_login')->name('user.rep_login');
    Route::get('/check_virfuy/{id}', 'Api\UserController@check_virfuy')->name('user.check_virfuy');
    Route::post('/forget_password', 'Api\UserController@forget_password')->name('user.forget_password');
    Route::post('/reset_password', 'Api\UserController@reset_password')->name('user.reset_password');
});

/*
 * GeneralController
 * about us , contact us
 */
Route::prefix('general')->group(function () {
    Route::post('/contact_us', 'Api\GeneralController@contact_us')->name('general.contact_us');
    Route::get('/about_us', 'Api\GeneralController@about_us')->name('general.about_us');
    Route::get('/get_currencies', 'Api\GeneralController@get_currencies')->name('general.get_currency');
    Route::get('/get_info', 'Api\GeneralController@get_info')->name('general.get_info');
    Route::get('/about_us_web_site', 'Api\GeneralController@about_us_web_site')->name('general.about_us_web_site');
});

//Products Route
Route::prefix('Product')->group(function () {
    Route::get('/get_products', 'Api\ProductController@get_products')->name('product.get_products');
//     Route::get('/product_details/{product_id}', 'Api\ProductController@prduct_detials')->name('product.product_details');
//     Route::get('/products_by_category/{cat_id}', 'Api\ProductController@products_by_category')->name('product.products');
//     Route::get('/get_comments/{product_id}', 'Api\RateCommentController@get_comments')->name('product.get_comments');

});

// //Home Route
// Route::prefix('Home')->group(function () {
//     Route::get('/home', 'Api\homeController@home')->name('home.home');
//     Route::get('/home_website', 'Api\homeController@home_website')->name('home.home_website');
//     Route::get('/search_by_name', 'Api\homeController@search_by_name')->name('home.search_by_name');
//     Route::get('/get_main_cat', 'Api\homeController@get_main_cat')->name('home.get_main_cat');
//     Route::get('/filter', 'Api\homeController@filter')->name('home.home');
//     Route::get('/filter_website', 'Api\homeController@filter_website')->name('home.filter_website');
//     Route::get('/new_item', 'Api\homeController@new_item')->name('general.new_item');
// });

Route::post('/sender_test', 'testContoller@test');
Route::post('/test_n', 'testContoller@test_n')->name('user.test_n');
Route::get('/delete_mostafa', 'Api\UserController@delete_mostafa')->name('user.delete_mostafa');

Route::get('/sentNotifcationToCoustomUser/{id}', 'Api\UserController@sentNotifcationToCoustomUser')->name('user.sentNotifcationToCoustomUser');

Route::post('/html_to_image', 'Api\Rep_orderController@html_to_image');