@extends('layouts.app')

@section('title')
    {{trans('main.home')}}
@endsection

@section('content')
    @include('include.slider')

    @include('include.about_header')

    @include('include.product_secion1')

    @include('include.product_section2')

    @include('include.suscribe')


    @include('include.our_brands')


@endsection
