@extends('layouts.app')
@section('title')
    404
    @endsection

@section('content')

    <div class="breadcrumb_section bg_gray page-title-mini">
        <div class="container"><!-- STRART CONTAINER -->
            <div class="row align-items-center">
                <div class="col-md-6">
                    <div class="page-title">
                        <h1>404</h1>
                    </div>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb justify-content-md-end">
                        <li class="breadcrumb-item"><a href="/">{{trans('main.home')}}</a></li>
                        <li class="breadcrumb-item active">404</li>
                    </ol>
                </div>
            </div>
        </div><!-- END CONTAINER-->
    </div>
    <div class="main_content">

        <!-- START 404 SECTION -->
        <div class="section">
            <div class="error_wrap">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-lg-6 col-md-10 order-lg-first">
                            <div class="text-center">
                                <div class="error_txt">404</div>

                                <h5 class="mb-2 mb-sm-3">
                                    @if(get_lang() =='ar') عفوا الصفحة المطلوبة غير موجودة @else oops! The page you requested was not found! @endif
                                </h5>
                                <div class="search_form pb-3 pb-md-4">
                                    <form  action="{{Route('pages.search')}}">
                                        <input name="text" id="text" type="text" placeholder="{{trans('main.search')}}"  class="form-control">
                                        <button type="submit" class="btn icon_search"><i class="ion-ios-search-strong"></i></button>
                                    </form>
                                </div>
                                <a href="/" class="btn btn-fill-out">{{trans('main.Back_To_Home')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END 404 SECTION -->

        @include('include.suscribe')

    </div>

@endsection


