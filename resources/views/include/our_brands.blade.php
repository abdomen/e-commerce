@php
$brands=\App\Models\Brands::take(6)->get();
@endphp

<div class="container">
    <div class="row">
        <div class="partner-area">
            <ul>
                @foreach($brands as $row)
                <li><img data-cfsrc="{{getImageUrl('Brands',$row->image)}}" width="137" height="45" alt="partner logo" style="display:none;visibility:hidden;"></li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
