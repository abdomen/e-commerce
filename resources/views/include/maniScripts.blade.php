<script>
            @if(Auth::check())
    var color=0;
    var size=0;
    var quantity=1;
    function choose_color(color_id)
    {
        color= color_id;
    }
    function size_id(size_id)
    {
        size= size_id;
    }
    function add_to_cart(product_id)
    {

        Toset('{{trans("main.progress")}}','info','',false);
        quantity=$('#quantity').val() > 0 ? $('#quantity').val() : 1;

        $.ajax({
            url : '/User/add_to_cart/' + product_id +'?color_id='+color +'&size_id='+size+'&quantity='+quantity,
            type : 'get',
                        headers: { 'lang': '{{get_lang()}}' },

            success: function(data)
            {
                $.toast().reset('all');
                if(data.status == 1){
                    Toset(data.message,'success','',false);
                    color=0;
                    size=0;
                    quantity=1;
                    location.reload();
                }else if(data.status==0){
                    Toset(data.message,'error','',false);
                }
            },
            error : function(data)
            {
                $.toast().reset('all');
                Toset('{{trans("main.serverError")}}','error','',5000);
            }
        })
    }
    @else
    function add_to_cart(product_id)
    {
        location.href='{{Route("User.show_login")}}';
    }
    @endif
</script>


<script>
    function delete_from_cart(product_id )
    {
        Toset('{{trans("main.progress")}}','info','',false);

        $.ajax({
            url : '/User/delete_from_cart/' + product_id ,
            type : 'get',
            headers: { 'lang': '{{get_lang()}}' },
            success: function(data)
            {
                $.toast().reset('all');
                if(data.status == 1){
                    Toset(data.message,'success','',false);
                    location.reload();
                }else if(data.status==0){
                    Toset(data.message,'error','',false);
                }
            },
            error : function(data)
            {
                $.toast().reset('all');
                Toset('{{trans("main.serverError")}}','error','',5000);
            }
        })
    }
</script>





<script>
    function goToCat() {
        if($('#catCatCaaat').val() != 0){
        location.href='/Products/product_by_category/'+ $('#catCatCaaat').val();
        }
    }

    @if(Auth::check())
    function wishlist(product_id,reload)
    {
        Toset('{{trans("main.progress")}}','info','',false);

        $.ajax({
            url : '/User/wishlist/' + product_id +'?getCount=1',
            type : 'get',
            headers: { 'lang': '{{get_lang()}}' },
            success: function(data)
            {
                $.toast().reset('all');
                if(data.status == 1){
                    Toset(data.message,'success','',5000);
                    $('#whishList_count').text(data.data.get_count);
                    data.data.check==1 ? $('#whish_list_style_'+product_id).css({'color' : 'red'}) : $('#whish_list_style_'+product_id).css({'color' : ''});
                    reload==2 ? location.reload() : '';
                }else if(data.status==0){
                    Toset(data.message,'error','',false);
                }
            },
            error : function(data)
            {
                $.toast().reset('all');
                Toset('{{trans("main.serverError")}}','error','',5000);
            }
        })
    }
    @else
    function wishlist() {
        location.href='{{Route("User.show_login")}}';
    }
    @endif
</script>

<script>
    $('#subscribe_form').submit(function(e){
        e.preventDefault();
        Toset('{{trans("main.progress")}}','info','',false);
        $.ajax({
            url : '/pages/subscribe?email='+$('#susbcribe_email').val() ,
            type : 'get',
            headers: { 'lang': '{{get_lang()}}' },
            success: function(data)
            {
                $.toast().reset('all');
                if(data.status == 1){
                    Toset(data.message,'success','',5000);
                    $('#subscribe_form')[0].reset();
                }else if(data.status==0){
                    Toset(data.message,'error','',5000);
                }
            },
            error : function(data)
            {
                $.toast().reset('all');
                Toset('{{trans("main.serverError")}}','error','',5000);
            }
        })

    })
</script>

<script>
    function changeLang() {
        var langKey = $('#langKey').val();
        if (langKey == 'ar') {
            location.href = '{{ LaravelLocalization::getLocalizedUrl("ar") }}';
        } else {
            location.href = '{{ LaravelLocalization::getLocalizedUrl("en") }}';
        }
    }
</script>


<script>
    function quick_View(id) {
        alert(5);
        $('#myModal').modal('toggle');
    }
</script>