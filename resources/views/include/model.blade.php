<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{trans('main.addLogo')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="cart_form">
                    @csrf
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">{{trans('main.logo')}}:</label>
                        <input type="file" name="image" class="form-control" id="recipient-name">
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{trans('main.Close')}}</button>
                <button type="submit" class="btn btn-primary">{{trans('main.save')}}</button>
                </form>
            </div>
        </div>
    </div>
</div>