<div class="subscribe-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <ul>
                    <li>{{trans('main.subscribe')}}
                    </li>
                    <li>
                        <form id="subscribe_form">
                        <input style="color: #fff" name="email" id="susbcribe_email" type="email" required placeholder="{{trans('main.Email_address')}}">
                    </li>
                    <li>
                        <button type="submit">{{trans('main.Subscribe')}}</button>
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
