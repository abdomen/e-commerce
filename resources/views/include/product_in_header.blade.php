<li class="dropdown dropdown-mega-menu">
    <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">{{trans('main.Products')}}</a>
    <div class="dropdown-menu">
        <ul class="mega-menu d-lg-flex">
            @foreach(getCats(2,4) as $cat)
                <li class="mega-menu-col col-lg-3">
                    <ul>
                        <li class="dropdown-header">{{getTitleOrDesc($cat,1)}}</li>
                        @foreach($cat->product as $key=>$row)
                            @if($key ==5) @break @endif
                        <li><a class="dropdown-item nav-link nav_item" href="{{route('productsFront.productDetails',$row->id)}}">{{getTitleOrDesc($row,1)}}</a></li>
                            @endforeach
                    </ul>
                </li>
                @endforeach
        </ul>

        <div class="d-lg-flex menu_banners">
            @foreach(get_ads(3,2) as $row)
            <div class="col-lg-6">
                <div class="header-banner">
                    <img src="{{asset('/images/Ads/'.$row->image)}}" alt="menu_banner2">
                    <div class="banne_info">
                        <h6>{{ getlastmod() =='ar' ? $row->sub_title_ar : $row->sub_title_en }}</h6>
                        <h4>{{ getlastmod() =='ar' ? $row->title_ar : $row->title_en }}</h4>

                        @if($row->btn_link)
                        <a target="_blank" href="{{$row->btn_link}}">{{ getlastmod() =='ar' ? $row->btn_text_ar : $row->btn_text_en }}</a>
                            @endif
                    </div>
                </div>
            </div>
                @endforeach

        </div>
    </div>
</li>
