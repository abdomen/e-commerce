<div class="home_fashion_area">
    <div class="container-fluid">
        <div class="row">
        @foreach(getProduct(1,1) as $row)
            <div class="fashion_area_lft"><img data-cfsrc="{{getImageUrl('Products',$row->image)}}" alt="Fashion"
                                               style="display:none;visibility:hidden;">
                <noscript><img src="{{getImageUrl('Products',$row->image)}}" alt="Fashion"></noscript>
                <div class="fashion_bx1_overlay">
                    <h1>{{getTitleOrDesc($row,1)}}</h1>
                    <div><a href="{{route('productsFront.productDetails',$row->id)}}" class="cmn-btn2">{{trans('main.shop_now')}}</a></div>
                </div>
            </div>
            @endforeach
            <div class="fashion_area_rht">
                <ul>
                    @foreach(getProduct(1,4) as $key=>$row)
                        @if($key !=0)
                    <li><img data-cfsrc="{{getImageUrl('Products',$row->image)}}" alt="Fashion" style="display:none;visibility:hidden;">
                        <div class="fashion_overlay2">
                            <h3>{{getTitleOrDesc($row,1)}}</h3>
                            <h6><a href="{{route('productsFront.productDetails',$row->id)}}">{{trans('main.shop_now')}} <i class="fa fa-long-arrow-right"></i></a></h6>
                        </div>
                    </li>
                        @endif
                        @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
