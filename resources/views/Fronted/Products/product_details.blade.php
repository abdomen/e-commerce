@extends('layouts.app')
{{--@php--}}
{{--$check=Auth::user()->my_cart->contains($product->id);--}}
{{--$value=$check ? Auth::user()--}}
{{--@endphp--}}
@section('title')
    {{getTitleOrDesc($product,1)}}
@endsection

@section('content')
    <div class="header-top-inner">
        <div class="container">

            <h6>{{getTitleOrDesc($product->cat,1)}}</h6>
            <h2>{{getTitleOrDesc($product,1)}}</h2>

        </div>
    </div>


    <div class="featured-area">
        <div class="container">

            <div class="product_details clr">
                <div class="product_details_img ">
                    <div class="images oflow"><a href="{{getImageUrl('Products',$product->image)}}" class="woocommerce-main-image zoom">
                            <img
                                    data-cfsrc="{{getImageUrl('Products',$product->image)}}" class="attachment-shop_thumbnail" alt=""
                                    style="display:none;visibility:hidden;"/>
                            <noscript><img src="{{getImageUrl('Products',$product->image)}}" class="attachment-shop_thumbnail" alt=""/>
                            </noscript>
                        </a>
                        <div class="thumbnails">
                            @foreach($product->images as $row)
                            <a href="{{getImageUrl('Product_image',$row->image)}}" class="zoom first"> <img data-cfsrc="{{getImageUrl('Product_image',$row->image)}}"
                                                                      class="attachment-shop_thumbnail" alt=""
                                                                      style="display:none;visibility:hidden;"/>
                                <noscript><img src="{{getImageUrl('Product_image',$row->image)}}" class="attachment-shop_thumbnail" alt=""/>
                                </noscript>
                            </a>
                                @endforeach

                        </div>
                    </div>
                </div>
                <div class="product_details_txt">
                    {!! product_price($product) !!}
                    <div class="rating_bx clr">
                        <div class="rating">
                             <i class="far fa-star"></i>
                            @for($i=0;$i<$product->rate;$i++)
                            <i class="far fa-star"></i>
                            @endfor
                           
                            
                        </div>
{{--                        <div class="write_review"><a href="#"><i class="fa fa-edit"></i> Write A Review </a></div>--}}
                    </div>
                    <div class="product_code"><span>{{trans('main.Product_Code')}}: </span> {{$product->id}}</div>
                    <div class="product_code"><span>{{trans('main.Category')}}: </span> {{getTitleOrDesc($product->cat,1)}}</div>
                    <div class="size_bx clr">
                        @if($product->sizes->count() > 0)
                        <div class="size size_radio">
                            <ul>

                                <li>{{trans('main.size')}}:</li>
                               @foreach($product->sizes as $row)
                                <li>
                                    <input onclick="size_id('{{$row->id}}')" type="radio" id="f2" name="cc"/>
                                    <label for="f2"><span>{{getTitleOrDesc($row,1)}}</span></label>
                                </li>
                                   @endforeach
                            </ul>
                        </div>
                        @endif
                            @if($product->colors->count() > 0)
                        <div class="size colors color_radio">
                            <ul>
                                <li>{{trans('main.color')}}:</li>
                                @foreach($product->colors as $row)
                                <li>
                                    <input onclick="choose_color('{{$row->id}}')" type="radio" style="background-color: {{$row->color_code}}"  name="cc"/>
                                    <label for="c4"><span></span></label>
                                </li>
                                    @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif
                    <div class="add_btn_area">
                        <div class="qty_bx">
                            <div class="sp-quantity">
                                <div class="sp-minus fff"><a class="ddd" href="javascript:void(0)">-</a></div>
                                <div class="sp-input">
                                    <input type="text" class="quntity-input" id="quantity" value="1"/>
                                </div>
                                <div class="sp-plus fff"><a class="ddd" href="javascript:void(0)">+</a></div>
                            </div>
                        </div>
                        <br>
                        <div class="btn_bx">
                            <button class="cmn-btn" onclick="add_to_cart('{{$product->id}}')">{{trans('main.add_t_cat')}}</button>
                        </div>
{{--                        <div class="add_wishlist"><i class="fa fa-heart"></i> <a href="#">Add To Wishlist</a></div>--}}
                    </div>
                </div>

            </div>
            <br>

        </div>

    </div>
    @include('Fronted.Products.rating')


@endsection

@section('script')
    <script>
        var rate = 0;
        $('.gg').click(function () {
            rate = $(this).data("value");
        })

        $('#rate_form').submit(function (e) {
            e.preventDefault();
            $.toast().reset('all');
            Toset('{{trans("main.progress")}}', 'info', '', false);
            if ('{{!Auth::check()}}') {
                location.href = '{{route("User.show_login")}}';
            } else {
                var data = {'rate': rate, 'comment': $('#comment').val()};
                $.ajax({
                    url: '/User/save_my_rate/{{$product->id}}?rate=' + rate,
                    data: data,
                    headers: {'lang': '{{get_lang()}}'},
                    type: 'get',
                    success: function (data) {
                        $.toast().reset('all');
                        if (data.status == 1) {
                            Toset(data.message, 'success', '', false);
                            location.reload();
                        } else {
                            Toset(data.message, 'error', '', false);
                        }
                    },
                    error: function (data) {
                        Toset('{{trans("main.serverError")}}', 'error', '', 5000);
                    }

                })
            }
        })
    </script>
@endsection
