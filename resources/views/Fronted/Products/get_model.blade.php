
<div class="ajax_quick_view">
    <div class="row">
        <div class="col-lg-6 col-md-6 mb-4 mb-md-0">
            <div class="product-image">
                <div class="product_img_box">
                    <img id="product_img" src='/images/Products/{{$row->image}}' data-zoom-image="/images/Products/{{$row->image}}" alt="product_img1" />
                </div>
                <div id="pr_item_gallery" class="product_gallery_item slick_slider" data-slides-to-show="4" data-slides-to-scroll="1" data-infinite="false">
                    <div class="item">
                        <a href="#" class="product_gallery_item active"  data-image="/images/Products/{{$row->image}}" data-zoom-image="/images/Products/{{$row->image}}">
                            <img src="/images/Products/{{$row->image}}" alt="product_small_img1" />
                        </a>
                    </div>

                @foreach($row->images as $key=>$image)
                        <div class="item">
                            <a href="#" class="product_gallery_item"  data-image="/images/Product_image/{{$image->image}}" data-zoom-image="/images/Product_image/{{$image->image}}">
                                <img src="/images/Product_image/{{$image->image}}" alt="product_small_img1" />
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="pr_detail">
                <div class="product_description">
                    <h4 class="product_title"><a href="{{Route('productsFront.productDetails',$row->id)}}">{{getTitleOrDesc($row,1)}}</a></h4>
                    {!! product_price($row) !!}
                    <div class="rating_wrap">
                        <div class="rating">
                            <div class="product_rate" style="width:{{$row->rate * 100 / 5}}%"></div>
                        </div>
                        <span class="rating_num">({{$row->user_rate->count()}})</span>
                    </div>
                    <div class="pr_desc">
                        <p>{{getTitleOrDesc($row,2)}}</p>
                    </div>

                    {!! color_div($row) !!}
                    @if($row->sizes->count() >0)
                        <div class="pr_switch_wrap">
                            <span class="switch_lable">{{trans('main.size')}}</span>
                            <div class="product_size_switch">
                                @foreach($row->sizes as $key=>$size)
                                    <span onclick="size_id('{{$size->id}}')" id="{{$size->id}}">{{substr(getTitleOrDesc($size,1),0,2)}}</span>
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>
                <hr />
                <div class="cart_extra">
                    <div class="cart-product-quantity">
                        <div class="quantity">
                            <input type="button" value="-" class="minus">
                            <input type="text" name="quantity" value="1" title="Qty" id="quantity" class="qty" size="4">
                            <input type="button" value="+" class="plus">
                        </div>
                    </div>
                    <div class="cart_btn">
                        <button class="btn btn-fill-out btn-addtocart" type="button" onclick="add_to_cart('{{$row->id}}')"><i class="icon-basket-loaded"></i>
                            {{trans('main.add_t_cat')}}
                        </button>
                        <a class="add_wishlist" onclick="wishlist('{{$row->id}}')"><i class="icon-heart"></i></a>
                    </div>
                </div>
                <hr />
                <ul class="product-meta">
                    @if($row->cat)   <li>{{trans('main.Category')}}: <a href="{{Route('productsFront.product_by_category',$row->cat->id)}}">{{getTitleOrDesc($row->cat,1)}}</a></li> @endif
                </ul>


            </div>
        </div>
    </div>
</div>

<script src="/assets/js/scripts.js"></script>
