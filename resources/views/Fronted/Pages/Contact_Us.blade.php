@extends('layouts.app')

@section('title')
    {{trans('main.Contact_Us')}}
    @endsection

@section('content')
    <<div class="header-top-inner">
        <div class="container">

            <h2>{{trans('main.Contact_Us')}}</h2>

        </div>
    </div>


    <div class="contact_section1">
        <div class="container">
            <div class="row">

                <div class="contact_map_area">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m26!1m12!1m3!1d3020.941528989988!2d-73.97184738518196!3d40.785299591066035!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m11!3e6!4m3!3m2!1d40.785741099999996!2d-73.969826!4m5!1s0x89c2589ac1735415%3A0x4652759aef136e70!2sCentral+Park+West%2C+New+York%2C+NY%2C+USA!3m2!1d40.784858199999995!2d-73.9696519!5e0!3m2!1sen!2sin!4v1488038448013"
                            width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>


                <div class="contact_detail_area">
                    <ul>

                        <li>
                            <i class="fa fa-home"></i>
                            <div class="contact_details_des">
                                <h2>{{trans('main.address')}}:</h2>
                                <p>
                                    {{get_lang() == 'ar' ? get_main_seetings()->address_ar : get_main_seetings()->address_en}}
                                </p>
                            </div>
                        </li>
                        <li>
                            <i class="fa fa-phone"></i>
                            <div class="contact_details_des">
                                <h2>{{trans('main.phone')}}:</h2>
                                <p>+{{get_main_seetings()->phone}}</p>
                                <p>+{{get_main_seetings()->phone}}</p>
                            </div>
                        </li>
                        <li>
                            <i class="fa fa-envelope"></i>
                            <div class="contact_details_des">
                                <h2>{{trans('main.Email_address')}}:</h2>
                                <p><a href="mailto:info@{{get_main_seetings()->email}}">{{get_main_seetings()->email}}</a></p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <div class="contact_frm_area">
        <div class="product_comment spacer">
            <div class="container">

                <div class="site-section-area">
                    <h2><span>{{trans('main.Get_In_touch')}}</span></h2>
                </div>
                <form id="contact_form">
                <div class="comment_frm" >
                    <ul>
                        <li>
                            <input type="text" required placeholder="{{trans('main.name')}}" id="name">
                        </li>
                        <li>
                            <input type="text" placeholder="{{trans('main.Email_address')}}" id="email">
                        </li>
                        <li>
                            <input type="number" required placeholder="{{trans('main.phone')}}" id="phone">
                        </li>
                        <li>
                            <textarea required placeholder="{{trans('main.message')}}" id="message"></textarea>
                        </li>
                        <li>
                            <button type="submit" class="cmn-btn2">{{trans('main.Send_Message')}}</button>
                        </li>
                    </ul>
                </div>
                </form>

            </div>
        </div>
    </div>

    @include('include.suscribe')
    @include('include.our_brands')
@endsection


@section('script')
    <script>
        $('#contact_form').submit(function(e){
            e.preventDefault();
            $('#submit').attr("disabled", true);
            $.toast().reset('all');
            Toset('{{trans("main.progress")}}','info','',false);
            var data={'email': $('#email').val() , 'phone' : $('#phone').val(),"name": $('#name').val(),'message' : $('#message').val()};

            $.ajax({
                url : '/pages/submit_message',
                headers: { 'lang': '{{get_lang()}}' },
                type : 'get',
                data : data,
                success : function(data){
                    $.toast().reset('all');
                    $('#submit').attr("disabled", false);
                    if(data.status == 0){
                        Toset(data.message,'error','',5000);
                    }else{
                        Toset(data.message,'success','',5000);
                        $('#contact_form')[0].reset();
                    }

                },
                error : function(data)
                {
                    $('#submit').attr("disabled", false);
                    Toset('{{trans("main.serverError")}}','error','',5000);
                }
            })
        })
    </script>
@endsection
