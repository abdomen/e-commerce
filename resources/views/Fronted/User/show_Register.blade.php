@extends('layouts.app')

@section('title')
    {{trans('main.Register')}}
@endsection

@section('content')
    <div class="header-top-inner">
        <div class="container">

            <h2>{{trans('main.Register')}}</h2>

        </div>
    </div>


    <div class="checkout_wrapper">
        <div class="container">


            <div class="checkout_info">
                <div class="row">
                    <div class="col-lg-3 col-sm-12"></div>
                    <div class=" col-lg-6 col-sm-12">
                        <div class="billing_info">
                            <div class="site-section-area">

                                <h2><span>{{trans('main.Register')}}</span></h2>
                            </div>
                            <div class="billing_frm card">
                                <div class="card-body">
                                    <form id="register_form">

                                        <div class="form-group">
                                            <input type="text" class="form-control" name="frist_name" id="frist_name" placeholder="{{trans('main.frist_name')}}">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="last_name" id="last_name" placeholder="{{trans('main.last_name')}}">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" required name="email" id="email" placeholder="{{trans('main.Email_address')}}">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" required name="phone" id="phone" placeholder="{{trans('main.phone')}}">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control"required=""  name="password" id="password" placeholder="{{trans('main.password')}}">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="{{trans('main.Confirm_Password')}}">
                                        </div>
                                        <div class="btnlogin">
                                            <button class="cmn-btn2">{{trans('main.Register')}}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-lg-3 col-sm-12"></div>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        $('#register_form').submit(function(e){
            e.preventDefault();
            $('#login').attr("disabled", true);
            $.toast().reset('all');
            Toset('{{trans("main.progress")}}','info','',false);
            var data={'email': $('#email').val() , 'phone' : $('#phone').val(),"password": $('#password').val(),'last_name' : $('#last_name').val(),
                'frist_name' : $('#frist_name').val() , 'password_confirmation' : $('#password_confirmation').val()
            };

            $.ajax({
                url : '/User/register',
                headers: { 'lang': '{{get_lang()}}'},
                type : 'get',
                data : data,
                success : function(data){
                    $.toast().reset('all');
                    $('#login').attr("disabled", false);
                    if(data.status == 0){
                        Toset(data.message,'error','',false);
                    }else{
                        Toset(data.message,'success','',false);
                        location.href='/';
                    }
                },
                error : function(data)
                {
                    $('#login').attr("disabled", false);
                    Toset('{{trans("main.serverError")}}','error','',5000);
                }
            })
        })
    </script>
@endsection
