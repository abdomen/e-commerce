@extends('layouts.app')
@section('title')
{{trans('main.cart')}}
@endsection

@section('content')
    <div class="header-top-inner">
        <div class="container">

            <h2>{{trans('main.cart')}}</h2>

        </div>
    </div>


    <div class="cart_wrapper">
        <div class="container">

            <div class="cart_item_area">
                <h2>{{trans('main.cart')}}</h2>
                <div class="cart_item_tbl">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>{{trans('main.product')}}</td>
                            <td>&nbsp;</td>
                            <td>{{trans('main.price')}}</td>
                            <td>{{trans('main.Quantity')}}</td>
                            <td>{{trans('main.Total')}}</td>
                            <td>&nbsp;</td>
                        </tr>
                        @foreach(Auth::user()->my_cart as $row)
                            @php
                                $price=\App\Http\Controllers\Manage\BaseController::get_price(Auth::user(),$row);
                            @endphp
                        <tr>
                            <td><img src="{{getImageUrl('Products',$row->image)}}" alt="product"></td>
                            <td><h4>{{getTitleOrDesc($row,1)}}</h4>
                            <td class="price">{{$price}}</td>
                            <td class="cart_qty">
                                <div class="sp-quantity">
                                    <div class="sp-minus fff"><a class="ddd" href="javascript:void(0)">-</a></div>
                                    <div class="sp-input">
                                        <input type="text" class="quntity-input" name="{{$row->id}}"  value="{{$row->pivot->quantity}}"/>
                                    </div>
                                    <div class="sp-plus fff"><a class="ddd" href="javascript:void(0)">+</a></div>
                                </div>
                            </td>
                            <td class="total">$ {{$row->pivot->quantity * $price}}</td>
                            <td class="remove"><a href="#" onclick="delete_from_cart('{{$row->id}}')"><i class="fa fa-trash"></i></a></td>
                        </tr>
                            @endforeach
                    </table>
                </div>
                <div class="cart_btn_area">
                    <button onclick="location.href='/'">{{trans('main.Continue_Shopping')}}</button>
                    <button onclick="updateCart()">{{trans('main.update_cart')}}</button>

                </div>
            </div>

            <div class="cart_cal_area">

                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="cart_total">
                            <h3>{{trans('main.Total')}}</h3>
                            <div class="cart_black">
                                <table id="mytable " width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="customerIDCell">{{trans('main.Cart_Subtotal')}}</td>
                                        <td>:</td>
                                        <td>$ {{getPrice(1)}}</td>
                                    </tr>
                                    <tr>
                                        <td>{{trans('main.Total')}}</td>
                                        <td>:</td>
                                        <td>$ {{getPrice(3)}}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <button onclick="location.href='{{route("Users.make_order")}}'">{{trans('main.Proceed_To_CheckOut')}}</button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- END MAIN CONTENT -->
@endsection

@section('script')
<script>
    var ids=[];
    function updateCart()
    {
        var map = {};
$(".quntity-input").each(function() {
    map[$(this).attr("name")] = $(this).val();
});
        Toset('{{trans("main.progress")}}','info','',false);

$.ajax({
    url : '/User/update_cart',
    data : { values : map },
                headers: { 'lang': '{{get_lang()}}' },

    success : function(data){
       $.toast().reset('all');
       
       if(data.status==0){
                    Toset(data.message,'error','',5000);
                }else{
                           Toset(data.message,'success','',5000);

                    location.reload();
                    
                }
    }
})
console.log(map); // "red"
    }
    
</script>
@endsection