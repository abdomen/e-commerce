@extends('layouts.app')
@section('title')
    {{trans('main.Wishlist')}}
@endsection

@section('content')
     <div class="header-top-inner">
    <div class="container">

        <h2>{{trans('main.Wishlist')}} </h2>

    </div>
</div>


<div class="cart_wrapper">
    <div class="container">

        <div class="cart_item_area">
            <h2>{{trans('main.Wishlist')}}</h2>
            <div class="cart_item_tbl">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>{{trans('main.product')}}</td>
                        <td>&nbsp;</td>
                        <td>{{trans('main.price')}}</td>
                        <td>{{trans('main.add_t_cat')}}</td>
                        <td>&nbsp;</td>
                    </tr>
                    @foreach(Auth::user()->my_wishlist as $row)
                    <tr>
                        <td><img src="{{getImageUrl('Products',$row->image)}}" alt="product"></td>
                        <td><h4>{{getTitleOrDesc($row,1)}}</h4>
                        <td class="price">${{\App\Http\Controllers\Manage\BaseController::get_price(Auth::user(),$row)}}</td>

                        <td class="cart1  plantmore-product-add-cart"><a onclick="add_to_cart('{{$row->id}}')">{{trans('main.add_t_cat')}}</a></td>
                        <td class="remove"><a onclick="wishlist('{{$row->id}}',2)"><i class="fa fa-trash"></i></a></td>
                    </tr>
                    @endforeach
                </table>
            </div>


        </div>
    </div>


        <!-- END SECTION SHOP -->
        @include('include.suscribe')
    </div>
    <!-- END MAIN CONTENT -->

@endsection
