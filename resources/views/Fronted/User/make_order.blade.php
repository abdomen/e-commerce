@extends('layouts.app')
@section('title')
    {{trans('main.Checkout')}}
    @endsection

@section('content')
   <div class="header-top-inner">
    <div class="container">

        <h2>{{trans('main.Checkout')}}</h2>

    </div>
</div>


<div class="checkout_wrapper">
    <div class="container">


        <div class="checkout_info">
            <div class="row">
                <div class="col-lg-7 col-md-7  col-sm-12">
                    <div class="billing_info">
                        <div class="site-section-area">
                            <h2>{{trans('main.Checkout')}}</h2>
                        </div>
                        <div class="billing_frm">
                            <form>
                            
                              
                                <div class="form-group">
                                    <input type="text" required class="form-control" id="pickup_country" placeholder="@if(Request::segment(1)=='ar') العنوان @else Address* @endif">
                                </div>
                                
                        
                
                                <div class="form-group">
                                    <textarea class="form-control"></textarea>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12">
                    <div class="order_info">
                        <div class="site-section-area">
                            <h6>Winter Collections</h6>
                            <h2>{{ trans('main.Your_Order') }}</h2>
                        </div>
                        <div class="order_bg">
                            <div class="order_total">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <th>{{trans('main.product')}}</th>
                                        <th>{{trans('main.Total')}}</th>
                                    </tr>
                                </table>
                                <table width="76%" border="0" cellspacing="0" cellpadding="0" class="tbl2">
                                    @foreach(Auth::user()->my_cart as $row)
                                    <tr>
                                        <td>{{getTitleOrDesc($row,1)}} X- {{ $row->pivot->quantity}}</td>
                                        <td>${{\App\Http\Controllers\Manage\BaseController::get_price(Auth::user(),$row) * $row->pivot->quantity}}</td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td>{{trans('main.Cart_Subtotal')}}</td>
                                        <td>${{getPrice(1)}}</td>
                                    </tr>
                                    <tr>
                                        <td>{{trans('main.Shipping')}}</td>
                                        <td class="free_txt">{{getPrice(2)}}</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>{{trans('main.Total')}}</div>
                                        </td>
                                        <td>
                                            <div>${{getPrice(3)}}</div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="payment_method">

                                <h5>
                                    <button onclick="add_address()"  class="cmn-btn">{{ trans('main.Place_Order')}}</button>
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('include.suscribe')
@include('include.our_brands')

@endsection


@section('script')


<script>
    var address_id=0;
    var code_id='{{Auth::user()->discount_id}}' ? '{{Auth::user()->discount_id}}' : 0;
    function make_order()
    {
        address_id= address_id == 0 ? $('#address_id').val() : address_id;

        $.ajax({
            url : '/User/make_my_order/' +'?address_id='+address_id +'&payment_method=1&code_id='+code_id,
            type : 'get',
             headers: { 'lang': '{{get_lang()}}' },
            success: function(data)
            {
                $.toast().reset('all');
                if(data.status == 1){
                    Toset(data.message,'success','',false);

                    location.href='/';
                }else if(data.status==0){
                    Toset(data.message,'error','',false);
                }
            },
            error : function(data)
            {
                $.toast().reset('all');
                Toset('{{trans("main.serverError")}}','error','',5000);
            }
        })
    }
</script>

    <script>
        function check_code() {
            Toset('{{trans("main.progress")}}','info','',false);
            $.ajax({
                url : '/User/check_discount_code?code='+$('#code_id').val(),
                type : 'get',
                success: function(data)
                {
                    $.toast().reset('all');
                    if(data.status == 1){
                        Toset(data.message,'success','',6000);
                        code_id=data.data.code_id;
                        $('#total_price').text(data.data.total_price);
                        $('#code_id').val('');
                    }else if(data.status==0){
                        Toset(data.message,'error','',6000);
                        code_id=0;
                       ;
                    }
                },
                error : function(data)
                {
                    $.toast().reset('all');
                    Toset('{{trans("main.serverError")}}','error','',5000);
                }
            })
        }
    </script>

    <script>
        function add_address() {
             if($('#pickup_country').val() == ''){
                alert(@if(Request::segment(1) == 'ar') 'من فضلك ادخل العنوان' @else 'address is required' @endif);
            }else{
            Toset('{{trans("main.progress")}}','info','',false);
           
            $.ajax({
                url : '/User/add_address?address='+$('#pickup_country').val(),
                type : 'get',
                headers: { 'lang': '{{get_lang()}}' },
                success: function(data)
                {

                    if(data.status == 1){
                        address_id=data.data.id;
                        make_order();
                    }else if(data.status==0){
                        Toset(data.message,'error','',6000);

                    }
                },
                error : function(data)
                {
                    $.toast().reset('all');
                    Toset('{{trans("main.serverError")}}','error','',5000);
                }
            })
            }
        }
    </script>

@endsection


