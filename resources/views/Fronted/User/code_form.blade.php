<form method="post" id="code_form" style="display: none">
    @csrf
    <div class="form-group">
        <input type="password" required="" class="form-control" name="password" id="password" placeholder="{{trans('main.passwordNew')}}">
    </div>
    <div class="form-group">
        <input type="text" required="" class="form-control" name="code" id="code" placeholder="{{trans('main.code')}}">
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-fill-out btn-block" name="login">{{trans('main.reset_password')}}</button>
    </div>
</form>
