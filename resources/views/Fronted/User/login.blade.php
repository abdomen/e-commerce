@extends('layouts.app')

@section('title')
    {{trans('main.login')}}
@endsection

@section('content')
    <div class="header-top-inner">
        <div class="container">

            <h2> {{trans('main.login')}}</h2>

        </div>
    </div>


    <div class="checkout_wrapper">
        <div class="container">


            <div class="checkout_info">
                <div class="row">
                    <div class="col-lg-3 col-sm-12"></div>
                    <div class=" col-lg-6 col-sm-12">
                        <div class="billing_info">
                            <div class="site-section-area">

                                <h2><span> {{trans('main.login')}}</span></h2>
                            </div>
                            <div class="billing_frm card">
                                <div class="card-body">
                                    <form id="login_form">

                                        <div class="form-group">
                                            <input type="text" class="form-control" required name="email" id="email" placeholder="{{trans('main.Email_address')}}">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" required="" type="password" id="password" name="password" placeholder="{{trans('main.password')}}">
                                        </div>

                                        <div class="btnlogin">
                                            <button class="cmn-btn2">{{trans('main.login')}}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-lg-3 col-sm-12"></div>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        $('#login_form').submit(function(e){
            e.preventDefault();
            $('#login').attr("disabled", true);
            $.toast().reset('all');
            Toset('{{trans("main.progress")}}','info','',false);
            var data={'eamilOrPhone': $('#email').val() , 'password' : $('#password').val(),"_token": "{{ csrf_token() }}"};

            $.ajax({
                url : '/User/login',
                headers: { 'lang': '{{get_lang()}}' , 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type : 'get',
                data : data,
                success : function(data){
                    $.toast().reset('all');
                    $('#login').attr("disabled", false);
                    if(data.status == 0){
                        Toset(data.message,'error','',false);
                    }else if(data.status == 1){
                        Toset(data.message,'success','',false);
                        location.href='/';
                    }

                },
                error : function(data)
                {
                    Toset('{{trans("main.serverError")}}','error','',5000);
                }
            })
        })
    </script>
@endsection
